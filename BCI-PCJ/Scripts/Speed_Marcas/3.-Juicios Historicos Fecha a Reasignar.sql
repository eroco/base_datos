--JUICIOS HISTORICOS, SE DEBE PEGAR LA FECHA DE REASIAGNACION DEL JUICIO

DECLARE @v_id_gestion_mandamiento as int
SELECT @v_id_gestion_mandamiento = DBO.fn_ut_parametro_constante('ID_GESTION_MANDAMIENTO')


DECLARE @TEMPORAL TABLE(
	ID int identity(1,1),
	idjuicio int,
	plazo int,
	fechaini smalldatetime,
	fechaplazo smalldatetime
)

--BUSCO TODOS LOS JUICIOS CON LA GESTION MANDAMIENTO Y QUE SEAN SPEED
INSERT INTO @TEMPORAL(IDJUICIO,fechaini)
select DISTINCT ju_juiciosgestiones.ID_JUICIO,ju_fechaini
from ju_juiciosgestiones
inner join ju_juicios on ju_juicios.id_juicio = ju_juiciosgestiones.id_juicio
where ju_juiciosgestiones.id_Gestion = @v_id_gestion_mandamiento
AND [dbo].[fn_retorna_si_es_juicio_speed](ju_juiciosgestiones.ID_JUICIO) != 0
order by ju_juiciosgestiones.id_juicio

update @temporal set plazo = [dbo].[fn_retorna_plazo_juicio_speed](idjuicio)
update @temporal set fechaplazo = isnull(fechaini,getdate()) + plazo

update ju_juicios set ju_fechaareasignarspeed = fechaplazo
from(
	select idjuicio,fechaplazo from @temporal 
) data
where ju_juicios.id_juicio = idjuicio

--revision
/*
select id_juicio,ju_rol,cl_rut,cl_cliente,id_abogado,ab_abogado,ju_fechaini,ju_fechaareasignarspeed 
from ju_juicios where ju_fechaareasignarspeed is not null
*/