
if not exists(select * from ca_menu where men_nombre = 'Operaciones' and id_menupadre = 12)
	Insert into ca_menu(id_menupadre,men_nombre,men_url,id_tipoventana,id_opcion,
	id_sistema,men_opciones,men_ancho,men_alto,men_orden,men_activo,men_nombreventana)
	values(
			12,'Operaciones','',
			1,237,1,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
			800,560,80,1,'PLG Operaciones'
	)


if not exists(select * from ca_menu where men_nombre = 'Operaciones Tipos')
	Insert into ca_menu(id_menupadre,men_nombre,men_url,id_tipoventana,id_opcion,
	id_sistema,men_opciones,men_ancho,men_alto,men_orden,men_activo,men_nombreventana)
	values(
			(select top 1 id_menu from ca_menu where men_nombre = 'Operaciones' and id_menupadre = 12),
			'Operaciones Tipos','(urlGarantias)MantenedoresBasicos/operaciones/wf_op_operacionestipos_no.aspx',
			1,235,1,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
			800,560,80,1,'PLG Operaciones Tipos'
	)

/*
delete ca_menu where men_nombre = 'Operaciones' and id_menupadre = 12
delete ca_menu where men_nombre = 'Operaciones Tipos'
*/