DECLARE @TEMPORAL table (
	id int identity(1,1),
	idoperaciontipo int,
	plazo int
)

insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = '1 VCTO.'
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'AUTOMOTRIZ                    '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 0 from op_operacionestipos where op_operaciontipo = 'CAE                           '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 40 from op_operacionestipos where op_operaciontipo = 'COMERCIAL                     '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'CONSUMO                       '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'CONSUMO COLAB.                '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 0 from op_operacionestipos where op_operaciontipo = 'CORFO                         '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'CREDITO                       '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 0 from op_operacionestipos where op_operaciontipo = 'FOGAIN                        '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 0 from op_operacionestipos where op_operaciontipo = 'FOGAPE                        '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'HIPOTECARIO                   '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'Indefinida                    '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'LIN.CRE.COMERCIAL             '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'LIN.CRE.CONSUMO               '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'LIN.CRED. COMERCIAL           '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'LINEA                         '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'LINEA CREDITO                 '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'MASTER.PE                     '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'MUTUO HIP.                    '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'MUTUO VIV                     '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'PAGARE EN GTIA.               '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'REPROGRAMADO                  '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'RETENCION                     '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 30 from op_operacionestipos where op_operaciontipo = 'TARJETA                       '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 40 from op_operacionestipos where op_operaciontipo = 'VISA                          '
insert into @TEMPORAL(idoperaciontipo,plazo) select id_operaciontipo, 40 from op_operacionestipos where op_operaciontipo = 'VISA EMP.                     '

update op_operacionestipos set ot_plazosasignacionSpeed = 0 where ot_plazosasignacionSpeed is null

update op_operacionestipos set ot_plazosasignacionSpeed = data.plazo
from(
	select idoperaciontipo,plazo from @temporal
) data
where op_operacionestipos.id_operaciontipo = data.idoperaciontipo


--select * from op_operacionestipos order by ot_plazosasignacionSpeed desc