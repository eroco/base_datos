--alters a tablas
IF not EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'op_operacionestipos'
		  AND COLUMN_NAME = 'ot_plazosasignacionSpeed') 
begin
	alter table op_operacionestipos add ot_plazosasignacionSpeed int default(0)
end

IF not EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'ju_juicios'
		  AND COLUMN_NAME = 'ju_fechareasignacionspeed') 
begin
	alter table ju_juicios add ju_fechareasignacionspeed smalldatetime 
	--fecha que realmente se genero la reasignacion automatica
end


IF not EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'ju_juicios'
		  AND COLUMN_NAME = 'ju_fechaareasignarspeed') 
begin
	alter table ju_juicios add ju_fechaareasignarspeed smalldatetime 
	--'Fecha limite para la reasignacion automatica de speed, se calcula fecha de inicio del juicio mas los dias plazo menor de las operaciones del juicio' 
end


IF not EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'op_operaciones'
		  AND COLUMN_NAME = 'op_fechadesmarquespeed') 
begin
	alter table op_operaciones add op_fechadesmarquespeed smalldatetime 
end

IF not EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'op_operaciones'
		  AND COLUMN_NAME = 'op_userdesmarquespeed') 
begin
	alter table op_operaciones add op_userdesmarquespeed varchar(800) 
end