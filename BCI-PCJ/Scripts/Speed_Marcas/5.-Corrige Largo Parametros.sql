UPDATE tg_parametros SET par_largo = 9 where par_unidad in (
'uf','Porcentaje','Contable','Entero','ID'
) and par_largo is null

UPDATE tg_parametros SET par_largo = 1000 where par_unidad in (
'Texto','NOMBRE'
) and par_largo is null