--SOLO SE DEBE DEJAR 1. PARA EL AMBIENTE QUE SE ESTE EJECUTANDO
if not exists(select * from tg_constantes where ct_constante = 'ConectionStringBulk') begin
	
	--INTEGRACION
	--Insert into tg_constantes(ct_constante, ct_constantevalor ,id_constantetipo, ct_constantedescripcion)
	--values('ConectionStringBulk','Data Source=192.0.0.21\SQL2008;Initial Catalog=PJBCI_INT;Persist Security Info=True;User ID=usr_pjbci;Password=usr_pjbci',1,'ConectionStringBulk PARA EL FTP ') 

	--CLON
	--Insert into tg_constantes(ct_constante, ct_constantevalor ,id_constantetipo, ct_constantedescripcion)
	--values('ConectionStringBulk','Data Source=192.0.0.21\SQL2008;Initial Catalog=PCJBCI_PROD_CLON;Persist Security Info=True;User ID=usr_pjbci;Password=usr_pjbci',1,'ConectionStringBulk PARA EL FTP ') 

	--PRODUCCIÓN
	Insert into tg_constantes(ct_constante, ct_constantevalor ,id_constantetipo, ct_constantedescripcion)
	values('ConectionStringBulk','Data Source=192.0.0.149;Initial Catalog=PCJBCI_PROD;Persist Security Info=True;User ID=usr_pjbci;Password=usr_pjbci',1,'ConectionStringBulk PARA EL FTP ') 
	

end

if not exists(select * from tg_constantes where ct_constante = 'ftpHost') begin
	
	--INTEGRACION
	--Insert into tg_constantes(ct_constante, ct_constantevalor ,id_constantetipo, ct_constantedescripcion)
	--values('ftpHost','ftp.plataformagroup.cl/inbox/pruebas',1,'ftpHost PARA EL FTP ') 

	--CLON
	--Insert into tg_constantes(ct_constante, ct_constantevalor ,id_constantetipo, ct_constantedescripcion)
	--values('ftpHost','ftp.plataformagroup.cl/inbox/pruebas',1,'ftpHost PARA EL FTP ') 

	--PRODUCCIÓN
	Insert into tg_constantes(ct_constante, ct_constantevalor ,id_constantetipo, ct_constantedescripcion)
	values('ftpHost','ftp.plataformagroup.cl/inbox',1,'ftpHost PARA EL FTP ') 


end
/*
delete tg_constantes where ct_constante = 'ConectionStringBulk'
delete tg_constantes where ct_constante = 'ftpHost'
*/
