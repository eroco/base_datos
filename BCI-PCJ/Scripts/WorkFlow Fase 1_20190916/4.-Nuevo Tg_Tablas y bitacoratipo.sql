--select * from tg_tablas
---32	wf_remates
if not exists(select * from tg_tablas where ta_tabla = 'wf_remates') begin
	insert into tg_tablas(ta_tabla,ta_descripcion,ta_activo)
	values('wf_remates','WorkFlow Remates',1)
end

if not exists(select * from bi_bitacorastipos where bt_bitacoratipo = 'WorkFlow Remates') begin
	insert into bi_bitacorastipos(bt_bitacoratipo,id_Tabla,bt_activo,id_clasificacion)
	values('WorkFlow Remates',(select id_tabla from tg_tablas where ta_tabla = 'wf_remates'),1,1)
end

--Select * from bi_bitacorastipos where bt_bitacoratipo = 'WorkFlow Remates'