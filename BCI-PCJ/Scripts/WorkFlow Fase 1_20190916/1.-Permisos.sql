if not exists(select * from [ca_opciones] where [id_opcion] = 773) begin
	INSERT INTO [dbo].[ca_opciones]
		([id_opcion]
		,[op_opcion]
		,[op_constante]
		,[id_proceso])
	VALUES
		(773
		,'Workflow Remate - Ver nomina de workflow Remate'
		,'ACC_WORKFLOW_REMATE-VERTODO'
		,33)
end

if not exists(select * from [ca_opciones] where [id_opcion] = 774) begin
	INSERT INTO [dbo].[ca_opciones]
		([id_opcion]
		,[op_opcion]
		,[op_constante]
		,[id_proceso])
	VALUES
		(774
		,'Workflow Remate - Cerrar etapa'
		,'ACC_WORKFLOW_REMATE-CERRARETAPA'
		,33)
end

--select * from [ca_opciones] order by 1 desc