/*--INSERTA PERMISOS MASIVOS--*/
declare @temporalPerfiles table(
	id int identity(1,1),
	idperfil int
)

declare @temporalOpciones table(
	id int identity(1,1),
	idOpcion int
)

insert into @temporalPerfiles(idperfil) values(1)

insert into @temporalOpciones(idOpcion) values(773)
insert into @temporalOpciones(idOpcion) values(774)


INSERT INTO CA_PERFILESOPCIONES(ID_PERFIL,ID_OPCION)
select id_perfil,id_opcion from ca_perfiles 
cross join ca_opciones
where 
id_perfil in ( select idperfil from @temporalPerfiles )
and id_opcion in (select idOpcion from @temporalOpciones)
and id_perfil+''+id_opcion not in (
	select id_perfil+''+id_opcion from ca_perfilesopciones where id_perfil in (select idperfil from @temporalPerfiles)
	and id_opcion in (select idOpcion from @temporalOpciones)
)

/*
select * from CA_PERFILESOPCIONES where id_opcion in (773,774)
*/