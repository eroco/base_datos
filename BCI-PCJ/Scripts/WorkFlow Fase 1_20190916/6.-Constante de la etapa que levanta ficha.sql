
if not exists(select * from tg_constantes where ct_constante = 'ID_REMATEETAPA_FICHA') begin

	Insert into tg_constantes(ct_constante, ct_constantevalor ,id_constantetipo, ct_constantedescripcion)
	values('ID_REMATEETAPA_FICHA',
	(select cast(id_remateetapa as varchar) id_remateetapa from wf_rematesetapas where re_remateetapa like '%Fecha de Remate - Ingreso Nueva Ficha Abogado%' )
	,1,'Etapa que levanta la FICHA') 

end


if not exists(select * from tg_constantes where ct_constante = 'URL_REMATEETAPA_FICHA') begin

	Insert into tg_constantes(ct_constante, ct_constantevalor ,id_constantetipo, ct_constantedescripcion)
	values('URL_REMATEETAPA_FICHA',
	'info_general_remate.asp?accion=MODIFICAR&id_propiedadrj={id_propiedadrj}&id_propiedadcomite={id_propiedadcomite}'
	,1,'url para FICHA creada') 

end



---select * from tg_constantes where ct_constante like '%CONST_CLA_TAREAS%'