
create table wf_rematesprorrogastipos(
	id_remateprorrogatipo int identity(1,1),
	rpt_remateprorrogatipo varchar(500),
	rpt_esvigente int,
	rpt_orden int
)


alter table wf_rematesprorrogas add id_remateprorrogatipo int

/*llave foranea me sirve para crear las paginas ASPX*/
ALTER TABLE dbo.wf_rematesprorrogastipos
	ADD CONSTRAINT UQ_wf_rematesprorrogastipos_id_remateprorrogatipo UNIQUE (id_remateprorrogatipo);

ALTER TABLE dbo.wf_rematesprorrogas ADD CONSTRAINT wf_rematesprorrogas_wf_rematesprorrogastipos_FK1 
	FOREIGN KEY (id_remateprorrogatipo) REFERENCES dbo.wf_rematesprorrogastipos (id_remateprorrogatipo);

/*no tengo idea de que tipo de prorroga deben ir aca pero se crean 3 para seguir con desarrollo*/
insert into wf_rematesprorrogastipos(rpt_remateprorrogatipo,rpt_esvigente,rpt_orden) values('Extender plazo 1 a�o de workflow',1,1);
insert into wf_rematesprorrogastipos(rpt_remateprorrogatipo,rpt_esvigente,rpt_orden) values('Extender plazo 2 a�o de workflow',1,2);
insert into wf_rematesprorrogastipos(rpt_remateprorrogatipo,rpt_esvigente,rpt_orden) values('Extender plazo 3 a�o de workflow',1,3);

/*necesito guardar la persona que genera la prorroga*/
alter table wf_rematesprorrogas add id_persona int


ALTER TABLE dbo.wf_rematesprorrogas ADD CONSTRAINT wf_rematesprorrogas_tg_personas_FK1 
	FOREIGN KEY (id_persona) REFERENCES dbo.tg_personas (id_persona);


/*agrego tipo de pagos de pruebas*/
insert into wf_rematespagostipos(rpt_rematepagotipo,rpt_esvigente,rpt_orden) values('Efectivo',1,1);
insert into wf_rematespagostipos(rpt_rematepagotipo,rpt_esvigente,rpt_orden) values('Cr�dito',1,2);
insert into wf_rematespagostipos(rpt_rematepagotipo,rpt_esvigente,rpt_orden) values('D�bito',1,3);
insert into wf_rematespagostipos(rpt_rematepagotipo,rpt_esvigente,rpt_orden) values('Cheque',1,4);
insert into wf_rematespagostipos(rpt_rematepagotipo,rpt_esvigente,rpt_orden) values('Transferencia Bancaria',1,5);
insert into wf_rematespagostipos(rpt_rematepagotipo,rpt_esvigente,rpt_orden) values('PayPal',1,6);
insert into wf_rematespagostipos(rpt_rematepagotipo,rpt_esvigente,rpt_orden) values('Sofort',1,7);
insert into wf_rematespagostipos(rpt_rematepagotipo,rpt_esvigente,rpt_orden) values('Cheque a Fecha',1,8);
insert into wf_rematespagostipos(rpt_rematepagotipo,rpt_esvigente,rpt_orden) values('Vale Vista',1,9);
insert into wf_rematespagostipos(rpt_rematepagotipo,rpt_esvigente,rpt_orden) values('Direct Debit',1,10);

/*
ALTER TABLE dbo.wf_rematesdetalles ADD CONSTRAINT wf_wf_rematesdetalles_wf_rematesopciones_FK1 
	FOREIGN KEY (id_remateopcion) REFERENCES dbo.wf_rematesopciones (id_remateopcion);
*/


update wf_rematesetapas set re_urlanb = 'info_propiedad_remate.asp?accion=INGRESAR&tipoRegistro=PP&id_consignatario=31&id_rematedetalle={id_rematedetalle}' 
where re_remateetapa like '%Fecha de Remate - Ingreso Nueva Ficha Abogado%'

update wf_rematesetapas set re_urlanb = 'info_general_remate.asp?id_propiedadcomite={id_propiedadcomite}&id_propiedadrj={id_propiedadrj}&accion=MODIFICAR' 
where re_remateetapa like '%Fecha de Remate - Solicitud de Tasaci�n%'

update wf_rematesetapas set re_urlanb = 'comite_opinion_uai.asp?accion=MODIFICAR&id_propiedadcomite={id_propiedadcomite}&usuario=1' 
where re_remateetapa like '%Consejo de Remate - Postura Pre comit�%'

update wf_rematesetapas set re_urlanb = 'comite_opinion_uai.asp?accion=MODIFICAR&id_propiedadcomite={id_propiedadcomite}&usuario=3' 
where re_remateetapa = 'Remate'

update wf_rematesetapas set re_urlanb = 'comite_propiedad_remresultado.asp?id_propiedadrj={id_propiedadrj}&id_propiedadcomite={id_propiedadcomite}' 
where re_remateetapa = 'Adjudicaci�n'

--http://integracionbci.plataformaanb.cl/sitio_interno/remate_judicial/comite_propiedad_remresultado.asp?id_propiedadcomite=180

alter table wf_rematesdetallespropiedadesrj add id_propiedadcomite int