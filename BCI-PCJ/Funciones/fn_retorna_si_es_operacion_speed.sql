
ALTER FUNCTION [dbo].[fn_retorna_si_es_operacion_speed](
    @id_operacion			as int
)
returns INT

/*
@Author : Emanuel Roco
@Fecha : 03/09/2019

@Author : 
@Fecha Modificación : 
*/

AS 
BEGIN
	DECLARE @CUENTA_OP_SPEED INT = 0

	SELECT @CUENTA_OP_SPEED = COUNT(1) FROM OP_OPERACIONES
	WHERE id_operacion = @id_operacion 
	AND substring(lower(ltrim(rtrim(isnull(OP_HITO,'')))),1,5) = 'speed'

	IF @CUENTA_OP_SPEED > 0 BEGIN
		SET @CUENTA_OP_SPEED = 1
	END

	RETURN @CUENTA_OP_SPEED

END



