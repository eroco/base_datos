CREATE FUNCTION [dbo].[fn_retorna_plazo_juicio_speed](
    @id_juicio			as int
)
returns INT

/*
@Author : Emanuel Roco
@Fecha : 30/07/2018
*/

AS 
BEGIN
	DECLARE @PLAZO_SPEED INT = 0

	SELECT @PLAZO_SPEED = MIN(isnull(ot_plazosasignacionSpeed,0)) 
	FROM ju_juicios
	INNER JOIN op_operacionesjuicios ON op_operacionesjuicios.id_juicio = ju_juicios.id_juicio
	INNER JOIN op_operaciones on op_operaciones.id_operacion = op_operacionesjuicios.id_operacion
	INNER JOIN OP_OPERACIONESTIPOS ON OP_OPERACIONESTIPOS.ID_OPERACIONTIPO = OP_OPERACIONES.ID_OPERACIONTIPO
	WHERE ju_juicios.id_juicio = @id_juicio 
	AND [dbo].[fn_retorna_si_es_operacion_speed](op_operaciones.id_operacion) = 1

	RETURN @PLAZO_SPEED

END




