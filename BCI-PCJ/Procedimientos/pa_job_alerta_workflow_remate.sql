USE [PJBCI_INT]
GO
/****** Object:  StoredProcedure [dbo].[pa_job_alerta_workflow_remate]    Script Date: 30/08/2019 15:10:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[pa_job_alerta_workflow_remate]
as

/*
	Creador por			:	Desarrollador 1
	Fecha Creación		:	28/08/2019
	Procedimiento		:	
	Modificado por		:	
	Fecha Modificación	:	
	Modificado por		:	
	Fecha Modificación	:	
	Comentario Mod.		:	Actualiza los segmentos en las tablas de informes diarios
*/



/********************CMUNOZ CORREOS A RESPONSABLES de la ETAPA ***************************************/
BEGIN TRAN

	declare @temporalETAPA table (
		id			int identity(1,1),
		msj			varchar(8000),
		correo		varchar(1000),
		idjuicio	int
	)

	INSERT INTO @temporalETAPA(msj,correo,idjuicio)
	SELECT 
	'Estimado, <br><br>'+
	'Se Informa que la Etapa del Workflow de Remates '''+ re.re_remateetapa + ''', esta a días de su cumplimiento, la fecha es el ' + 
	[dbo].[fn_ut_rellena_prefijos](cast(month(rd_fechacumplimiento) as varchar(12)),2,'0','izq') + '/' + 
	[dbo].[fn_ut_rellena_prefijos](cast(month(rd_fechacumplimiento) as varchar(12)),2,'0','izq') +'/' +
	cast(year(rd_fechacumplimiento) as varchar(12)) +', el detalle es el siguiente: <br>'+
	'<b>Juicio Rol</b>		: ' +  j.ju_rol + ' <br>'+
	'<b>Tribunal</b>		: '+ t.tr_tribunal + ' <br>'+
	'<b>Abogado</b>			: '+ j.ab_abogado + ' <br>'+
	'<b>Cliente Rut</b>		: ' + j.cl_rut + ' <br>'+
	'<b>Nombre Cliente</b>	: ' +j.cl_cliente +' <br><br>'+
	'Saludos' msj,
	pu.pu_email correo, 
	j.id_juicio idjuicio
	,rd_fechacumplimiento
	,re_diasplazo
	,datediff(d,getdate(),rd_fechacumplimiento) datediff_rd_fechacumplimiento
	,isnull(re_diasplazo,1)/2 _re_diasplazo_mitad
	FROM            tg_personasubicaciones pu INNER JOIN
		tg_personas AS pe ON pu.id_persona = pe.id_persona INNER JOIN
		wf_remates AS r INNER JOIN
		ju_juicios AS j ON r.id_juicio = j.id_juicio INNER JOIN
		ju_tribunales AS t ON j.id_tribunal = t.id_tribunal INNER JOIN
		ju_juiciosetapas AS jet ON j.id_juicioetapa = jet.id_juicioetapa INNER JOIN
		ju_juiciosTipos AS jt ON j.id_juiciotipo = jt.id_juiciotipo INNER JOIN
		wf_rematesdetalles AS rd ON r.id_remate = rd.id_remate INNER JOIN
		wf_rematesetapas AS re ON rd.id_remateetapa = re.id_remateetapa INNER JOIN
		ju_juiciosEstados AS je ON j.id_juicioestado = je.id_juicioestado ON pe.id_persona = rd.id_responsable
	WHERE datediff(d,getdate(),rd_fechacumplimiento) <= isnull(re_diasplazo,1)/2
		AND (pu.pu_esDeNotificacion = 1) 
		AND (r.re_fechafin IS NULL) 
		AND (rd.id_remateresultado = 1)
		--select * from wf_rematesetapas

	if @@error <> 0 goto on_errorResponsable; 



	declare @corrResponsable			int
	declare @corrMAXResponsable			int
	declare @NroInicialResponsable		int 
	declare @NroFinalResponsable		int
	declare @CORREOResponsable			varchar(8000)
	declare @MSJResponsable				varchar(8000)

	select  @NroFinalResponsable = max(id), @NroInicialResponsable = min(id) 
	from @temporalETAPA 

	set     @corrResponsable  = @NroInicialResponsable

	WHILE ( @corrResponsable <= (@NroFinalResponsable) )
	begin

		select	@MSJResponsable = msj,
				@CORREOResponsable = correo
		from @temporalETAPA
		where id = @corrResponsable;
		if @@error <> 0 goto on_errorResponsable; 

		if @correoResponsable != '' begin
			Exec pa_clr_enviar_correo
			@config_Smtp = 'mail.plataformagroup.cl',
			@config_port = '25',
			@config_user = 'plataformajudicialbci@plataformagroup.cl',
			@config_pass = 'pcjbci$0270',
			@config_ssl = '0',
			@param_To = @CORREOResponsable,
			@param_Cc = 'eroco@plataformagroup.cl,ygonzalez@plataformagroup.cl',--'servicioalcliente@plataformagroup.cl',
			@param_From = 'plataformajudicialbci@plataformagroup.cl',
			@param_Subject = 'WorkFlow Alarmas Responsable de la Tarea',
			@param_body = @MSJResponsable,
			@param_files = '';
			if @@error <> 0 goto on_errorResponsable; 
		end
		/*---------------------MANEJO DE SALTOS DEL WHILE------------------*/
		if @corrResponsable = @NroFinalResponsable break

		/* LE ASIGNAMOS A LA VARIABLE @CORR EL SIGUIENTE VALOR VALIDO DEL JUICIO DEL CLIENTE */
		SELECT distinct top 1 @corrResponsable = id 
		from @temporalETAPA 
		where 1=1
		AND isnull(id,0) > @corrResponsable
		ORDER BY id;
		if @@error <> 0 goto on_errorResponsable; 
	end

if @@trancount > 0 BEGIN
	commit tran
	print 'Proceso  correo de RESPONSABLE ETAPA ejecutado correctamente.'
	select 0, getdate() as fecha, DB_NAME() as base, SYSTEM_USER as Usuario, CURRENT_USER as usuario2 , 'Proceso  correo de RESPONSABLE ETAPA ejecutado correctamente.'
END

ON_ERRORResponsable:
If @@TRANCOUNT > 0 BEGIN
	ROLLBACK TRAN
	print 'Se Produjo un error en la transacción (aparatado de correo de RESPONSABLE)'
	select -1, getdate() as fecha, DB_NAME() as base, SYSTEM_USER as Usuario, CURRENT_USER as usuario2, 'Se Produjo un error en la transacción (aparatado de correo de RESPONSABLE)'
END

-------------------------------------------------------------CORREOS A SUPERVISORES


----------------------------------------------EROCO CORREOS A RESPONSABLES de la alarma

BEGIN TRAN
	declare @temporal table (
		id int identity(1,1),
		msj  varchar(8000),
		correo  varchar(1000),
		idjuicio int
	)

	INSERT INTO @temporal(msj,correo,idjuicio)
	SELECT 
	'Estimado, <br><br>'+
	'Se Informa que la Etapa del Workflow de Remates '''+ re.re_remateetapa + ''', esta a días de su cumplimiento, la fecha es el ' + 
	[dbo].[fn_ut_rellena_prefijos](cast(month(rd_fechacumplimiento) as varchar(12)),2,'0','izq') + '/' + 
	[dbo].[fn_ut_rellena_prefijos](cast(month(rd_fechacumplimiento) as varchar(12)),2,'0','izq') +'/' +
	cast(year(rd_fechacumplimiento) as varchar(12)) +', el detalle es el siguiente: <br>'+
	'<b>Juicio Rol</b>		: ' +  j.ju_rol + ' <br>'+
	'<b>Tribunal</b>		: '+ t.tr_tribunal + ' <br>'+
	'<b>Abogado</b>			: '+ j.ab_abogado + ' <br>'+
	'<b>Cliente Rut</b>		: ' + j.cl_rut + ' <br>'+
	'<b>Nombre Cliente</b>	: ' +j.cl_cliente +' <br><br>'+
	'Saludos' msj,
	tg_personasubicaciones.pu_email correo, 
	j.id_juicio idjuicio
	,rd_fechacumplimiento
	,ra_plazo
	,datediff(d,getdate(),rd_fechacumplimiento) datediff_rd_fechacumplimiento
	FROM wf_remates AS r INNER JOIN
		ju_juicios AS j ON r.id_juicio = j.id_juicio INNER JOIN
		ju_tribunales AS t ON j.id_tribunal = t.id_tribunal INNER JOIN
		ju_juiciosetapas AS jet ON j.id_juicioetapa = jet.id_juicioetapa INNER JOIN
		ju_juiciosTipos AS jt ON j.id_juiciotipo = jt.id_juiciotipo INNER JOIN
		wf_rematesdetalles AS rd ON r.id_remate = rd.id_remate INNER JOIN
		wf_rematesetapas AS re ON rd.id_remateetapa = re.id_remateetapa INNER JOIN
		ju_juiciosEstados AS je ON j.id_juicioestado = je.id_juicioestado INNER JOIN
		wf_rematesalarmas ON re.id_remateetapa = wf_rematesalarmas.id_remateetapa INNER JOIN
		tg_personas AS pe ON wf_rematesalarmas.id_responsable = pe.id_persona INNER JOIN
		tg_personasubicaciones ON pe.id_persona = tg_personasubicaciones.id_persona
	where datediff(d,getdate(),rd_fechacumplimiento) <= ra_plazo
		and pu_esDeNotificacion = 1
		and re_fechafin is null
		and id_remateresultado = 1;
	if @@error <> 0 goto on_error; 



	declare @corr				int
	declare @corrMAX			int
	declare @NroInicial			int 
	declare @NroFinal			int
	declare @CORREO				varchar(8000)
	declare @MSJ				varchar(8000)

	select  @NroFinal = max(id), @NroInicial = min(id) 
	from @temporal 

	set     @corr  = @NroInicial

	WHILE ( @corr <= (@NroFinal) )
	begin

		select	@MSJ = msj,
				@CORREO = correo
		from @temporal
		where id = @corr;
		if @@error <> 0 goto on_error; 

		if @correo != '' begin
			Exec pa_clr_enviar_correo
			@config_Smtp = 'mail.plataformagroup.cl',
			@config_port = '25',
			@config_user = 'plataformajudicialbci@plataformagroup.cl',
			@config_pass = 'pcjbci$0270',
			@config_ssl = '0',
			@param_To = @CORREO,
			@param_Cc = 'eroco@plataformagroup.cl,ygonzalez@plataformagroup.cl',--'servicioalcliente@plataformagroup.cl',
			@param_From = 'plataformajudicialbci@plataformagroup.cl',
			@param_Subject = 'WorkFlow Alarmas Responsable',
			@param_body = @MSJ,
			@param_files = '';
			if @@error <> 0 goto on_error; 
		end
		/*---------------------MANEJO DE SALTOS DEL WHILE------------------*/
		if @corr = @NroFinal break

		/* LE ASIGNAMOS A LA VARIABLE @CORR EL SIGUIENTE VALOR VALIDO DEL JUICIO DEL CLIENTE */
		SELECT distinct top 1 @corr = id 
		from @temporal 
		where 1=1
		AND isnull(id,0) > @corr
		ORDER BY id;
		if @@error <> 0 goto on_error; 
	end

if @@trancount > 0 BEGIN
	commit tran
	print 'Proceso  correo de RESPONSABLE ALARMA ejecutado correctamente.'
	select 0, getdate() as fecha, DB_NAME() as base, SYSTEM_USER as Usuario, CURRENT_USER as usuario2 , 'Proceso  correo de RESPONSABLE ALARMA ejecutado correctamente.'
END

ON_ERROR:
If @@TRANCOUNT > 0 BEGIN
	ROLLBACK TRAN
	print 'Se Produjo un error en la transacción (aparatado de correo de RESPONSABLE)'
	select -1, getdate() as fecha, DB_NAME() as base, SYSTEM_USER as Usuario, CURRENT_USER as usuario2, 'Se Produjo un error en la transacción (aparatado de correo de RESPONSABLE)'
END

-------------------------------------------------------------CORREOS A SUPERVISORES
BEGIN TRAN
	declare @temporal_supervisor table (
		id int identity(1,1),
		msj  varchar(8000),
		correo  varchar(1000),
		idjuicio int
	)

	INSERT INTO @temporal_supervisor(msj,correo,idjuicio)
	SELECT 
		'Estimado, <br><br>'+
		'Se Informa que la Etapa del Workflow de Remates '''+ re.re_remateetapa + ''', esta a días de su cumplimiento, la fecha es el ' + 
		[dbo].[fn_ut_rellena_prefijos](cast(month(rd_fechacumplimiento) as varchar(12)),2,'0','izq') + '/' + 
		[dbo].[fn_ut_rellena_prefijos](cast(month(rd_fechacumplimiento) as varchar(12)),2,'0','izq') +'/' +
		cast(year(rd_fechacumplimiento) as varchar(12)) +', el detalle es el siguiente: <br>'+
		'<b>Juicio Rol</b>		: ' +  j.ju_rol + ' <br>'+
		'<b>Tribunal</b>		: '+ t.tr_tribunal + ' <br>'+
		'<b>Abogado</b>			: '+ j.ab_abogado + ' <br>'+
		'<b>Cliente Rut</b>		: ' + j.cl_rut + ' <br>'+
		'<b>Nombre Cliente</b>	: ' +j.cl_cliente +' <br><br>'+
		'Saludos' msj,
		tg_personasubicaciones.pu_email correo, 
		j.id_juicio idjuicio
		--,rd.id_rematedetalle
		--,rd_fechacumplimiento
		--,ra_plazosupervisor
		--,datediff(d,getdate(),rd_fechacumplimiento) datediff_rd_fechacumplimiento
	FROM            wf_remates AS r INNER JOIN
		ju_juicios AS j ON r.id_juicio = j.id_juicio INNER JOIN
		ju_tribunales AS t ON j.id_tribunal = t.id_tribunal INNER JOIN
		ju_juiciosetapas AS jet ON j.id_juicioetapa = jet.id_juicioetapa INNER JOIN
		ju_juiciosTipos AS jt ON j.id_juiciotipo = jt.id_juiciotipo INNER JOIN
		wf_rematesdetalles AS rd ON r.id_remate = rd.id_remate INNER JOIN
		wf_rematesetapas AS re ON rd.id_remateetapa = re.id_remateetapa INNER JOIN
		ju_juiciosEstados AS je ON j.id_juicioestado = je.id_juicioestado INNER JOIN
		wf_rematesalarmas ON re.id_remateetapa = wf_rematesalarmas.id_remateetapa  INNER JOIN
		tg_personas AS ps ON wf_rematesalarmas.id_supervisor = ps.id_persona INNER JOIN
		tg_personasubicaciones ON ps.id_persona = tg_personasubicaciones.id_persona
	where 1=1
		and datediff(d,getdate(),rd_fechacumplimiento) <= ra_plazosupervisor
		and pu_esDeNotificacion = 1
		and id_remateresultado = 1
		and re_fechafin is null;
	if @@error <> 0 goto ON_ERROR_SUPERVISOR; 

	declare @corr_supervisor				int
	declare @corrMAX_supervisor				int
	declare @NroInicial_supervisor			int 
	declare @NroFinal_supervisor			int
	declare @CORREO_supervisor				varchar(8000)
	declare @MSJ_supervisor					varchar(8000)

	select  @NroFinal_supervisor = max(id), @NroInicial_supervisor = min(id) 
	from @temporal_supervisor 

	set     @corr_supervisor  = @NroInicial_supervisor

	WHILE ( @corr_supervisor <= (@NroFinal_supervisor) )
	begin

		select	@MSJ = msj,
				@CORREO = correo
		from @temporal_supervisor
		where id = @corr_supervisor;
		if @@error <> 0 goto ON_ERROR_SUPERVISOR;

		if @correo != '' begin
			Exec pa_clr_enviar_correo
			@config_Smtp = 'mail.plataformagroup.cl',
			@config_port = '25',
			@config_user = 'plataformajudicialbci@plataformagroup.cl',
			@config_pass = 'pcjbci$0270',
			@config_ssl = '0',
			@param_To = @CORREO,
			@param_Cc = 'eroco@plataformagroup.cl,ygonzalez@plataformagroup.cl',--'servicioalcliente@plataformagroup.cl',
			@param_From = 'plataformajudicialbci@plataformagroup.cl',
			@param_Subject = 'WorkFlow Alarmas Supervisor',
			@param_body = @MSJ,
			@param_files = '';
			if @@error <> 0 goto on_error; 
		end

		/*---------------------MANEJO DE SALTOS DEL WHILE------------------*/
		if @corr_supervisor = @NroFinal_supervisor break

		/* LE ASIGNAMOS A LA VARIABLE @CORR EL SIGUIENTE VALOR VALIDO DEL JUICIO DEL CLIENTE */
		SELECT distinct top 1 @corr = id 
		from @temporal_supervisor 
		where 1=1
		AND isnull(id,0) > @corr_supervisor
		ORDER BY id;
		if @@error <> 0 goto ON_ERROR_SUPERVISOR;
	end


if @@trancount > 0 BEGIN
	commit tran
	PRINT 'Proceso  correo de SUPERVISOR ALARMA ejecutado correctamente.'
	select 0, getdate() as fecha, DB_NAME() as base, SYSTEM_USER as Usuario, CURRENT_USER as usuario2 , 'Proceso  correo de SUPERVISOR ALARMA ejecutado correctamente.'
END

ON_ERROR_SUPERVISOR:
If @@TRANCOUNT > 0 BEGIN
	ROLLBACK TRAN
	print 'Se Produjo un error en la transacción (aparatado de correo de SUPERVISOR)'
	select -1, getdate() as fecha, DB_NAME() as base, SYSTEM_USER as Usuario, CURRENT_USER as usuario2, 'Se Produjo un error en la transacción (aparatado de correo de SUPERVISOR)'
END

-----CORREO a responsable del detalle