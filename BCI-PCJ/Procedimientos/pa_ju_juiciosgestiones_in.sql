IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa_ju_juiciosgestiones_in]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa_ju_juiciosgestiones_in]
GO

CREATE PROCEDURE [dbo].[pa_ju_juiciosgestiones_in]
@jg_juiciogestion				as varchar(5000) = '',
@jg_fecha						as smalldatetime = null,
@jg_fechaingreso				as smalldatetime = null,
@jg_usuario						as varchar(20) = '',
@id_juicio						as int = 0,
@id_gestion						as int = 0,
@jg_esactiva					as bit = 0,
@id_persona						as int = 0,
@jg_montogasto					as money = 0,
@id_juicioetapa					as int = 0,
@id_juicioetapa_juicio			as int = 0,
@je_juicioetapa_juicio			varchar(500)	 = '',
@je_juicioetapa_gestion			varchar(500)	 = '',
@id_personaubicacion			AS INT = 0,
@jg_esdenotificacion			as int = 0
/*
Creador por : ds-program9
Fecha Creación: 27/11/2007 18:27:10
Procedimiento:pa_ju_juiciosgestiones_in
Modificado por: Jessica Coloma
Fecha Modificación: 20 Oct 2010
llamada de test     :  
begin tran
    pa_ju_juiciosgestiones_in '',null,null,'',0,0,0
		pa_ju_juiciosgestiones_in 'Prueba exhorto','30-11-2008','10-11-2008','Flavio',64818,1526,1,0,0
		select * from ju_juicios where id_juicio = 64818
	select * from ju_juiciosgestiones where id_juicio = 64818 order by jg_fecha desc
	select * from ju_gestiones
rollback
anulartest : ROLLBACK(TRAN)
Resultado: Procedimiento de inserción de un nuevo registro en la tabla ju_juiciosgestiones

alter table ju_juicios add ju_fecnotificacionsistema smalldatetime 
*/

as



declare @ga_gastos table
    (
	   norden int identity(1,1)
      ,id_juicio int
      ,id_gestion int
      ,id_gastotipo int
	  ,id_gasto int	
    )


set nocount on
declare @id_juiciogestion as int
declare @id_gastojg as int
declare @id_marca_notificacion as int
declare @id_marca_embargo as int
declare @id_marca_remate as int
declare @id_gestion_marca as int
declare @CONST_Solicitud_Ingresada as int
declare @CONST_Solicitud_Enviada as int
declare @CONST_Solicitud_Cerrada as int
declare @TIPO_GESTION_JUDICIAL as int


select  @id_marca_notificacion = dbo.fn_ut_constantes('MARCA_NOTIFICAR'),
        @id_marca_embargo = dbo.fn_ut_constantes('Marca_gestion_Embargo'),
        @id_marca_remate = dbo.fn_ut_constantes('MARCA_remate_bien'),
		@CONST_Solicitud_Ingresada = dbo.fn_ut_constantes('CONST_Solicitud_Ingresada'),
		@CONST_Solicitud_Enviada = dbo.fn_ut_constantes('CONST_Solicitud_Enviada'),
		@CONST_Solicitud_Cerrada = dbo.fn_ut_constantes('CONST_Solicitud_Cerrada'),
		@TIPO_GESTION_JUDICIAL = dbo.fn_ut_constantes('TIPO_GESTION_JUDICIAL')
		
begin tran
INSERT INTO ju_juiciosgestiones
            (
            jg_juiciogestion,
            jg_fecha,
            jg_fechaingreso,
            jg_usuario,
            id_juicio,
            id_gestion,
            jg_esactiva,
            id_persona,
            jg_montogasto,
			id_juicioetapa,
			id_juicioetapa_juicio
            )
            Values
            (
            @jg_juiciogestion,
            @jg_fecha,
            @jg_fechaingreso,
            @jg_usuario,
            @id_juicio,
            @id_gestion,
            @jg_esactiva,
			@id_persona,
            @jg_montogasto,
			@id_juicioetapa,
			@id_juicioetapa_juicio
            )

if @@error <> 0 goto on_error 
select @id_juiciogestion = ident_current('ju_juiciosgestiones')


--actualizo gestion con un gasto
if exists(select 1
		  from ga_gastos 
		  where ga_flgfuerastandar=1 and
                id_gestion = @id_gestion and
				id_juicioingreso = @id_juicio and
				--not exists (select id_gasto from ju_juiciosgestiones where id_juicio=@id_juicio and not id_gasto is null))
				not id_gasto in (select id_gasto from ju_juiciosgestiones where id_juicio=@id_juicio and not id_gasto is null))
begin

	select top 1 @id_gastojg = id_gasto
    from ga_gastos 
    where ga_flgfuerastandar=1 and
          id_gestion = @id_gestion and
	  	  id_juicioingreso = @id_juicio and
	  	  id_gastoestado in (1,2,3,6) and 
          --not id_gastoestado in (5,4) and 
          --not exists (select id_gasto from ju_juiciosgestiones where id_juicio=@id_juicio and not id_gasto is null)
		  not id_gasto in (select id_gasto from ju_juiciosgestiones where id_juicio=@id_juicio and not id_gasto is null)

	if isnull(@id_gastojg,0)<>0
		update ju_juiciosgestiones
			set id_gasto = @id_gastojg
		where id_juicio = @id_juicio and
			  id_gestion = @id_gestion and
			  id_gasto is null
end
if @@error <> 0 goto on_error 

--EROCO 02-05-2018 DIASSINGESTION TOMARA FECHAS DE GESTIONES OPERACIONES Y JUDICIALES
-- Actualiza el id de la última gestion a partir de lo que se ingresó 
-- incluye gestiones operacionales y judiciales
-- actualiza fecha de la ultima gestion judicial 
update ju_juicios 
	set 
		id_gestion = k.id_gestion,
		id_juiciogestion = k.id_juiciogestion,
		ju_fechaugestion = k.jg_fecha
	from 
		( select top 1 jg_fecha,ju_juiciosgestiones.id_gestion,id_juiciogestion 
				FROM         ju_juiciosgestiones INNER JOIN
									  ju_gestiones ON ju_juiciosgestiones.id_gestion = ju_gestiones.id_gestion
				WHERE     (ju_juiciosgestiones.id_juicio = @id_juicio)
				ORDER BY ju_juiciosgestiones.jg_fecha DESC, ju_juiciosgestiones.id_juiciogestion DESC 
		) as k
	where id_juicio = @id_juicio
if @@error <> 0 goto on_error 


--EROCO 02-05-2018 DIASSINGESTION RESPALDO DEL ANTIGUO RESCATE DE FECHAS DE ULTIMA GESTION Y ID_GESTION
-- Actualiza el id de la última gestion a partir de lo que se ingresó 
-- incluye gestiones operacionales y judiciales
/*
update ju_juicios 
	set 
		id_gestion = k.id_gestion,
		id_juiciogestion = k.id_juiciogestion
	from 
		( select top 1 ju_juiciosgestiones.id_gestion,id_juiciogestion 
				FROM         ju_juiciosgestiones INNER JOIN
									  ju_gestiones ON ju_juiciosgestiones.id_gestion = ju_gestiones.id_gestion
				WHERE     (ju_juiciosgestiones.id_juicio = @id_juicio)
				ORDER BY ju_juiciosgestiones.jg_fecha DESC, ju_juiciosgestiones.id_juiciogestion DESC 
		) as k
	where id_juicio = @id_juicio
if @@error <> 0 goto on_error 

-- actualiza fecha de la ultima gestion judicial 
update ju_juicios 
	set 
		ju_fechaugestion = k.jg_fecha
	from 
		( select top 1 jg_fecha,ju_juiciosgestiones.id_gestion,id_juiciogestion 
				FROM         ju_juiciosgestiones INNER JOIN
									  ju_gestiones ON ju_juiciosgestiones.id_gestion = ju_gestiones.id_gestion
				WHERE     (ju_juiciosgestiones.id_juicio = @id_juicio and ju_gestiones.id_gestiontipo = @TIPO_GESTION_JUDICIAL )
				ORDER BY ju_juiciosgestiones.jg_fecha DESC, ju_juiciosgestiones.id_juiciogestion DESC 
		) as k
	where id_juicio = @id_juicio
if @@error <> 0 goto on_error 
*/

-- actualiza el campo de fecha correspondiente en el juicio, dependiendo de la marca de la gestion, 
-- sin importar la etapa judicial en que se ingrese
	select @id_gestion_marca = id_marca 
	from ju_gestionesmarcas
	where id_gestion = @id_gestion
	
	if (@id_gestion_marca = @id_marca_notificacion)
		begin
			Update ju_juicios 
			set ju_fecnotificacion = @jg_fecha,
				ju_fecnotificacionsistema = @jg_fechaingreso
			where id_juicio = @id_juicio
		end

	else if (@id_gestion_marca = @id_marca_embargo)
		begin
			Update ju_juicios 
			set ju_fecembargo = @jg_fecha
			where id_juicio = @id_juicio
		end

	else if (@id_gestion_marca = @id_marca_remate)
		begin
			Update ju_juicios 
			set ju_fecuremate = @jg_fecha
			where id_juicio = @id_juicio
		end



-- actualiza gastos
if exists (select 1
		   from ga_gastos g inner join 
				ga_solicitudes s on g.id_solicitud = s.id_solicitud
		   where id_juicioingreso=@id_juicio
				 and id_gestion=@id_gestion
				 and ga_flgfuerastandar=1
				 and (id_solicitudestado = @CONST_Solicitud_Ingresada
					  or id_solicitudestado = @CONST_Solicitud_Enviada
					  or id_solicitudestado = @CONST_Solicitud_Cerrada))
begin

    insert into @ga_gastos
		   (
			 id_juicio
			,id_gestion
			,id_gastotipo
			,id_gasto
			)
	select	@id_juicio,
			@id_gestion,
			id_gastotipo,
			id_gasto
	from ga_gastos g inner join 
		 ga_solicitudes s on g.id_solicitud = s.id_solicitud
	where (id_solicitudestado = @CONST_Solicitud_Ingresada or id_solicitudestado = @CONST_Solicitud_Enviada or id_solicitudestado = @CONST_Solicitud_Cerrada)
		   and id_juicioingreso=@id_juicio	
		   and id_gestion=@id_gestion	
		   and ga_flgfuerastandar=1
    		   
	if @@error <> 0 goto ON_ERROR

	declare @corr       int
	declare @NroInicial int , @NroFinal   int
	declare @ga_flgfuerastandar as bit
	declare @ga_causalfuerastandar as varchar(300)
    declare @id_gasto int
	declare @id_gastotipo int
	declare @id_juiciotramo int
	declare @gtt_cantidad int, @gtt_nummaximo int
	declare @gtt_montomaximo decimal(18,2), @ga_montosuma decimal(18,2)
	select  @NroFinal   = max(norden), @NroInicial = min(norden) from  @ga_gastos
	set     @corr       = @NroInicial
	---------------------------------------------------------------
	WHILE ( @corr <= (@NroFinal) )
	begin
	   if  exists ( select norden from  @ga_gastos  where norden = @corr )
	   BEGIN 
	

		   --Obtengo tramo del juicio
		   select @id_juiciotramo = id_juiciotramo
		   from ju_juicios
		   where id_juicio = @id_juicio

			select @id_gasto = id_gasto,
				   @id_gastotipo = id_gastotipo
			from @ga_gastos
			where norden = @corr 

		 			
		   select @ga_montosuma = sum(isnull(ga_monto,0)),	
				  @gtt_cantidad = count(id_juicioingreso)
		   from ga_gastos	
		   where id_juicioingreso = @id_juicio and 
				 id_gastotipo = @id_gastotipo and
				 id_gastoestado <> 5

		   if isnull(@ga_montosuma,0)=0
			  set @ga_montosuma = 0
	
		   if isnull(@gtt_cantidad,0)=0
			  set @gtt_cantidad = 0

		   --Obtengo los valores maximos para este tipo de gasto dependiendo del tramo del juicio en particular
		   select @gtt_montomaximo = gtt_montomaximo,
				  @gtt_nummaximo = gtt_nummaximo
		   from ga_gastostipostramos
		   where id_gastotipo = @id_gastotipo and
 				 id_juiciotramo = @id_juiciotramo

		   --Verifico si existe motivo para que el gasto este fuera de estandar    
		   if (@ga_montosuma > @gtt_montomaximo) and (@gtt_cantidad > @gtt_nummaximo)
			  set @ga_causalfuerastandar = 'El gasto esta fuera de estandar por la cantidad de gastos de este tipo y por el monto permitido para el juicio.' 	   	

		   if (@ga_montosuma <= @gtt_montomaximo) and (@gtt_cantidad <= @gtt_nummaximo)
			  set @ga_causalfuerastandar = ''  

		   if (@ga_montosuma > @gtt_montomaximo) and (@gtt_cantidad <= @gtt_nummaximo)	
			  set @ga_causalfuerastandar = 'El gasto esta fuera de estandar porque supera el monto permitido para este juicio y este tipo de gasto.'
		 
		   if (@ga_montosuma <= @gtt_montomaximo) and (@gtt_cantidad > @gtt_nummaximo)	
			  set @ga_causalfuerastandar = 'El gasto esta fuera de estandar por la cantidad de gastos de este tipo para el juicio ingresado.'

		   if @ga_causalfuerastandar=''
			  set @ga_flgfuerastandar=0
		   else
	          set @ga_flgfuerastandar=1

		   --actualizo el gasto
		   if  exists(select 1 
		  		      from ju_juiciosgestiones
					  where id_juicio = @id_juicio and
							id_gestion = @id_gestion)
				update ga_gastos
				set ga_flgfuerastandar = @ga_flgfuerastandar,
					ga_causalfuerastandar = @ga_causalfuerastandar			
				where id_gasto = @id_gasto	

			if @@error <> 0 goto ON_ERROR
	   END
	---------------------------------------------------------------

	set @corr = @corr + 1
	---------------------------------------------------------------
	end

end

/*EROCO 07-12-2017 SE ANALIZA LA MARCA DE LA GESTION PARA SABER SI ES DE NOTIFICACION O BUSQUEDA NEGATIVA*/
DECLARE @CUENTA_MARCA AS INT

SELECT @CUENTA_MARCA = COUNT(1) FROM JU_GESTIONESMARCAS WHERE ID_MARCA IN (6,21) AND ID_GESTION = @ID_GESTION

IF @CUENTA_MARCA = 1 BEGIN

	DECLARE @CUENTA_MARCA_NOTIFICACION AS INT
	DECLARE @CUENTA_MARCA_NEGATIVA AS INT
	DECLARE @V_ID_JUICIOGESTION AS INT

	SELECT @CUENTA_MARCA_NEGATIVA = COUNT(1) FROM JU_GESTIONESMARCAS WHERE ID_MARCA IN (21) AND ID_GESTION = @ID_GESTION
	SELECT @CUENTA_MARCA_NOTIFICACION = COUNT(1) FROM JU_GESTIONESMARCAS WHERE ID_MARCA IN (6) AND ID_GESTION = @ID_GESTION

	select @V_ID_JUICIOGESTION = ident_current('ju_juiciosgestiones')

	--SE ACTUALIZA SI ES DE NOTIFICACION
	DECLARE @ES_NOTIFICACION AS INT
	
	IF @CUENTA_MARCA_NEGATIVA = 1 BEGIN
		SET @ES_NOTIFICACION = 0
	END

	IF @CUENTA_MARCA_NOTIFICACION = 1 BEGIN
		SET @ES_NOTIFICACION = 1
		
		--SE ACTUALIZA LA FECHA DE ULTIMA NOTIFICACION QUE SE USO LA DIRECCION
		UPDATE tg_personasubicaciones SET pu_fechaunotificacion = GETDATE()  where id_personaubicacion = @id_personaubicacion

		UPDATE tg_personasubicaciones SET pu_esdenotificacion = 0 where id_persona = @id_persona and not id_personaubicacion = @id_personaubicacion
		UPDATE tg_personasubicaciones SET pu_esdenotificacion = 1 where id_personaubicacion = @id_personaubicacion

	END

	--SE ACTUALIZA EL ID_PERSONAUBICACION SELECCIONADO EN EL FORMULARIO Y SE DEJA MARCA SI ES DE NOTIFICACION
	UPDATE	ju_juiciosgestiones 
	SET		id_personaubicacion = @id_personaubicacion,
			jg_esdenotificacion = @ES_NOTIFICACION  
	where	id_juiciogestion = @V_ID_JUICIOGESTION

END

/*EROCO 20-08-2018 SE ANALIZA SI LA GESTION INGRESADA, ES LA QUE GATILLA EL WORKFLOW*/
declare @id_gestion_workflow as int


select @id_gestion_workflow = ct_constantevalor from tg_constantes where ct_constante = 'ID_GESTION_WORKFLOW'

if @id_gestion_workflow = @id_gestion begin

	declare @id_remateetapa as int
	declare @id_responsable as int
	declare @id_remate as int
	declare @id_rematedetalle as int
	declare @cuenta_workflow_activo as int
	declare @id_personaConectada as int
	--REVISO SI EL JUICIO TIENE UN WORKFLOW ACTIVO
	select @cuenta_workflow_activo = count(1) from wf_remates where id_juicio = @id_juicio and re_fechafin is null

	if @cuenta_workflow_activo = 0 begin
		declare @id_abogado					as int
		declare @id_controlador				as int
		declare @sin_abogado				as int
		declare @sin_controlador			as int

		--OBTEBER LO DEL JUICIO
		select @id_abogado = id_abogado, @id_controlador = id_controlador from ju_juicios where id_juicio = @id_juicio

		set		@sin_abogado		= dbo.fn_ut_constantes('ID_PERSONA_SINABOGADO')
		set		@sin_controlador	= dbo.fn_ut_constantes('ID_PERSONA_SININFORMACION')

		--OBTENGO LA ETAPA MAS PEQUEÑA
		SELECT top 1	@id_remateetapa = id_remateetapa,
						@id_responsable =   case when wf_rematesetapas.id_responsable = @sin_abogado then @id_abogado 
											else case when  wf_rematesetapas.id_responsable = @sin_controlador 
											then @id_controlador 
											else wf_rematesetapas.id_responsable 
											end END
		FROM WF_REMATESETAPAS WHERE re_esvigente = 1 
		ORDER BY RE_ORDEN ASC

		select top 1 @id_personaConectada = id_persona 
		from ca_usuarios where ltrim(rtrim(lower(us_consuser))) = ltrim(rtrim(lower(@jg_usuario)))

		--SE INICIA WORKFLOW
		insert into wf_remates(
			re_remate,
			id_juicio,
			re_fechasolicitud,
			id_persona,
			id_remateetapa,
			re_usuario,
			re_ultimocomentario
		)
		values(
			'Remate',
			@id_juicio,
			getdate(),
			@id_personaConectada,
			@id_remateetapa,
			@jg_usuario,
			substring(@jg_juiciogestion,0,50)
		)

		select top 1 @id_remate = id_remate from wf_remates where id_juicio = @id_juicio order by id_remate desc;

		insert into wf_rematesdetalles(
			id_remateetapa,
			id_responsable,
			id_remate,
			rd_fechaetapaingreso,
			rd_fechaetapa,
			id_remateresultado,
			id_personagenerador,
			rd_comentario
		)
		values(
			@id_remateetapa,
			@id_responsable,
			@id_remate,
			getdate(),
			getdate(),
			1, --PENDIENTE
			@id_personaConectada,
			substring(@jg_juiciogestion,0,200)
		)

		select @id_rematedetalle = id_rematedetalle from wf_rematesdetalles where id_remate = @id_remate order by id_rematedetalle desc;
		
		update wf_remates set id_rematedetalle = @id_rematedetalle where id_remate = @id_remate;

		/*INSERTO BITACORA AL JUICIO Y AL REMATE PARA LOS COMENTARIOS*/
		DECLARE @FECHAHOY									AS SMALLDATETIME
		DECLARE @cuerpo_bitacora							AS VARCHAR(1000)	= ''
		DECLARE @param_fecha								AS VARCHAR(100)		

		set @param_fecha = (select replace(convert(varchar, getdate(), 103),'/','-')); --dd-mm-yyyy
		set @FECHAHOY = CAST(GETDATE() AS SMALLDATETIME)

		/*SE GENERA BITACORA AL JUICIO*/
		SET @cuerpo_bitacora = 'WorkFlow Inicio : ' + @jg_juiciogestion

		EXEC DBO.pa_bi_bitacoras_automaticas_notifica_WORKFLOW
		@bi_bitacora		= @cuerpo_bitacora,
		@bi_fecha			= @FECHAHOY,
		@id_persona			= @id_personaConectada, --(por defecto SISTEMA)
		@bi_esautomatico	= 1, --(por defecto)
		@bi_msg_user		= @jg_usuario, --Usuario conectado al sistema
		@bi_msg_fecha		= @FECHAHOY,
		@id_origen			= @id_juicio,
		@bi_usuario			= @jg_usuario, --Usuario conectado al sistema
		@bi_confidencial	= 0,
		@id_bitacorapadre	= null,
		@id_bitacoratipo	= 25,
		@id_remate			= @id_remate


		--CORREO AL NUEVO RESPONSABLE O AL ANTIGUO
		DECLARE @CORREO AS VARCHAR(8000) = ''
		DECLARE @MSJ AS VARCHAR(8000) = ''
		DECLARE @ju_rol AS VARCHAR(100) = ''
		DECLARE @tr_tribunal AS VARCHAR(100) = ''
		DECLARE @cl_rut AS VARCHAR(100) = ''
		DECLARE @cl_deudor AS VARCHAR(100) = ''

		SELECT @CORREO = PU_EMAIL FROM TG_PERSONASUBICACIONES 
		WHERE ID_PERSONA = @ID_RESPONSABLE 
		AND PU_ESDENOTIFICACION = 1

		IF @CORREO != '' BEGIN

			select	@ju_rol = ltrim(rtrim(ju_rol)),
					@tr_tribunal = tr_tribunal,
					@cl_rut = cl_rut,
					@cl_deudor = cl_cliente
			from ju_juicios
			inner join ju_tribunales on ju_tribunales.id_tribunal = ju_juicios.id_tribunal
			where id_juicio = @id_juicio

			SET @MSJ += 'Estimado <br><br>'
			SET @MSJ += 'Se ha generado la siguiente actuación en el Workflow de remate del juicio : <br><br>'
			SET @MSJ += '<b>Juicio :</b> ' + @ju_rol + '<br>'
			SET @MSJ += '<b>Tribunal :</b> ' + @tr_tribunal + '<br>'
			SET @MSJ += '<b>Deudor :</b> ' + @cl_deudor + '<br>'
			SET @MSJ += '<b>Rut :</b> ' + @cl_rut + '<br>'
			SET @MSJ += '<b>Actuación :</b> ' + substring(@cuerpo_bitacora,0,7700) + ' <br><br>'
			SET @MSJ += 'Saludos'

			Exec pa_clr_enviar_correo
			@config_Smtp = 'mail.plataformagroup.cl',
			@config_port = '25',
			@config_user = 'plataformajudicialbci@plataformagroup.cl',
			@config_pass = 'pcjbci$0270',
			@config_ssl = '0',
			@param_To = @CORREO,
			@param_Cc = 'eroco@plataformagroup.cl,ygonzalez@plataformagroup.cl,cmunoz@plataformagroup.cl',--'servicioalcliente@plataformagroup.cl',
			@param_From = 'plataformajudicialbci@plataformagroup.cl',
			@param_Subject = 'WorkFlow Actuación',
			@param_body = @MSJ,
			@param_files = ''
			
		END
	end 
end


-- control de errores
if @@error <> 0 goto on_error 
select @id_juiciogestion = ident_current('ju_juiciosgestiones')

if @@trancount > 0
commit tran
select @id_juiciogestion id_juiciogestion,''
return 0

ON_ERROR:
If @@TRANCOUNT > 0
ROLLBACK TRAN
select -1, 'Se Produjo un error en la transacción y se abortó'
return -1

Error_Params:
select -2, 'Problemas con los parámetros '
return -2























