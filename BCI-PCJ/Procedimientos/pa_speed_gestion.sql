IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa_speed_gestion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa_speed_gestion]
GO

CREATE PROCEDURE [dbo].[pa_speed_gestion]
@id_juicio		as int = 0,
@usuario		as varchar(50) = '',
@id_opcion		as int = 0

/*
Creador por			:	EMANUEL ROCO RIVAS
Fecha Creacion		:	31-07-2018

Modificado por		:	EROCO
Fecha Modificacion	:	03/09/2019
Comentario mod.		:   SE QUITA TODO EL CAMINO 2 'DELEGA PODER' YA QUE LA GESTION DE DELEGA PODER AHORA NO HACE NADA


llamada de test : rollback
anulartest		: ROLLBACK(TRAN)
*/

as

Begin tran

/*

/*31-07-2018*/
PROPUESTA Proyecto Speed ET v1.docx 

•	Ingreso de gestión “Mandamiento”
	o	Se creará un proceso online (oculto) que realiza la asignación automática sobre los abogados SPEED que participarán del piloto (se define el abogado que proseguirá con la causa), esta asignación será solo a nivel de utilización de cuotas.

•	Ingreso de gestión  “Delega poder”
	o	Se realiza la reasignación del abogado definitivo en el sistema quedando disponible esta información en la Plataforma.
	o	Se genera bitácora al nuevo abogado avisándole que se le delego un nuevo juicio.


5.5	Asignación Automática
Hay que replicar la lógica de la asignación automática de la Ficha de asignación, esta se basa en el SP llamado [pa_op_in_asignacion_automatica]

5.6	Bitácoras
Una vez se realice la asignación automática, hay que generar una bitácora en el juicio informando el cambio de abogado


/*04/09/2019*/
G:\2019\Proyectos\Aprobados\BCI\Proyecto Speed - Marca y Otros\Documentación tecnica\Proyecto Speed Marca y Otros ET v1.DOCX

3.2 3.2.	Ingreso de Gestiones (Marca Speed)
Otro de los cambios que ha solicitado el cliente tiene que ver con las gestiones “Mandamiento” y “Delega poder”, tal como se indicó, estas gestiones gatillan el cambio de abogado (sin concretar) y su asignación final respectivamente avisan vía bitácoras al nuevo abogado y su Staff.
Lo que se requiere en particular es que al ingresar la primera gestión llamada “Mandamiento” ya no se requiera el ingreso de la gestión “Delega poder” para la asignación final y aviso, sino que el sistema genere la asignación de manera automática después de pasados X días desde la fecha de inicio del juicio, X será un parámetro que el usuario podrá modificar y que depende del tipo de operación (más adelante se explicara bien este punto). Sí la gestión “Mandamiento” se ingresa posterior a los X días inicialmente definidos, se asignará de manera automática el abogado definitivo.
Dado lo anterior, la lógica actual cambia y la gestión “Delega Poder”  deja de ser relevante en este proyecto y no tendrá ninguna acción asociada, por lo tanto no se gatillará ningún proceso al momento de ser ingresada por los usuarios en los juicios SPEED.

*/

/*VARIABLES*/
DECLARE @cuerpo_bitacora				AS VARCHAR(1000) = ''
DECLARE @es_juicioSpeed					AS INT
DECLARE @id_grupoasignacion				AS INT
DECLARE @FECHAHOY						AS SMALLDATETIME = NULL
DECLARE @id_gestion_mandamiento			as int
DECLARE @cuenta_gestion_mandamiento		as int = 0
DECLARE @v_id_gestion_delegapoder		as int
DECLARE @cuenta_gestion_delegapoder		as int = 0
DECLARE @v_id_gestion_mandamiento		as int = 0
DECLARE @id_pagare						as int = 0

declare @id_abogadospeed				int
DECLARE @id_AbogadoNuevo				AS INT
DECLARE @ab_AbogadoNuevo				AS VARCHAR(200) = ''
DECLARE @id_Abogado						AS INT
DECLARE @ab_AbogadoAnterior				AS VARCHAR(200) = ''

/*VERIFICO ANTES QUE NADA SI EL JUICIO TIENE OPERACIONES SPEED*/
SELECT @es_juicioSpeed = DBO.fn_retorna_si_es_juicio_speed(@id_juicio)
set @FECHAHOY = CAST(GETDATE() AS SMALLDATETIME)
if @@error <> 0 goto ON_ERROR

/*Verifico si el juicio el abogado es el predeterminado*/
set @id_abogadospeed = DBO.fn_ut_parametro_constante('ID_ABOGADOCARGO_SPEED')
select @id_Abogado = id_abogado from ju_juicios where id_juicio = @id_juicio

IF @es_juicioSpeed > 0 and @id_Abogado = @id_abogadospeed BEGIN
	IF @id_opcion = 1 BEGIN 
	/*GESTION MANDAMIENTO*/

		/*ESTO PODRIA SER UN PARAMETRO PARA QUE CUANDO CREEN OTRO GRUPO DE ASIGNACION NO TOME LA ULTIMA CREADA POR AHORA QUEDARA ASI. CON UN SELECT AL DEL TIPO SPEED*/
		select top 1 @id_grupoasignacion = id_grupoasignacion from dbo.do_gruposAsignacion where ag_flgesSpeed = 1

		--- OBTENER LA OPERACION SPEED DEL JUICIO
		--- ONTERNER EL ULTIMO PAGARE DE LA OPERACION DEL JUICIO
		SELECT top 1 @id_pagare = id_pagare 
		FROM JU_JUICIOS 
		INNER JOIN OP_OPERACIONESJUICIOS ON OP_OPERACIONESJUICIOS.ID_JUICIO = JU_JUICIOS.ID_JUICIO
		INNER JOIN OP_OPERACIONES ON OP_OPERACIONES.ID_OPERACION = OP_OPERACIONESJUICIOS.ID_OPERACION
		INNER JOIN OP_OPERACIONESPAGARES ON OP_OPERACIONESPAGARES.ID_OPERACION = OP_OPERACIONES.ID_OPERACION
		WHERE ju_juicios.ID_JUICIO = @ID_JUICIO
		--AND substring(lower(ltrim(rtrim(isnull(OP_HITO,'')))),1,5) = 'speed'
		AND [dbo].[fn_retorna_si_es_operacion_speed](op_operaciones.id_operacion) = 1
		order by id_pagare desc

		/*ASIGNACION AUTOMATICA*/
		-- CAMBIAR EL ID_JUICIO X EL ID_PAGARE
		EXEC pa_op_in_asignacion_automatica_juicio_gestion_speed @id_pagare = @id_pagare, @id_juicio = @id_juicio, @user = @usuario, @id_grupoasignacion = @id_grupoasignacion
		if @@error <> 0 goto ON_ERROR

		/*EROCO AHORA CON LA GESTION SE DEBE OBTENER EL PLAZO MENOR DE LAS OPERACIOENS SPEED QUE TIENE EL JUICIO*/
		declare @plazo as int = 0
		select @plazo = [dbo].[fn_retorna_plazo_juicio_speed](@id_juicio)
		if @@error <> 0 goto ON_ERROR

		/*EROCO 04/09/2019 SE PEGA EL PLAZO A REASIGNAR ESTE SERVIRA PARA EL JOB DE SPEED*/
		update ju_juicios set ju_fechaareasignarspeed = isnull(ju_Fechaini,getdate()) + @plazo where id_juicio = @id_juicio;
		if @@error <> 0 goto ON_ERROR

		/*
		select @ab_AbogadoNuevo = ab_abogadoSpeed from ju_juicios where id_juicio = @id_juicio

		/*SE GENERA BITACORA*/
		SET @cuerpo_bitacora = 'El Juicio será reasignado al Abogado: ' + @ab_AbogadoNuevo + '.'
		EXEC [pa_bi_bitacoras_automaticas_x_juicios_participantes_in]
		@bi_bitacora		= @cuerpo_bitacora,
		@bi_fecha			= @FECHAHOY,
		@id_persona			= 9, --(por defecto SISTEMA)
		@bi_esautomatico	= 1, --(por defecto)
		@bi_msg_user		= @usuario, --Usuario conectado al sistema
		@bi_msg_fecha		= @FECHAHOY,
		@id_origen			= @id_juicio,
		@bi_usuario			= @usuario, --Usuario conectado al sistema
		@bi_confidencial	= 0,
		@id_bitacorapadre	= null,
		@id_bitacoratipo	= 25
		if @@error <> 0 goto ON_ERROR 
		*/
	END
	ELSE IF @id_opcion = 2 BEGIN 
		/*GESTION DELEGA PODER*/
		print 'AHORA ACA NO SE HACE NADA, SE PASO TODO AL JOB'
	END
END

-- control de errores
if @@trancount > 0
commit tran
return 0

ON_ERROR:
If @@TRANCOUNT > 0
ROLLBACK TRAN
select -1, 'Se Produjo un error en la transacción y se abortó'
return -1

Error_Params:
select -2, 'Problemas con los parámetros '
return -2























