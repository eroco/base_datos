USE [PJBCI_INT]
GO
/****** Object:  StoredProcedure [dbo].[pa_ju_juicios_seguimientos_no_new]    Script Date: 12/09/2019 15:02:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[pa_ju_juicios_seguimientos_no_new]
@pagina			int = 1,
@regpag			int = 15,
@criterio			int = 1, 
@criterioname			varchar(50) = 'id_bitacora', 
@criterioorden			int = 0, 
@id_juicio			int	 = 0,
@tipo			varchar(50)	 = '',
@jg_fecha			varchar(20)	 = null,
@jg_fecha_hasta			varchar(20)	 = null,
@id_gestion			int	 = 0,
@ge_gestion			varchar(100)	 = '',
@usr			varchar(50) = '', 
@f_carga			varchar(20)	 = null,
@f_carga_hasta			varchar(20)	 = null

/*
Creador por         : DS-PROGRAM11
Fecha Creación      : 23/05/2008 16:04:52
Procedimiento       : dbo.pa_ju_juicios_seguimientos_no
Modificado por      : 
Fecha Modificación  : 
llamada de test     : 
                      dbo.pa_ju_juicios_seguimientos_no @pagina, @regpag, @criterio, @criterioname, @criterioorden@id_juicio,@tipo,@jg_fecha,@id_gestion,@ge_gestion

dbo.pa_ju_juicios_seguimientos_no_new @pagina=1, @regpag=40, @criterioorden=1, @criterioname=jg_fecha, @id_juicio=41327, @id_gestion=0, @ge_gestion='', @jg_fecha='', @jg_fecha_hasta='', @tipo=''
pa_ju_juicios_seguimientos_no_jg_fecha_desc  @pagina=1, @regpag=40, @criterioorden=1, @criterioname=jg_fecha, @id_juicio=41327, @id_gestion=0, @ge_gestion='', @jg_fecha='', @jg_fecha_hasta='', @tipo=''

select * from ju_juicios where id_juicio=41327


Resultado           : Procedimiento de tipo nominas, devuelve selecciones ordenadas de acuerdo al criterio
*/


as

-----------------------------------------------------------------------------
-- Declaración de variables             
-----------------------------------------------------------------------------

Declare @Maximo				int
Declare @Minimo				int
Declare @paginas			int
Declare @totregs			int
Declare @ordenamiento1		varchar(4000)
Declare @ordenamiento2		varchar(4000)
Declare @ordenamiento3		varchar(4000)
declare @ordenascdesc		varchar(4)

Select @Maximo = (@pagina * @regpag)
Select @Minimo = @Maximo - (@regpag - 1)

set nocount on

set rowcount @Maximo
-----------------------------------------------------------------------------
-- Asignación  de variables              
-----------------------------------------------------------------------------
declare @sqlstr as varchar(8000)
declare @sqlstr1 as varchar(8000)
declare @sqlstr2 as varchar(8000)
declare @sqlfrom as varchar(8000)
declare @sqlfrom1 as varchar(8000)
declare @sqlfrom2 as varchar(8000)
declare @strfiltro as varchar(8000)
declare @strfiltro1 as varchar(8000)
declare @strfiltro2 as varchar(8000)

declare @tabletemp1 TABLE (variable varchar(30), prefijo varchar(100))
declare @tabletemp2 TABLE (variable varchar(30), prefijo varchar(100))
declare @tabletemp3 TABLE (variable varchar(30), prefijo varchar(100))

insert into @tabletemp1 (prefijo,variable) values ('id_juicio'		,'id_juicio')
insert into @tabletemp1 (prefijo,variable) values ('jg_fecha'		,'jg_fecha')
insert into @tabletemp1 (prefijo,variable) values ('id_gestion'		,'id_gestion')
insert into @tabletemp1 (prefijo,variable) values ('ge_gestion'		,'ge_gestion')
insert into @tabletemp1 (prefijo,variable) values ('jg_usuario'		,'usr')
insert into @tabletemp1 (prefijo,variable) values ('jg_fechaingreso','f_carga')
insert into @tabletemp1 (prefijo,variable) values ('jg_fecha'	,'tipo')
insert into @tabletemp1 (prefijo,variable) values ('ju_juiciosgestiones.id_juiciogestion'		,'id_bitacora')


insert into @tabletemp2 (prefijo,variable) values ('id_juicio'		,'id_juicio')
insert into @tabletemp2 (prefijo,variable) values ('bi_fecha'		,'jg_fecha')
insert into @tabletemp2 (prefijo,variable) values ('id_bitacora'	,'id_gestion')
insert into @tabletemp2 (prefijo,variable) values ('bi_bitacora'	,'ge_gestion')
insert into @tabletemp2 (prefijo,variable) values ('bi_usuario'		,'usr')
insert into @tabletemp2 (prefijo,variable) values ('bi_fecha'		,'f_carga')
insert into @tabletemp2 (prefijo,variable) values ('case isnull(id_tarea,0)  when 0 then ''Bitácora'' else ''Tarea'' end','tipo')
insert into @tabletemp2 (prefijo,variable) values ('bi_bitacoras.id_bitacora'	,'id_bitacora')


insert into @tabletemp3 (prefijo,variable) values ('tipo'		,'tipo')
insert into @tabletemp3 (prefijo,variable) values ('jg_fecha'	,'jg_fecha')
insert into @tabletemp3 (prefijo,variable) values ('ge_gestion'	,'ge_gestion')
insert into @tabletemp3 (prefijo,variable) values ('usr'		,'usr')
insert into @tabletemp3 (prefijo,variable) values ('f_carga'	,'f_carga')
insert into @tabletemp3 (prefijo,variable) values ('jg_fecha'	,'id_bitacora')


select @ordenascdesc = 'ASC'
if @criterioorden = 1 select @ordenascdesc = 'DESC'

select @ordenamiento1 = ' Row_Number() Over (Order by '+ prefijo + ' ' + @ordenascdesc +' ) as Norden '  
		from @tabletemp1 where variable = @criterioname 

select @ordenamiento2 = ' Row_Number() Over (Order by '+ prefijo + ' ' + @ordenascdesc +' ) as Norden '  
		from @tabletemp2 where variable = @criterioname 

select @ordenamiento3 = ' Order by '+ prefijo + ' ' + @ordenascdesc +' '  
		from @tabletemp3 where variable = @criterioname 


PRINT @ordenascdesc

if @pagina = 0 goto on_error

-----------------------------------------------------------------------------
-- Generación de select  
-----------------------------------------------------------------------------

select @sqlstr1 = ' 
		select top ' + cast(@Maximo as varchar) + ' 
		ju_juiciosgestiones.id_juiciogestion id_bitacora,
		dbo.ju_juicios.id_juicio, 
		''Gestión'' as tipo, 
		dbo.ju_juiciosgestiones.jg_fecha, 
		dbo.ju_gestiones.id_gestion, 
		dbo.ju_gestiones.ge_gestion, 
		ju_juiciosgestiones.jg_usuario as usr, 
		ju_juiciosgestiones.jg_fechaingreso as f_carga, ' +
		@ordenamiento1

		select @sqlstr2 = ' 
		select top ' + cast(@Maximo as varchar) + ' 
		bi_bitacoras.id_bitacora,
		dbo.ju_juicios.id_juicio,  
		case isnull(id_tarea,0)  when 0 then ''Bitácora'' else ''Tarea'' end tipo, 
		dbo.bi_bitacoras.bi_fecha,  
		dbo.bi_bitacoras.id_bitacora,  
		dbo.bi_bitacoras.bi_bitacora, 
		dbo.bi_bitacoras.bi_usuario as usr, 
		null as f_carga, ' +
		@ordenamiento2

--print @ordenamiento1
print @ordenamiento2

select @sqlfrom1 = ' 
		 From 
		 dbo.ju_juicios inner join 
		 dbo.ju_gestiones inner join 
		 dbo.ju_juiciosgestiones on dbo.ju_gestiones.id_gestion = dbo.ju_juiciosgestiones.id_gestion on 
		 dbo.ju_juicios.id_juicio = dbo.ju_juiciosgestiones.id_juicio 
		 
	'

select @sqlfrom2 = '
		 FROM         ju_juicios INNER JOIN  
		 bi_bitacoras ON ju_juicios.id_juicio = bi_bitacoras.id_origen INNER JOIN 
		 bi_bitacorasTipos ON bi_bitacoras.id_bitacoratipo = bi_bitacorasTipos.id_bitacoratipo LEFT OUTER JOIN 
		 bi_tareas ON bi_bitacoras.id_bitacora = bi_tareas.id_bitacora 
	' 

select @strfiltro1='WHERE 1=1 '
	select @strfiltro1 = dbo.fn_ut_sqldinamico_filtros(@strfiltro1, 'dbo.ju_juicios.id_juicio','int', @id_juicio,'')  
	select @strfiltro1 = dbo.fn_ut_sqldinamico_filtros(@strfiltro1, 'dbo.ju_juiciosgestiones.jg_fecha','date',@jg_fecha,@jg_fecha_hasta)
	select @strfiltro1 = dbo.fn_ut_sqldinamico_filtros(@strfiltro1, 'tipo','str', @tipo,'')  
	select @strfiltro1 = dbo.fn_ut_sqldinamico_filtros(@strfiltro1, 'dbo.ju_gestiones.id_gestion','int', @id_gestion,'')  
	select @strfiltro1 = dbo.fn_ut_sqldinamico_filtros(@strfiltro1, 'ju_gestiones.ge_gestion', 'str',@ge_gestion,'')

select @sqlfrom1 = @sqlfrom1 + isnull(@strfiltro1,'')

select @strfiltro2='WHERE 1=1  '
	select @strfiltro2 = dbo.fn_ut_sqldinamico_filtros(@strfiltro2, 'dbo.ju_juicios.id_juicio','int', @id_juicio,'')  
	select @strfiltro2 = dbo.fn_ut_sqldinamico_filtros(@strfiltro2, 'dbo.bi_bitacoras.bi_fecha','date',@jg_fecha,@jg_fecha_hasta)
	select @strfiltro2 = dbo.fn_ut_sqldinamico_filtros(@strfiltro2, 'tipo','str', @tipo,'')  
	select @strfiltro2 = dbo.fn_ut_sqldinamico_filtros(@strfiltro2, 'dbo.bi_bitacoras.id_bitacora','int', @id_gestion,'')  
	select @strfiltro2 = dbo.fn_ut_sqldinamico_filtros(@strfiltro2, 'bi_bitacoras.bi_bitacora', 'str',@ge_gestion,'')

--eroco se añade este if por que desde el juicio solo debe traer bitacoras que tengan como tabla el juicio
if @id_juicio <> 0 begin

	SET @strfiltro2 += ' AND ID_TABLA = 1'

end


select @sqlfrom2 = @sqlfrom2 + isnull(@strfiltro2,'')

--print @sqlfrom1
--print @sqlfrom2

-----------------------------------------------------------------------------
-- Devuelve la cantidad de registros totales
-----------------------------------------------------------------------------
declare @strtot as varchar(8000)
select @strtot = dbo.fn_ut_sqldinamico_totalregistros_union(@sqlfrom1,@sqlfrom2,@regpag)
--print @strtot
exec(@strtot)

--print @strtot
select @sqlstr = ' 
select  
id_bitacora,
id_juicio, 
tipo, 
jg_fecha, 
id_gestion, 
ge_gestion,
usr, 
f_carga, 
norden2,
Norden
from 
( 
	select  
	id_bitacora,
	id_juicio, 
	tipo, 
	jg_fecha, 
	id_gestion, 
	ge_gestion, 
	usr, 
	f_carga, 
	norden norden2,
	Row_Number() Over (' + @ordenamiento3 + ', norden asc  ) as Norden 
	From( ' +
	@sqlstr1 + isnull(@sqlfrom1,'')  + '
	UNION ' +
	@sqlstr2 + isnull(@sqlfrom2,'')  + '
) As  Paginacion ) as pag   
Where 
Norden >= ' + cast(@minimo as varchar(20)) + ' and Norden <= ' + cast(@maximo as varchar(20))


print @sqlstr
exec(@sqlstr)


if @@error <> 0 goto ON_ERROR

IF @@TRANCOUNT > 0
select 0,''
return 0

ON_ERROR:
IF @@TRANCOUNT > 0
select -1, 'Se Produjo un error en la transacción y se abortó'
return -1

Error_Params:
select -2, 'Problemas con los parámetros '
return -2






