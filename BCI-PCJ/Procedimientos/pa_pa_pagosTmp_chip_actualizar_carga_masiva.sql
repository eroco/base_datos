IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa_pa_pagosTmp_chip_actualizar_carga_masiva]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa_pa_pagosTmp_chip_actualizar_carga_masiva]
GO

CREATE procedure [dbo].[pa_pa_pagosTmp_chip_actualizar_carga_masiva]

/*
Creado por			: 
Fecha Creación		:  18-01-2007
Test				:

05/06/2019
EROCO se clona proceso de pa_pa_pagosTmp_chip_actualizar y se quita validacion de fecha de pago para el archivo cargado.

*/
as

declare @id_archivopago int
declare @id_archivopagotipo int
declare @apt_flgesvde bit
declare @flag bit
declare @registros int
declare @uf money
declare @us money
declare @moneda_uf int
declare @moneda_peso int
declare @moneda_us int
declare @Normaliza int
declare @Masivo int
declare @HONO_NORMALIZA decimal(10,2)
declare @PORC_PROV_HONO decimal(10,2)
declare @cant_fechas int
declare @cant_reg int
declare @pa_fecha smalldatetime

declare @myquery			varchar(8000)
declare @myheader			varchar(8000)
declare @archivo			varchar(100)
declare @fecha_desde		smalldatetime
declare @fecha_ayer			varchar(12)
declare @fecha_dia_desde	varchar(2)
declare @fecha_mes_desde	varchar(2)
declare @fecha_ano_desde	varchar(4)
declare @fecha				varchar(20)

declare @ju_juicios table
    (
	   norden int identity(1,1)
      ,id_juicio int
      ,ju_porchono decimal(10,2)
      ,id_cliente int
      ,cl_rut varchar(10)
      ,id_abogado int
    )

declare @ju_juiciosop table
    (
	   norden int identity(1,1)
      ,id_juicio int
      ,id_operacion int
      ,op_codoperacion    varchar(12) collate Modern_Spanish_CI_AS null
      ,cl_rut varchar(10)
      ,id_abogado int
      ,op_monto money
	  ,ju_porchono decimal(10,2)
	  ,operacion_comp bit
    )

declare @abogados_op table
    (
	   norden int identity(1,1)
      ,id_abogado int
      ,op_monto money
	  ,id_juiciomaxavance int
      ,ju_porchono decimal(10,2)
      ,id_operacion int
      ,pa_monto money
	  ,porcentaje_pago decimal(10,2)
    )

declare @abogados_op_nuevosregistros table
    (
	   norden int identity(1,1)
      ,id_abogado int
      ,op_monto money
	  ,id_juiciomaxavance int
      ,ju_porchono decimal(10,2)
      ,id_operacion int
      ,pa_monto money
	  ,porcentaje_pago decimal(10,2)
      ,id_pago int
      ,cl_rut varchar(10)
      ,op_codoperacion varchar(12)
    )

begin tran


select @moneda_uf = dbo.fn_ut_constantes('Uf'),
	   @moneda_peso = dbo.fn_ut_constantes('Pesos'),
	   @moneda_us = dbo.fn_ut_constantes('Dolar'),	
	   @Normaliza = dbo.fn_ut_constantes('cobranza_normaliza'),
	   @Masivo = dbo.fn_ut_constantes('juicio_tramo_masivo'),
	   @HONO_NORMALIZA = dbo.fn_ut_parametro_constante('HONO_NORMALIZA'),
       @PORC_PROV_HONO = dbo.fn_ut_parametro_constante('PORC_PROV_HONO'),
 	   @flag = 1

select @uf = tc_monto
from dbo.tg_tiposcambios
where tc_fecha = dbo.fn_ut_fecha_trunca_horas(getdate())
	  and id_moneda = @moneda_uf

select @us = tc_monto
from dbo.tg_tiposcambios
where tc_fecha = dbo.fn_ut_fecha_trunca_horas(getdate())
	  and id_moneda = @moneda_us

select top 1 @id_archivopago = id_archivopago, @id_archivopagotipo = dbo.pa_archivospagostipos.id_archivopagotipo, @apt_flgesvde = apt_flgesvde
from dbo.pa_archivospagos inner join
	 dbo.pa_archivospagostipos on dbo.pa_archivospagos.id_archivopagotipo = dbo.pa_archivospagostipos.id_archivopagotipo
order by id_archivopago desc

update pa_pagosTmp_chip
set id_moneda = case when mo_moneda = 'Pesos' then
                          @moneda_peso
					 else
                        case when mo_moneda = 'U.F.' then
						          @moneda_uf
							 else
								  @moneda_us
                             end
                     end 

--Actualización de campos para el archivo temp (Si es archivo es VDE)*************************
if @apt_flgesvde = 1
begin

	--Obtengo todos los juicios asociados al rut
	insert into @ju_juicios (id_juicio,ju_porchono,id_cliente,cl_rut,id_abogado)
	select id_juicio,ju_porchono,id_cliente,cl_rut,id_abogado--,id_juicioestado
	from pa_pagosTmp_chip p inner join
		 ju_juicios j on p.pa_rutcli = j.cl_rut	
    where j.id_organizacion = 2
		 
 
	--Obtengo todas las operaciones asociadas a los juicios 
    insert into @ju_juiciosop 
		   (id_juicio,
		    id_operacion,
		    op_codoperacion,
			cl_rut,
			id_abogado,
			op_monto,
			ju_porchono)
	select  o.id_juicio,
		    o.id_operacion,
		    op.op_codoperacion,
		    j.cl_rut,
		    o.id_persona,
		    case when op.id_moneda = @moneda_peso then
                      op.op_monto
				 else
					 case when op.id_moneda =@moneda_uf then 
					           op.op_monto * @uf
                          else
							   op.op_monto * @us
		        		  end
				 end op_monto,ju_porchono	
	from op_operacionesjuicios o inner join
		 op_operaciones op on o.id_operacion = op.id_operacion inner join
		 @ju_juicios j on o.id_juicio = j.id_juicio

	--Actualizo el campo para las operaciones que son compartidas por mas de 1 abogado
    update jp
		set jp.operacion_comp = @flag
    from @ju_juiciosop jp
	where jp.id_operacion in (select o.id_operacion
						      from @ju_juiciosop o
							  group by o.id_operacion 
							  having count(id_abogado)>1)

    --creo registro de pago de honorarios 
	declare @corr       int
	declare @NroInicial int , @NroFinal int
    declare @id_persona int , @abogados int, @id_operacion int, @id_pago int
    declare @monto_total money, @pa_monto money
	declare @cl_rut varchar(10), @ju_porchono decimal(9,2), @op_codoperacion varchar(12)
	select  @NroFinal   = max(id_pago), @NroInicial = min(id_pago) from pa_pagosTmp_chip
	set     @corr       = @NroInicial
	---------------------------------------------------------------
	while ( @corr <= (@nrofinal) )
	begin
	   if  exists ( select id_pago from  pa_pagosTmp_chip  where id_pago = @corr )
	   begin 
		    
			select @cl_rut = pa_rutcli, @pa_monto = pa_monto, @id_pago = id_pago
			from pa_pagosTmp_chip
			where id_pago = @corr
	
            select distinct @abogados = count(id_abogado)
            from @ju_juiciosop jo
			where cl_rut = @cl_rut

			--**********Solo 1 abogado para el cliente - CASO 1*********
			if @abogados = 1 
			  begin
				 select top 1 @id_operacion = id_operacion,
						@op_codoperacion = op_codoperacion,
                        @ju_porchono = ju_porchono
				 from @ju_juiciosop
				 where cl_rut = @cl_rut
				 order by ju_porchono desc,op_monto desc

                 update pa_pagosTmp_chip
					set cod_operacion = @op_codoperacion
				 where id_pago = @corr 		 

              end
            else
              begin
                    --***********Chequeo si el rut tiene una operacion compartida
                    if exists(select 1
							  from @ju_juiciosop
							  where cl_rut = @cl_rut and
								    operacion_comp = @flag)
                      begin --*******Si hay operaciones compartidas por varios abogados - CASO 2
						   select top 1 @id_operacion = id_operacion,
										@op_codoperacion = op_codoperacion,
										@ju_porchono = ju_porchono
						   from @ju_juiciosop
						   where cl_rut = @cl_rut and
								    operacion_comp = @flag

						   update pa_pagosTmp_chip
						 	  set cod_operacion = @op_codoperacion
						   where id_pago = @corr 		 
					  end
					else --*******Si no hay operaciones compartidas por varios abogados - CASO 3
					  begin
						   --Borro tabla con registros temporales e inserto abogados y cuantia de su cartera para ese cliente
						   delete from @abogados_op

						   insert into @abogados_op(id_abogado,op_monto)	
						   select id_abogado,sum(op_monto)
						   from @ju_juiciosop
						   where cl_rut = @cl_rut			
                           group by id_abogado

						   select @monto_total = sum(isnull(op_monto,0))
						   from @abogados_op
		
                           --Actualizo jucio con mayor avance y la operacion con mayor monto
						   update aop
							  set id_juiciomaxavance = (select top 1 id_juicio
												        from @ju_juiciosop jo
												        where jo.id_abogado = aop.id_abogado
											            order by ju_porchono desc, op_monto desc), 	
							      id_operacion = (select top 1 id_operacion
											      from @ju_juiciosop jo
											      where jo.id_abogado = aop.id_abogado
											      order by ju_porchono desc, op_monto desc) 
						   from	@abogados_op aop
							
						   --Actualizo porcentaje y monto que le corresponde a cada abogado	del pago realizado
						   update aop
							  set porcentaje_pago = (op_monto * 100) / @monto_total,
                                  pa_monto = @pa_monto * ((op_monto * 100) / @monto_total)
						   from	@abogados_op aop	

						   --Inserto registros nuevos que deben ser agregados a la tabla pa_pagosTmp_chip
						   insert into @abogados_op_nuevosregistros
							      (id_abogado,
								   op_monto,
								   id_juiciomaxavance,
								   ju_porchono,
								   id_operacion,
								   pa_monto,
								   porcentaje_pago,
								   id_pago,
								   cl_rut,
                                   op_codoperacion)	
						   select  aop.id_abogado,
								   aop.op_monto,
								   id_juiciomaxavance,
								   ju_porchono,
								   aop.id_operacion,
								   pa_monto,
								   porcentaje_pago,
								   @id_pago,
								   @cl_rut,
                                   op_codoperacion
						   from	@abogados_op aop inner join
                                op_operaciones o on  aop.id_operacion = o.id_operacion  
	
					  end
			  end

			if @@error <> 0 goto on_error
	   end
	---------------------------------------------------------------

	set @corr = @corr + 1
	---------------------------------------------------------------
	end

	select @registros = count(id_pago) from @abogados_op_nuevosregistros

	--Esto es valido solo para el CASO 3
	if @registros >0
	begin
		--Inserto los nuevos registros si es necesario
		insert into pa_pagosTmp_chip 
				(pa_monto,pa_variosdeudores,pa_rutcli,id_operacion,pa_fecha,id_moneda,
				 mo_moneda,pa_montopesos,pa_capital,pa_intereses,pa_saldo,pa_provisionhono,
				 pa_honocobranza,pa_flgpagado,pa_montouf,pa_rechazo,pa_flgrenegociacion,
				 pa_flgcuentapago,pa_flgconvenio,pa_montocaja,id_archivopago,pa_flgrecupcastigo,id_tiporeg,cod_operacion)
		select aop.pa_monto,aop.pa_monto,cl_rut,aop.id_operacion,pa_fecha,id_moneda,mo_moneda,pa_montopesos,
			   pa_capital,pa_intereses,pa_saldo,pa_provisionhono ,pa_honocobranza ,pa_flgpagado,
			   pa_montouf,pa_rechazo,pa_flgrenegociacion,pa_flgcuentapago,pa_flgconvenio,pa_montocaja,
			   id_archivopago,pa_flgrecupcastigo,id_tiporeg,op_codoperacion 
		from @abogados_op_nuevosregistros aop inner join
			 dbo.pa_pagosTmp_chip on aop.id_pago = dbo.pa_pagosTmp_chip.id_pago
		if @@error <> 0 goto on_error
	
		--Borro los registros originales
		delete from pa_pagosTmp_chip where id_pago in 
			                         (select id_pago
								      from @abogados_op_nuevosregistros)
		if @@error <> 0 goto on_error
	end

    update p
	set pa_saldo = op_saldoactual 
    from  pa_pagosTmp_chip p inner join
		  op_operaciones o on p.id_operacion = p.id_operacion
    if @@error <> 0 goto on_error

    update pa_pagosTmp_chip
	set pa_capital = case when pa_monto>pa_saldo then
						       isnull(pa_monto,0)-isnull(pa_saldo,0)
                          else
                               pa_monto
                          end 	
    if @@error <> 0 goto on_error

    update pa_pagosTmp_chip
	set pa_intereses = case when pa_monto>pa_saldo then
						         isnull(pa_monto,0)-isnull(pa_capital,0)
                            else
                                 0
                            end 		 
    if @@error <> 0 goto on_error
end 
--********************************************************************************************


    --*****************************************************************************************************
    --13/11/2008
    --Se actualiza con la operacion que no tenga fecha de salida o que tenga la fecha de salida mas reciente
    --Se modifico tambien la carga del archivo fi para dejar pasar todas las operaciones que existan en la BD

--		update pa_pagosTmp_chip
--		set id_operacion = p.id_operacion
--		from(
--			select     
--				op_operaciones.id_operacion, op_operaciones.op_codoperacion
--			from         
--				pa_pagosTmp_chip inner join
--				op_operaciones on pa_pagosTmp_chip.cod_operacion = op_operaciones.op_codoperacion
--	
--		) p inner join pa_pagosTmp_chip pa on pa.cod_operacion = p.op_codoperacion 


	update pa_pagosTmp_chip
	set id_operacion = dbo.fn_co_id_operacion_pago(cod_operacion)
	--*******************************************************************************************************



	update pa_pagosTmp_chip
	set id_persona = p.id_persona
	from(
		select     
			tg_personas.id_persona, tg_personas.pe_rut
		from         
			pa_pagosTmp_chip inner join
			tg_personas on pa_pagosTmp_chip.pa_rutcli = tg_personas.pe_rut

	) p inner join pa_pagosTmp_chip pa on pa.pa_rutcli = p.pe_rut 

    --Actualizo monto caja si es 0
	update t
	set pa_montocaja = (isnull(pa_capital,0) + isnull(pa_intereses,0)) +
					   (isnull(pa_capital,0) + isnull(pa_intereses,0)) * @PORC_PROV_HONO/100 +
                       case when c.id_cobranza = @Normaliza and c.id_clientetipo = @Masivo then
								(isnull(pa_capital,0) + isnull(pa_intereses,0)) * @HONO_NORMALIZA/100 
						    else
                                0
							end
	from pa_pagosTmp_chip t inner join
		 tg_clientes c on t.id_persona = c.id_persona
	where isnull(pa_montocaja,0) = 0 --and
--		  c.id_cobranza = @Normaliza and 
--		  c.id_clientetipo = @Masivo
	      



--***************** Ahora esto se hace por la carga del archivo fi de VDE
--	update pa_pagosTmp_chip
--	set pa_variosdeudores =
--				case
--					when isnull(dbo.fn_ut_pa_pagosTmp_chip_suma_con_op(pa_rutcli),0) = 0 then 0
--					else pa_monto / dbo.fn_ut_pa_pagosTmp_chip_suma_con_op(pa_rutcli) * isnull(dbo.fn_ut_pa_pagosTmp_chip_suma_sin_op(pa_rutcli),0)
--				end
--	where (cod_operacion is not null)


	--Actualizo el monto en pesos 
	update pa_pagosTmp_chip 
		set pa_montouf = round(pa_monto/tc_monto,4),
			pa_montopesos = pa_monto 
	from pa_pagosTmp_chip inner join 
		 op_operaciones on pa_pagosTmp_chip.id_operacion = op_operaciones.id_operacion inner join
		 tg_tiposcambios on dbo.fn_ut_fecha_trunca_horas(op_operaciones.op_fechaasignacion) = dbo.fn_ut_fecha_trunca_horas(tg_tiposcambios.tc_fecha) and op_operaciones.id_moneda = tg_tiposcambios.id_moneda

	update pa_pagosTmp_chip 
		set pa_montouf = round(pa_monto/tc_monto,4),
			pa_montopesos = pa_monto 
	from pa_pagosTmp_chip inner join 
		 op_operaciones on pa_pagosTmp_chip.id_operacion = op_operaciones.id_operacion inner join
		 tg_tiposcambios on dbo.fn_ut_fecha_trunca_horas(op_operaciones.op_fechaasignacion) = dbo.fn_ut_fecha_trunca_horas(tg_tiposcambios.tc_fecha) 
    where op_operaciones.id_moneda = 1 and tg_tiposcambios.id_moneda = 2



	update pa_pagosTmp_chip
	set pa_rechazo = isnull(pa_rechazo,'')+'No existe el id_operacion asociado a la Operacion. '
	where cod_operacion in (
	select     
		cod_operacion
	from         
		pa_pagosTmp_chip
	where		
		(id_operacion is null and isnull(cod_operacion,'') != '')
	)

	update pa_pagosTmp_chip
	set pa_rechazo = isnull(pa_rechazo,'')+'No existe el id_moneda correspondiente a la moneda. '
	where mo_moneda in (
	select 
		mo_moneda
	from 
		pa_pagosTmp_chip
	where		
		(id_moneda is null)
	)

	update pa_pagosTmp_chip
	set pa_rechazo = isnull(pa_rechazo,'')+'No existe el Cliente asociado al RUT. '
	where pa_rutcli in (
	select     
		pa_rutcli
	from         
		pa_pagosTmp_chip
	where		
		(id_persona is null)
	)


	--update pa_pagosTmp_chip
	--set pa_rechazo = 'Ya existe un pago con el mismo monto para esta fecha y esta operación. '
	--from(
	--	select     
	--		distinct pa_pagos.id_operacion
	--	from         
	--		pa_pagos inner join
	--		pa_pagosTmp_chip on pa_pagosTmp_chip.pa_monto = pa_pagos.pa_monto and 
	--		pa_pagosTmp_chip.id_operacion = pa_pagos.id_operacion and
	--		pa_pagosTmp_chip.pa_fecha = pa_pagos.pa_fecha 

	--) p inner join pa_pagosTmp_chip pa on pa.id_operacion = p.id_operacion

	update pa_pagosTmp_chip
	set pa_rechazo = 'Ya existe un pago con el mismo monto para esta fecha y esta operación. '
	from(
		select     
			distinct pa_pagos.id_operacion,pa_pagos.pa_fecha 
		from         
			pa_pagos inner join
			pa_pagosTmp_chip on pa_pagosTmp_chip.pa_monto = pa_pagos.pa_monto and 
			pa_pagosTmp_chip.id_operacion = pa_pagos.id_operacion and
			pa_pagosTmp_chip.pa_fecha = pa_pagos.pa_fecha 

	) p 
	inner join pa_pagosTmp_chip pa on pa.id_operacion = p.id_operacion and p.pa_fecha = pa.pa_fecha 

	/*EROCO 06/06/2019 AHORA SE TOMAN TODAS LAS FECHAS CARGADAS Y SE DEJA AL ARCHIVO LA FECHA DEL ARCHIVO QUE SERIA EL DÍA DE EJECUCION MENOS 1 DÍA.*/
	--Chequeo unicidad de fechas
    --select top 1 @pa_fecha = MAX(pa_fecha)
	--from pa_pagosTmp_chip
     
	/*
	if @pa_fecha is null
        select top 1 @pa_fecha = CAST(GETDATE()-1 AS DATE)
	    from pa_pagos_noencontrados
		where id_archivopago = @id_archivopago 
	*/

	update pa_archivospagos
	set ap_fecha = CAST(GETDATE()-1 AS DATE)
	where id_archivopago = @id_archivopago 
	if @@error <> 0 goto on_error
	 

	--if exists(select 1
	--		  from pa_pagosTmp_chip
	--		  where isnull(pa_rechazo,'') <> '')
	--begin
	--	 update pa_pagosTmp_chip
	--	 set id_archivopago = null
	--	 if @@error <> 0 goto on_error

	--	 print 'delete 2'
	--	 delete from dbo.pa_pagos_noencontrados
	--	 where id_archivopago = @id_archivopago 
	--	 if @@error <> 0 goto on_error

	--	 delete from pa_archivospagos
	--	 where id_archivopago = @id_archivopago 
	--	 if @@error <> 0 goto on_error

	--end

	PRINT ' LLENO TABLA HISTORICA DE PAGOS CMC 10/10/2017 '
	-- select * from pa_pagoshistoricos where id_archivopago=11875
	-- select * from pa_pagosTmp_chip
	-- select 150/2
	INSERT INTO [dbo].[pa_pagoshistoricos]
	   ([id_pago]
	   ,[pa_fecha]
	   ,[id_operacion]
	   ,[cod_operacion]
	   ,[id_moneda]
	   ,[mo_moneda]
	   ,[pa_monto]
	   ,[pa_montopesos]
	   ,[pa_variosdeudores]
	   ,[pa_capital]
	   ,[pa_intereses]
	   ,[pa_saldo]
	   ,[pa_provisionhono]
	   ,[pa_honocobranza]
	   ,[pa_flgpagado]
	   ,[pa_montouf]
	   ,[pa_rechazo]
	   ,[pa_rutcli]
	   ,[id_persona]
	   ,[pa_flgrenegociacion]
	   ,[pa_flgCuentaPago]
	   ,[pa_flgconvenio]
	   ,[pa_montocaja]
	   ,[id_archivopago]
	   ,[pa_flgRecupCastigo]
	   ,[id_tiporeg]
	   ,[pa_interesmora]
	   ,[pa_gastoscobranza]
	   ,[pa_costasjudiciales]
	   ,[pa_condonacion]
	   ,[pa_diasmora]
	   ,[pa_finproceso])
	SELECT [id_pago]
	  ,[pa_fecha]
	  ,[id_operacion]
	  ,[cod_operacion]
	  ,[id_moneda]
	  ,[mo_moneda]
	  ,[pa_monto]
	  ,[pa_montopesos]
	  ,[pa_variosdeudores]
	  ,[pa_capital]
	  ,[pa_intereses]
	  ,[pa_saldo]
	  ,[pa_provisionhono]
	  ,[pa_honocobranza]
	  ,[pa_flgpagado]
	  ,[pa_montouf]
	  ,[pa_rechazo]
	  ,[pa_rutcli]
	  ,[id_persona]
	  ,[pa_flgrenegociacion]
	  ,[pa_flgCuentaPago]
	  ,[pa_flgconvenio]
	  ,[pa_montocaja]
	  ,[id_archivopago]
	  ,[pa_flgRecupCastigo]
	  ,[id_tiporeg]
	  ,[pa_interesmora]
	  ,[pa_gastoscobranza]
	  ,[pa_costasjudiciales]
	  ,[pa_condonacion]
	  ,[pa_diasmora]
	  ,[pa_finproceso]
	FROM [dbo].[pa_pagosTmp_chip]


	--EROCO 20-11-2017 LOG PARA LA CARGA DE FI DE PAGOS CHIP
	--set		@fecha_desde		= dateadd(dd,-1,dbo.fn_ut_fecha_trunca_horas(getdate()))
	set		@fecha_desde		= getdate()
	set		@fecha_dia_desde	= day(dbo.fn_ut_fecha_trunca_horas(@fecha_desde))
	set		@fecha_mes_desde	= month(dbo.fn_ut_fecha_trunca_horas(@fecha_desde))
	set		@fecha_ano_desde	= year(dbo.fn_ut_fecha_trunca_horas(@fecha_desde))
	set		@fecha_ayer			= @fecha_dia_desde + @fecha_mes_desde + @fecha_ano_desde
	set		@fecha				= convert(varchar,getdate(),103) + ' ' + convert(varchar,getdate(),108)


	set		@archivo		= 'log_fi_chip_' + @fecha_ayer + '.xls'--log_fi_chip_20112017.xls
	set		@myheader		= ' Estimado(s):<br><br>Envío Log de Carga FI CHIP del día <b>' + @fecha + ' </b><br><br>Saludos,<br><br>Mesa de Ayuda<br>PlataformaGroup<br>Tel. 2 2963 8040 | 2 2963 8030<br>www.plataformagroup.cl<br>' 

	set		@myquery = '
	set nocount on SELECT 
	''<table border=1 width="100%">
			<tr style="font-weight: bold;">
				<td>Id Pago</td>'',''
				<td>Fecha de Pago</td>'',''
				<td>Id Operación</td>'',''
				<td>Código Operacion</td>'',''
				<td>Id Moneda</td>'',''
				<td>Moneda</td>'',''
				<td>Monto Pago</td>'',''
				<td>Monto en Pesos</td>'',''
				<td>Varios Deudores</td>'',''
				<td>Capital</td>'',''
				<td>Intereses</td>'',''
				<td>Saldo</td>'',''
				<td>Provisión Honorarios</td>'',''
				<td>Honorarios Cobranza</td>'',''
				<td>Marca de Pago</td>'',''
				<td>Monto UF</td>'',''
				<td>Rechazo</td>'',''
				<td>Rut Cliente</td>'',''
				<td>Id Persona</td>'',''
				<td>Marca Renegociacion</td>'',''
				<td>Marca Cuenta Pago</td>'',''
				<td>Marca Convenio</td>'',''
				<td>Mont Caja</td>'',''
				<td>Id Archivo Pago</td>'',''
				<td>Marca Recuperación Castigo</td>'',''
				<td>Id Tipo Registro</td>'',''
				<td>Interés Mora</td>'',''
				<td>Gastos Cobranza</td>'',''
				<td>Costas Judiciales</td>'',''
				<td>Condonacion</td>'',''
				<td>Dias Mora</td>'',''
				<td>Marca Fin Proceso</td>
			</tr>''
	UNION
	SELECT
	''<tr>
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.id_pago as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + convert(varchar,isnull( [PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_fecha,''''),103)  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.id_operacion as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.cod_operacion as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.id_moneda as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.mo_moneda as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_monto as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_montopesos as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_variosdeudores as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_capital as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_intereses as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_saldo as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_provisionhono as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_honocobranza as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_flgpagado as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_montouf as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_rechazo as varchar(500)),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_rutcli as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.id_persona as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_flgrenegociacion as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_flgCuentaPago as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_flgconvenio as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_montocaja as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.id_archivopago as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_flgRecupCastigo as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.id_tiporeg as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_interesmora as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_gastoscobranza as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_costasjudiciales as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_condonacion as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_diasmora as varchar),'''')  + ''</td>'',''
		<td>'' + space(1) + isnull( cast([PJBCI_INT].[dbo].pa_pagosTmp_chip.pa_finproceso as varchar),'''')  + ''</td>
	</tr>''
	FROM  [PJBCI_INT].[dbo].pa_pagosTmp_chip with (nolock)
	WHERE 1=1 
	'

	Exec msdb.dbo.sp_send_dbmail 
	@profile_name					= 'sql2005',
	@query							= @myquery,

	@body							= @myheader,
	@body_format					= 'HTML',

	--PRODUCCION
	--@recipients						= 'felipe.silva@bci.cl; esteban.espinoza@bci.cl; cristian.farias@bci.cl; connie.donoso@bci.cl',
	--@copy_recipients				= 'servicioalcliente@plataformagroup.cl; patricia.salas@bci.cl; manuel.diazp@bci.cl',
	--@subject						= 'Log de Carga FI CHIP',

	--INTEGRACION
	@recipients						= 'eroco@plataformagroup.cl',
	@copy_recipients				= 'cmunoz@plataformagroup.cl,ygonzalez@plataformagroup.cl',
	@subject						= 'INT Log de Carga FI CHIP',

	--CLON
	--@recipients						= 'exltorv@bci.cl,felipe.silva@bci.cl',
	--@copy_recipients				= 'cmunoz@plataformagroup.cl,eroco@plataformagroup.cl',
	--@subject						= 'CLON Log de Carga FI CHIP',

	@attach_query_result_as_file	= 1,
	@query_result_width				= 10000,
	@query_attachment_filename		= @archivo,
	@query_result_header			= 0,
	@exclude_query_output			= 1,
	@query_result_separator			= ' '

	if @@error <> 0 goto ON_ERROR



-- control de errores
if @@error <> 0 goto on_error 

if @@trancount > 0
commit tran
return 0

ON_ERROR:
If @@TRANCOUNT > 0
ROLLBACK TRAN
select -1, 'Se Produjo un error en la transacción y se abortó'
return -1

Error_Params:
select -2, 'Problemas con los parámetros '
return -2















