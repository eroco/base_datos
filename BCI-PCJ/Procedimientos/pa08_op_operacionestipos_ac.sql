IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa08_op_operacionestipos_ac]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa08_op_operacionestipos_ac]
GO

CREATE PROCEDURE [dbo].[pa08_op_operacionestipos_ac]
@id_operaciontipo as int = null,
@op_operaciontipo as varchar(30) = '',
@op_esaunvencimiento as bit = 0,
@op_flgEsCHIP as bit = 0,
@op_codigonova as int = null,
@op_flgEsCONSUMO as bit = 0,
@op_esggee as bit = 0,
@op_escae as bit = 0,
@ot_plazosasignacionSpeed as int = null,
@BinaryChecksum nvarchar(4000) = '',
@ForceUpdate  char(1) = 'Y'

/*
Creador por : DS-GFONTECILLA
Fecha Creación: 22-03-2013 12:27:28
Procedimiento:pa08_op_operacionestipos_ac

Modificado por: EROCO
Fecha Modificación : 05-09-2019 proyecto speed marcas

Modificado por:
Fecha Modificación :

llamada de test     :  
begin tran
    pa08_op_operacionestipos_ac 0,'',0,0,0,0
rollback
anulartest : ROLLBACK(TRAN)
Resultado: Procedimiento de inserción de un nuevo registro en la tabla op_operacionestipos
*/

as

set nocount on
DECLARE    @l_newValue nvarchar(4000),
    @return_status int,
    @l_rowcount int

-- Check whether the record still exists before doing update
   IF NOT EXISTS (SELECT * FROM [dbo].[op_operacionestipos] WHERE [id_operaciontipo] = @id_operaciontipo)
       RAISERROR ('Concurrency Error: The record has been deleted by another user. Table [dbo].[op_operacionestipos]', 16, 1)

   -- If user wants to force update to happen even if 
   -- the record has been modified by a concurrent user,
   -- then we do this.
   IF (@ForceUpdate = 'Y')
        BEGIN
            -- Update the record with the passed parameters
			Update [dbo].[op_operacionestipos] set
						[op_operaciontipo] = @op_operaciontipo,
						[op_esaunvencimiento] = @op_esaunvencimiento,
						[op_flgEsCHIP] = @op_flgEsCHIP,
						[op_codigonova] = @op_codigonova,
						[op_flgEsCONSUMO] = @op_flgEsCONSUMO,
						op_esggee = @op_esggee ,
						op_escae = @op_escae,
						ot_plazosasignacionSpeed = @ot_plazosasignacionSpeed
			WHERE [id_operaciontipo] = @id_operaciontipo

            -- Make sure only one record is affected
            SET @l_rowcount = @@ROWCOUNT
            IF @l_rowcount = 0
                RAISERROR ('The record cannot be updated.', 16, 1)
            IF @l_rowcount > 1
                RAISERROR ('duplicate object instances.', 16, 1)
        END
    ELSE
        BEGIN
            -- Get the checksum value for the record 
            -- and put an update lock on the record to 
            -- ensure transactional integrity.  The lock
            -- will be release when the transaction is 
            -- later committed or rolled back.
            Select @l_newValue = CAST(BINARY_CHECKSUM([dbo].[op_operacionestipos].[id_operaciontipo],[dbo].[op_operacionestipos].[op_operaciontipo],[dbo].[op_operacionestipos].[op_esaunvencimiento],[dbo].[op_operacionestipos].[op_flgEsCHIP],[dbo].[op_operacionestipos].[op_codigonova],[dbo].[op_operacionestipos].[op_flgEsCONSUMO]) AS nvarchar(4000))
            FROM [dbo].[op_operacionestipos] with (rowlock, holdlock)
            WHERE [id_operaciontipo] = @id_operaciontipo
	    -- Check concurrency by comparing the checksum values
            IF (@BinaryChecksum = @l_newValue)
                SET @return_status = 0     -- pass
            ElSE
                SET @return_status = 1     -- fail

            -- Concurrency check passed.  Go ahead and 
            -- update the record
            IF (@return_status = 0)
                BEGIN
					-- Update the record with the passed parameters
					Update [dbo].[op_operacionestipos] set
								[op_operaciontipo] = @op_operaciontipo,
								[op_esaunvencimiento] = @op_esaunvencimiento,
								[op_flgEsCHIP] = @op_flgEsCHIP,
								[op_codigonova] = @op_codigonova,
								[op_flgEsCONSUMO] = @op_flgEsCONSUMO,
								op_esggee = @op_esggee ,
								op_escae = @op_escae,
								ot_plazosasignacionSpeed = @ot_plazosasignacionSpeed
					WHERE [id_operaciontipo] = @id_operaciontipo

					-- Make sure only one record is affected
					SET @l_rowcount = @@ROWCOUNT
					IF @l_rowcount = 0
					    RAISERROR ('The record cannot be updated.', 16, 1)
					IF @l_rowcount > 1
					    RAISERROR ('duplicate object instances.', 16, 1)
                END
            ELSE
                -- Concurrency check failed.  Inform the user by raising the error
                RAISERROR ('Concurrency Error: The record has been updated by another user. Table [dbo].[op_operacionestipos]', 16, 1)
        END
