alter PROCEDURE [dbo].[pa08_tg_documentosbitacoras_in]
@id_tabla			as int = null,
@do_documento		as varchar(8000) = '',
@id_documentotipo	as int = null,
@do_fecha			as smalldatetime = null,
@id_origen			as int = null,
@do_archname		as varchar(100) = '',
@id_tipoarchivo		as int = null,
@do_texto			as varchar(50) = '',
@do_esprivado		as bit = 0,
@id_bitacora		as int = 0,
@bi_bitacora		as varchar(4000) = '',
@bi_usuario			as varchar(100) = '',
@id_persona			as int = 0,
@msg_cargamasiva	as varchar(200) = '', --EROCO 30-11-2017 Se agrega nuevo parametro para carga masiva de documentos asi queda constancia de que la carga se genero pro carga masiva
@bi_esautomatico	as int = 0,
@es_cargamasiva		as int = 0, --EROCO 01-12-2017 Se agrega nuevo parametro para carga masiva de documentos
@comentario			as varchar(200) = '',
@informacion		as varchar(max) = '',
@id_rematedetalle	as int = 0

/*
Creador por : NTBK-EROCO
Fecha Creación: 09-03-2017 12:38:08
Procedimiento:pa08_tg_documentos_in
Modificado por:
Fecha Modificación
EROCO 10/04/2017 se modifica mensaje en la bitacora
llamada de test     :  
begin tran
    pa08_tg_documentos_in 0,'',0,null,0,'',0,'',0
rollback
anulartest : ROLLBACK(TRAN)
Resultado: Procedimiento de inserción de un nuevo registro en la tabla tg_documentos
*/

as
set nocount on

DECLARE @MENSAJEENBITACORA AS VARCHAR(1000)
DECLARE @TipoDocumento as Varchar(100)
DECLARE @id_bitacoratipo as int 

select @id_tabla = id_tabla from tg_tablas where ta_tabla = 'tg_clientes'
select @id_bitacoratipo = id_bitacoratipo from bi_bitacorastipos where bt_bitacoratipo = 'Clientes'
SET @bi_bitacora = @bi_bitacora

INSERT INTO [dbo].[bi_bitacoras]
           (
			   [bi_bitacora],
			   [bi_fecha],
			   [id_persona],
			   [bi_esautomatico],
			   [bi_msg_user],
			   [bi_msg_fecha],
			   [id_origen],
			   [bi_usuario],
			   [bi_confidencial],
			   [id_bitacorapadre],
			   [id_bitacoratipo],
			   [bi_flgescomentario],
			   [id_juicio]
			)
			Values
			(
				@bi_bitacora,
				@do_fecha,
				@id_persona,
				@bi_esautomatico,
				@bi_usuario,
				@do_fecha,
				@id_persona,
				@bi_usuario,
				0,
				null,
				@id_bitacoratipo,
				0,
				null				
			)

select @id_bitacora = SCOPE_IDENTITY()

/**CAMBIO SOLICITADO POR BCI 10-04-2017 AGREGAR DOCUMENTO INGRESADO A LA BITACORA**/
select @TipoDocumento = dt_documentotipo from tg_documentostipos where id_documentotipo = @id_documentotipo

if @es_cargamasiva = 0
begin
	SET @MENSAJEENBITACORA = '<br>Documento : ' + @do_archname + ' , Tipo : ' + @TipoDocumento + '.'
END
ELSE
BEGIN
	SET @MENSAJEENBITACORA = ''
end

update [bi_bitacoras] set [bi_bitacora] = [bi_bitacora] + @MENSAJEENBITACORA where id_bitacora = @id_bitacora
/**FIN CAMBIO SOLICITADO POR BCI 10-04-2017 AGREGAR DOCUMENTO INGRESADO A LA BITACORA**/


INSERT INTO dbo.[tg_documentos]
            (
				[id_tabla],
				[do_documento],
				[id_documentotipo],
				[do_fecha],
				[id_origen],
				[do_archname],
				[id_tipoarchivo],
				[do_texto],
				[do_esprivado],
				[es_cargamasiva],
				[comentario],
				[informacion],
				[id_rematedetalle],
				es_workflow
            )
            Values
            (
				@id_tabla,
				@do_documento,
				@id_documentotipo,
				@do_fecha,
				@id_bitacora,
				@do_archname,
				@id_tipoarchivo,
				@do_texto,
				@do_esprivado,
				@es_cargamasiva,
				@comentario,
				@informacion,
				@id_rematedetalle,
				case @id_rematedetalle when 0 then 0 else 1 end
            )

RETURN @id_bitacora




GO


