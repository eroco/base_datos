ALTER PROCEDURE [dbo].[pa_ju_fichajudicial_Datosjuicio]
@id_juicio as int = 0,
@us_consuser	as varchar(50) = 'sistema'


/*
Creador por : ds-program8
Fecha Creación: 19/10/2007 9:31:27
Procedimiento:[pa_ju_fichajudicial_sel]
Modificado por:
Fecha Modificación
llamada de test     :  
    pa_ju_fichajudicial_Datosjuicio 
    pa_ju_fichajudicial_Datosjuicio  172310
select * from tg_personas where id_persona in (286718,66)
select * from ju_reasignaciones where id_juicio= 368308 order by 6

pa_ju_fichajudicial_Datosjuicio @id_juicio=7384
select * from ju_juicios where id_juicio = 7384

select * from ju_reasignaciones order by id_juicio --368308

7383
7384
7385
7386
7387
7388
Resultado: Procedimiento de tipo nominas, devuelve selecciones ordenadas de acuerdo al criterio
*/

as



--------------------------------------------------------------------------------------------------------
-- Datos del juicio
--------------------------------------------------------------------------------------------------------

declare @datosjuicios as table(
    orden	int identity(1,1),
    id_juicio			int, 
    id_controlador	    int, 
    Controlador         varchar(500) collate Modern_Spanish_CI_AS null,	
    id_ejecutivo        int, 
    ej_ejecutivo        varchar(500) collate Modern_Spanish_CI_AS null,
    Ejecutivo           varchar(500) collate Modern_Spanish_CI_AS null,
    id_juiciotipo       int, 
    jt_juiciotipo       varchar(50) collate Modern_Spanish_CI_AS null,
    ju_rol              varchar(20) collate Modern_Spanish_CI_AS null, 
    id_tribunal         int, 
    tr_tribunal         varchar(50) collate Modern_Spanish_CI_AS null,
    id_ciudad           int, 
    ci_ciudad           varchar(50) collate Modern_Spanish_CI_AS null, 
    ju_fechaini         smalldatetime, 
    id_juicioestado     int, 
    je_juicioestado     varchar(50) collate Modern_Spanish_CI_AS null,
    id_juicioetapa      int, 
    je_juicioetapa      varchar(50) collate Modern_Spanish_CI_AS null,
    id_moneda           int, 
    ju_montodemanda     money, 
    id_gestion          int default 0, 
    ge_gestion          varchar(100) collate Modern_Spanish_CI_AS default '',
    id_abogadooriginal  int default 0,
    id_abogadonuevo     int default 0,
    re_fecha            smalldatetime,
    aboAnterior         varchar(500) collate Modern_Spanish_CI_AS default '',
    aboActual           varchar(500) collate Modern_Spanish_CI_AS default '',
    id_Abogado          int default 0,
    abogado             varchar(500) collate Modern_Spanish_CI_AS null,
    id_cliente          int default 0,
    mo_moneda           varchar(50) collate Modern_Spanish_CI_AS null,
    mo_sigla            varchar(5) collate Modern_Spanish_CI_AS null,
    ju_fecreactivacion  smalldatetime,
    tiene_ddaa			int
)

--------------------------------------------------------------------------------------------------------
-- Datos clientes
--------------------------------------------------------------------------------------------------------
declare @datosclientes  as table(
    orden                   int identity(1,1),
    id_juicio               int, 
    ju_rol                  varchar(20) collate Modern_Spanish_CI_AS null,
    id_organizacion         int, 
    or_organizacion         varchar(50) collate Modern_Spanish_CI_AS null,
    id_subbanca             int, 
    sb_subbanca             varchar(50) collate Modern_Spanish_CI_AS null,
    pe_rut                  varchar(10) collate Modern_Spanish_CI_AS null,
    pe_nombrecompleto       varchar(500) collate Modern_Spanish_CI_AS null,
    pu_direccion            varchar(100) collate Modern_Spanish_CI_AS null,
    pu_fonos                varchar(50) collate Modern_Spanish_CI_AS null,
    pu_email                varchar(150) collate Modern_Spanish_CI_AS null,
    n_juicios               int default 0,
    n_garantiasgenerales    int default 0,
    n_bienes                int default 0,
    id_cliente              int,
    of_oficina              varchar(50) collate Modern_Spanish_CI_AS null,
    id_operacionaval        int default 0,
    n_notificaciones_deudor int default 0,
    n_notificaciones_otros  int default 0,
    pe_flgenquiebra			int default 0
)


declare @n_notificaciones_otros  int
declare @n_notificaciones_deudor int
declare @id_cliente              int
declare @MARCA_NOTIFICAR        int
declare @id_avaltipo            int


declare @CONST_GASTOS_DOC_CEN_PAGO  as int
declare @CONST_Gasto_Rechazado      as int
declare @CONST_GASTOS_DOC_A_PAGO      as int
declare @pagado                     as money
declare @porpagar                   as money
declare @capital                    as money
declare @total                    as money
declare @intereses                  as money

declare @id_personausuario	int
select @id_personausuario = id_persona from ca_usuarios where us_consuser= @us_consuser


set nocount on


-------------------------------------------------------------------------------------------
-- Datos del juicio
-------------------------------------------------------------------------------------------

insert into @datosjuicios(
    id_juicio,			
    id_controlador,
    Controlador,      
    id_ejecutivo,      
    ej_ejecutivo ,     
    Ejecutivo,         
    id_juiciotipo,     
    jt_juiciotipo,     
    ju_rol,            
    id_tribunal , 
    tr_tribunal,       
    id_ciudad ,        
    ci_ciudad,         
    ju_fechaini,       
    id_juicioestado,   
    je_juicioestado,   
    id_juicioetapa ,   
    je_juicioetapa,    
    id_moneda,         
    ju_montodemanda,   
    id_gestion ,         
    ge_gestion,
    id_Abogado,
    abogado ,
    id_cliente,
    mo_moneda,
    mo_sigla,ju_fecreactivacion         
)

SELECT     
	jui.id_juicio, 
	jui.id_controlador, 
	controlador.pe_nombrecompleto, 
	jui.id_ejecutivo, 
	jui.ej_ejecutivo, 
	eje.pe_nombrecompleto AS Ejecutivo, 
	jui.id_juiciotipo, 
	juitip.jt_juiciotipo, 
	jui.ju_rol, jui.id_tribunal , 
	tri.tr_tribunal, 
	tri.id_ciudad, 
	ciu.ci_ciudad, 
	jui.ju_fechaini, 
	jui.id_juicioestado, 
	juiest.je_juicioestado, 
	jui.id_juicioetapa, 
	juieta.je_juicioetapa, 
	jui.id_moneda, 
	jui.ju_montodemanda, 
	isnull(jui.id_gestion,0) as id_gestion, 
	isnull(gest.ge_gestion,'') as ge_gestion,
    jui.id_abogado,
    aboga.pe_nombrecompleto,
     jui.id_cliente,
    mon.mo_moneda,
    mon.mo_sigla,dbo.fn_ut_fecha_trunca_horas(ju_fecreactivacion)
--FROM         
--	dbo.ju_juiciosetapas as juieta (nolock) inner join
--	dbo.ju_juicios as jui  (nolock)  inner join
--	dbo.tg_personas as controlador   (nolock) on jui.id_controlador = controlador.id_persona inner join
--	dbo.ju_juiciostipos as juitip  (nolock)  on jui.id_juiciotipo = juitip.id_juiciotipo inner join
--	dbo.ju_tribunales as tri  (nolock)  on jui.id_tribunal = tri.id_tribunal inner join
--	dbo.ub_ciudades as ciu  (nolock) on tri.id_ciudad = ciu.id_ciudad inner join
--	dbo.ju_juiciosestados as juiest (nolock)  on jui.id_juicioestado = juiest.id_juicioestado on juieta.id_juicioetapa = jui.id_juicioetapa left outer join
--	dbo.tg_personas as eje  (nolock) on jui.id_ejecutivo = eje.id_persona left outer join
--	dbo.ju_gestiones as gest  (nolock) on jui.id_gestion = gest.id_gestion inner join
--    dbo.tg_personas as aboga  (nolock) on jui.id_abogado = aboga.id_persona inner join
--    dbo.tg_monedas as mon  (nolock) on jui.id_moneda = mon.id_moneda

-- OMITIDO 08022018 CMC
--FROM ju_juicios  AS jui WITH (nolock) 
--left outer JOIN ju_juiciosgestiones ON jui.id_juicio = ju_juiciosgestiones.id_juicio 
--left outer JOIN  ju_gestiones AS gest WITH (nolock) ON ju_juiciosgestiones.id_gestion = gest.id_gestion
--RIGHT OUTER JOIN ju_juiciosetapas AS juieta WITH (nolock) ON juieta.id_juicioetapa = jui.id_juicioetapa 
--INNER JOIN tg_personas AS controlador WITH (nolock) ON jui.id_controlador = controlador.id_persona 
--INNER JOIN ju_juiciosTipos AS juitip WITH (nolock) ON jui.id_juiciotipo = juitip.id_juiciotipo 
--INNER JOIN ju_tribunales AS tri WITH (nolock) ON jui.id_tribunal = tri.id_tribunal 
--INNER JOIN ub_ciudades AS ciu WITH (nolock) ON tri.id_ciudad = ciu.id_ciudad 
--INNER JOIN ju_juiciosEstados AS juiest WITH (nolock) ON jui.id_juicioestado = juiest.id_juicioestado 
--LEFT OUTER JOIN tg_personas AS eje WITH (nolock) ON jui.id_ejecutivo = eje.id_persona 
--INNER JOIN tg_personas AS aboga WITH (nolock) ON jui.id_abogado = aboga.id_persona 
--INNER JOIN tg_monedas AS mon WITH (nolock) ON jui.id_moneda = mon.id_moneda 

FROM            ju_gestiones AS gest WITH (nolock) INNER JOIN
                         ju_juiciosgestiones ON gest.id_gestion = ju_juiciosgestiones.id_gestion RIGHT OUTER JOIN
                         ju_juicios AS jui WITH (nolock) ON ju_juiciosgestiones.id_gestion = jui.id_gestion AND ju_juiciosgestiones.id_juicio = jui.id_juicio RIGHT OUTER JOIN
                         ju_juiciosetapas AS juieta WITH (nolock) ON juieta.id_juicioetapa = jui.id_juicioetapa INNER JOIN
                         tg_personas AS controlador WITH (nolock) ON jui.id_controlador = controlador.id_persona INNER JOIN
                         ju_juiciosTipos AS juitip WITH (nolock) ON jui.id_juiciotipo = juitip.id_juiciotipo INNER JOIN
                         ju_tribunales AS tri WITH (nolock) ON jui.id_tribunal = tri.id_tribunal INNER JOIN
                         ub_ciudades AS ciu WITH (nolock) ON tri.id_ciudad = ciu.id_ciudad INNER JOIN
                         ju_juiciosEstados AS juiest WITH (nolock) ON jui.id_juicioestado = juiest.id_juicioestado LEFT OUTER JOIN
                         tg_personas AS eje WITH (nolock) ON jui.id_ejecutivo = eje.id_persona INNER JOIN
                         tg_personas AS aboga WITH (nolock) ON jui.id_abogado = aboga.id_persona INNER JOIN
                         tg_monedas AS mon WITH (nolock) ON jui.id_moneda = mon.id_moneda
where
    jui.id_juicio = @id_juicio

update @datosjuicios
set id_abogadooriginal  = paso.id_abogadooriginal,
    id_abogadonuevo     = paso.id_abogadonuevo,
    re_fecha            = paso.fecha_asignacion,
    aboAnterior         = paso.aboanterior,
    aboActual           = paso.aboactual
from(
    --select     
    --        dbo.ju_reasignaciones.id_juicio, 
    --        dbo.ju_reasignaciones.re_reasignación, 
    --        dbo.ju_reasignaciones.id_abogadooriginal, 
    --        dbo.ju_reasignaciones.id_abogadonuevo, 
    --        max(dbo.ju_reasignaciones.re_fecha) as fecha_asignacion, 
    --        abogadoanterior.pe_nombrecompleto as aboanterior, 
    --        abogadoactual.pe_nombrecompleto as aboactual
    --from         
    --    dbo.ju_reasignaciones (nolock)  inner join
    --    dbo.tg_personas as abogadoanterior (nolock)  on dbo.ju_reasignaciones.id_abogadooriginal = abogadoanterior.id_persona inner join
    --    dbo.tg_personas as abogadoactual (nolock)  on dbo.ju_reasignaciones.id_abogadonuevo = abogadoactual.id_persona
    --group by 
    --    dbo.ju_reasignaciones.id_juicio, dbo.ju_reasignaciones.re_reasignación, dbo.ju_reasignaciones.id_abogadooriginal, 
    --    dbo.ju_reasignaciones.id_abogadonuevo, abogadoanterior.pe_nombrecompleto, abogadoactual.pe_nombrecompleto
        
  select    distinct 
            dbo.ju_reasignaciones.id_juicio, 
            dbo.ju_reasignaciones.re_reasignación, 
            dbo.ju_reasignaciones.id_abogadooriginal, 
            dbo.ju_reasignaciones.id_abogadonuevo, 
            max(dbo.ju_reasignaciones.re_fecha) as fecha_asignacion, 
            abogadoanterior.pe_nombrecompleto as aboanterior, 
            abogadoactual.pe_nombrecompleto as aboactual
    from         
        dbo.ju_reasignaciones (nolock)  inner join
        dbo.tg_personas as abogadoanterior (nolock)  on dbo.ju_reasignaciones.id_abogadooriginal = abogadoanterior.id_persona inner join
        dbo.tg_personas as abogadoactual (nolock)  on dbo.ju_reasignaciones.id_abogadonuevo = abogadoactual.id_persona inner join 
        (select dbo.ju_reasignaciones.id_juicio, max(dbo.ju_reasignaciones.id_reasignacion) id_reasignacion
		from dbo.ju_reasignaciones (nolock)
		group by dbo.ju_reasignaciones.id_juicio) as re on ju_reasignaciones.id_reasignacion = re.id_reasignacion
	--where ju_reasignaciones.id_juicio=368308        
    group by 
        dbo.ju_reasignaciones.id_juicio, dbo.ju_reasignaciones.re_reasignación, dbo.ju_reasignaciones.id_abogadooriginal, 
        dbo.ju_reasignaciones.id_abogadonuevo, abogadoanterior.pe_nombrecompleto, abogadoactual.pe_nombrecompleto   
) as paso inner join @datosjuicios as t  on paso.id_juicio = t.id_juicio


update @datosjuicios
set
	tiene_ddaa = tiene
from 
( select 1 tiene from ju_demandas_adetalles inner join @datosjuicios dj on dj.id_cliente = ju_demandas_adetalles.id_persona) s


--select * from ju_demandas_adetalles where


--------------------------------------------------------------------------------------------------------
-- Datos del juicio
--------------------------------------------------------------------------------------------------------

select
    orden,
    id_juicio,			
    id_controlador,	    
    Controlador,         
    id_ejecutivo,        
    ej_ejecutivo,        
    Ejecutivo,           
    id_juiciotipo,       
    jt_juiciotipo,       
    ju_rol,              
    id_tribunal,         
    tr_tribunal,         
    id_ciudad ,          
    ci_ciudad,           
    ju_fechaini,         
    id_juicioestado ,    
    je_juicioestado,     
    id_juicioetapa,      
    je_juicioetapa,      
    id_moneda,           
    ju_montodemanda,     
    id_gestion,          
    ge_gestion,          
    id_abogadooriginal,  
    id_abogadonuevo,     
    re_fecha,            
    aboAnterior,         
    aboActual,
    id_Abogado,
    abogado ,
    id_cliente,
    mo_moneda,
    mo_sigla ,dbo.fn_ut_fecha_trunca_horas(ju_fecreactivacion)   ju_fecreactivacion,
    isnull(tiene_ddaa,0) tiene_ddaa
from
    @datosjuicios



select 
    @MARCA_NOTIFICAR = dbo.fn_ut_constantes('MARCA_NOTIFICAR'),
    @id_avaltipo     = dbo.fn_ut_constantes('TAVAL_Deudor') 







-----------------------------------------------------------------
-- Datos clientes
-----------------------------------------------------------------
insert into @datosclientes(
    id_juicio,              
    ju_rol,                 
    id_organizacion,        
    or_organizacion,        
    id_subbanca,            
    sb_subbanca,            
    pe_rut,                 
    pe_nombrecompleto,      
    pu_direccion,           
    pu_fonos,               
    pu_email,
    id_cliente,
    of_oficina,
    pe_flgenquiebra             
    )
select     
    jui.id_juicio, 
    jui.ju_rol, 
    jui.id_organizacion, 
    org.or_organizacion, 
    subban.id_subbanca, 
    subban.sb_subbanca, 
    per.pe_rut, 
    per.pe_nombrecompleto, 
    dbo.fn_ut_trunca_datos(pu.pu_direccion,50),-- pu.pu_direccion, 
    pu.pu_fonos, 
    pu.pu_email,
    jui.id_cliente,
    of_oficina,
    pe_flgenquiebra
FROM         or_organizacion AS org INNER JOIN
	ju_juicios AS jui INNER JOIN
	tg_personas AS per ON jui.id_cliente = per.id_persona ON org.id_organizacion = jui.id_organizacion INNER JOIN
	or_subbancas AS subban INNER JOIN
	tg_clientes AS cli ON subban.id_subbanca = cli.id_subbanca INNER JOIN
	or_oficinas AS ofi ON cli.id_oficina = ofi.id_oficina ON per.id_persona = cli.id_persona LEFT OUTER JOIN
	tg_personasubicaciones AS pu ON per.id_persona = pu.id_persona
where
    jui.id_juicio = @id_juicio

update @datosclientes
set   n_garantiasgenerales = paso.n_garantiasgenerales
from
    (
    select 
        count(id_garantia) as n_garantiasgenerales,
        id_persona as id_cliente
    from
        ju_garantias  (nolock) 
    where 
        id_operacion is null and ga_fechasalida is null
    group by id_persona
    ) as paso inner join @datosclientes as t on paso.id_cliente = t.id_cliente



update @datosclientes
set   n_bienes = isnull(paso.n_bienes,0)
from
    (
    select 
        count(id_personabien) n_bienes,
        id_persona as id_cliente
     from 
        tg_personasbienes  (nolock) 
     group by id_persona
    ) as paso inner join @datosclientes as t on paso.id_cliente = t.id_cliente



update @datosclientes
set   n_juicios = isnull(paso.n_juicios,0)
from
    (
    select 
        count(id_juicio) as n_juicios,
        id_cliente as id_cliente
    from ju_juicios  (nolock) 
    where 
        id_juicio <> @id_juicio
     group by id_cliente
    ) as paso inner join @datosclientes as t on paso.id_cliente = t.id_cliente


select @id_cliente = id_cliente
from ju_juicios
where id_juicio = @id_juicio


select @n_notificaciones_deudor = count(id_gestion)
from ju_juiciosgestiones
where id_persona = @id_cliente and
      id_gestion in (select id_gestion
					 from ju_gestionesmarcas
					 where id_marca = @MARCA_NOTIFICAR)
	  and id_juicio = @id_juicio	

select @n_notificaciones_otros = count(id_gestion)
from ju_juiciosgestiones
where id_persona <> @id_cliente and
      id_gestion in (select id_gestion
					 from ju_gestionesmarcas
					 where id_marca = @MARCA_NOTIFICAR)
	  and id_juicio = @id_juicio	

update @datosclientes
set   id_operacionaval = isnull(paso.id_operacionaval,0)
from
    (


select distinct count(id_persona) id_operacionaval, id_persona id_cliente, id_juicio
from(
    select distinct tg_personas.id_persona, ju_juicios.id_juicio
    from         
        op_operacionesjuicios inner join
        ju_juicios on op_operacionesjuicios.id_juicio = ju_juicios.id_juicio inner join
        op_operacionesavales inner join
        tg_personas on op_operacionesavales.id_persona = tg_personas.id_persona on 
        op_operacionesjuicios.id_operacion = op_operacionesavales.id_operacion inner join
        op_avalestipos aval on op_operacionesavales.id_avaltipo = aval.id_avaltipo
    where
        ju_juicios.id_juicio = @id_juicio and
        op_operacionesavales.id_avaltipo <> @id_avaltipo
) as p 
    group by id_persona,id_juicio
    ) as paso inner join @datosclientes as t on paso.id_juicio = t.id_juicio



update @datosclientes
set n_notificaciones_deudor = isnull(@n_notificaciones_deudor,0),
	n_notificaciones_otros  = isnull(@n_notificaciones_otros,0)

--------------------------------------------------------------------------------------------------------
-- Datos clientes
--------------------------------------------------------------------------------------------------------
select 
    orden,                
    id_juicio,            
    ju_rol,               
    id_organizacion,      
    or_organizacion ,     
    id_subbanca,          
    sb_subbanca ,         
    pe_rut,               
    pe_nombrecompleto ,   
    pu_direccion,         
    pu_fonos ,            
    pu_email,             
    n_juicios,            
    n_garantiasgenerales ,
    n_bienes ,            
    id_cliente,
    of_oficina,
    id_operacionaval,
	n_notificaciones_deudor,
	n_notificaciones_otros,
	pe_flgenquiebra
from
    @datosclientes




select 
    @CONST_GASTOS_DOC_CEN_PAGO  =  dbo.fn_ut_constantes('CONST_GASTOS_DOC_CEN_PAGO'),
    @CONST_GASTOS_DOC_A_PAGO  =  dbo.fn_ut_constantes('CONST_GASTOS_DOC_A_PAGO'),
    @CONST_Gasto_Rechazado      =  dbo.fn_ut_constantes('CONST_Gasto_Rechazado')


       select 
        @pagado   = sum(t.pagado),
        @porpagar = sum(t.porpagar),
        @total = sum(t.tpagado+t.tporpagar)
        from(
            select  
               case when id_gastodocestado = @CONST_GASTOS_DOC_CEN_PAGO then ga_monto else 0 end as pagado,
               case when id_gastodocestado = @CONST_GASTOS_DOC_A_PAGO then ga_monto else 0 end as porpagar,
               case when id_gastodocestado = @CONST_GASTOS_DOC_CEN_PAGO then ga_monto else 0 end as tpagado,
               case when id_gastodocestado = @CONST_GASTOS_DOC_CEN_PAGO then 0 else ga_monto end as tporpagar
            from 
                ga_gastos g left outer join 
                ga_gastosdocs gd on g.id_gastodoc = gd.id_gastodoc
            where 
                g.id_gastoestado <>  @CONST_Gasto_Rechazado and
                id_juiciocargo = @id_juicio
        )as t 
 
        select 
            @capital   = sum(pa_capital),
            @intereses = sum(pa_intereses)
        from
            pa_pagos
        where
            id_operacion in(
                select 
                        id_operacion  
                    from 
                        op_operacionesjuicios
                where 
                    id_juicio = @id_juicio
            )


select
    isnull(@pagado,0) as pagado, isnull(@porpagar,0) as porpagar, isnull(@capital,0) as capital,  isnull(@intereses,0) as intereses,isnull(@total,0) as total


/*WORKFLOW JUICIO*/
SELECT COUNT(1) CANT_WORKFLOW FROM WF_REMATES
WHERE ID_JUICIO = @id_juicio
AND RE_FECHAFINREAL IS NULL




-----------INSERTO REGISTRO EN TABLA LOGS DE CONSULTA DE RUT --------------------

	--INSERT INTO [tg_tablas_logs]
	--	(
	--	[id_personausuario]
	--	,[tl_consuser]
	--	,[tl_fecha]
	--	,[id_cliente] 
	--	,[tl_rutcliente]
	--	,[id_juicio]
	--	,[id_abogado]
	--	,[tl_acceso])
	--SELECT
	--	@id_personausuario
	--	,@us_consuser
	--	,getdate()
	--	,id_cliente
	--	,cl_rut
	--	,id_juicio
	--	,id_abogado
	--	,'Ficha Juicio'
	--FROM ju_juicios 
	--WHERE id_juicio=@id_juicio	

-- control de errores
if @@error <> 0 goto on_error 
select 0,''
return 0

ON_ERROR:
select -1, 'Se Produjo un error en la transacción y se abortó'
return -1

Error_Params:
select -2, 'Problemas con los parámetros '
return -2









GO


