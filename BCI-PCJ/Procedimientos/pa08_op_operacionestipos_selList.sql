IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa08_op_operacionestipos_selList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa08_op_operacionestipos_selList]
GO

CREATE PROCEDURE [dbo].[pa08_op_operacionestipos_selList]
@id_operaciontipo as int = null,
@op_operaciontipo as varchar(30) = '',
@op_esaunvencimiento as int = 2,
@op_flgEsCHIP as int = 2,
@op_codigonova as int = null,
@op_flgEsCONSUMO as int = null

/*
Creador por : DS-GFONTECILLA
Fecha Creación: 22-03-2013 12:27:27
Procedimiento:pa08_op_operacionestipos_sel
Modificado por:
Fecha Modificación
llamada de test     :  
    pa08_op_operacionestipos_sel @id_operaciontipo,@op_operaciontipo,@op_esaunvencimiento,@op_flgEsCHIP,@op_codigonova,@op_flgEsCONSUMO
    pa08_op_operacionestipos_sel 0, '', 0, 0, 0, 0

Resultado: Procedimiento de tipo nominas, devuelve selecciones ordenadas de acuerdo al criterio
*/

as

-----------------------------------------------------------------------------
-- Declaracion de variables para construccion del select
-----------------------------------------------------------------------------
declare @strorder as varchar(1000) 
declare @sqlstr as varchar(8000) 
declare @strfiltro as varchar(8000)

-- construccion de la parte select de la consulta
select @sqlstr = 
'
Select 
     [dbo].[op_operacionestipos].[id_operaciontipo],
     [dbo].[op_operacionestipos].[op_operaciontipo],
     cast(isnull([dbo].[op_operacionestipos].[op_esaunvencimiento],0) as bit) [op_esaunvencimiento],
     cast(isnull([dbo].[op_operacionestipos].[op_flgEsCHIP],0) as bit) [op_flgEsCHIP],
     [dbo].[op_operacionestipos].[op_codigonova],
     cast(isnull([dbo].[op_operacionestipos].[op_flgEsCONSUMO],0) as bit) [op_flgEsCONSUMO],
	 cast(isnull([dbo].[op_operacionestipos].[op_esggee],0) as bit) [op_esggee],
	 cast(isnull([dbo].[op_operacionestipos].[op_escae],0) as bit) [op_escae], 
	 [dbo].[op_operacionestipos].[ot_plazosasignacionSpeed],
	CAST
	(
		BINARY_CHECKSUM
		(
			[dbo].[op_operacionestipos].[id_operaciontipo],
			[dbo].[op_operacionestipos].[op_operaciontipo],
			[dbo].[op_operacionestipos].[op_esaunvencimiento],
			[dbo].[op_operacionestipos].[op_flgEsCHIP],
			[dbo].[op_operacionestipos].[op_codigonova],
			[dbo].[op_operacionestipos].[op_flgEsCONSUMO],
			[dbo].[op_operacionestipos].[op_esggee],
			[dbo].[op_operacionestipos].[op_escae],
			[dbo].[op_operacionestipos].[ot_plazosasignacionSpeed]
		) AS nvarchar(4000)
	) AS BinaryChecksum 
From  
[dbo].[op_operacionestipos]
'
select @strfiltro=''
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.op_operacionestipos.id_operaciontipo','int', @id_operaciontipo,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.op_operacionestipos.op_operaciontipo','str', @op_operaciontipo,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.op_operacionestipos.op_esaunvencimiento','bit', @op_esaunvencimiento,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.op_operacionestipos.op_flgEsCHIP','bit', @op_flgEsCHIP,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.op_operacionestipos.op_codigonova','int', @op_codigonova,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.op_operacionestipos.op_flgEsCONSUMO','int', @op_flgEsCONSUMO,'')
select @strorder= ' order by [dbo].[op_operacionestipos].[op_operaciontipo]'

-- Construye el sqlstr agregando los filtros (where)
select @sqlstr = @sqlstr + @strfiltro + @strorder
exec(@sqlstr)
