IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa_carga_masiva_pagos_chip_ftp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa_carga_masiva_pagos_chip_ftp]
GO

CREATE PROCEDURE [dbo].[pa_carga_masiva_pagos_chip_ftp]
(
	@nombre_archivo			varchar(50)		= 'NOFILE',
	@nombre_archivo_log		varchar(50)		= ''
)
AS

/*
Usuario Modificación	: EROCO
Fecha Modificación		: 10-09-2019 se agregan mejoras al speed.
*/


SET NOCOUNT ON;

declare		@param_nom_archivo							varchar(1000);
declare 	@param_fecha								varchar(1000);
declare		@param_directorio_log						varchar(1000);
declare		@param_nom_archivo_log						varchar(1000);

declare     @param_temp_cursor_ingreso_pa_pagosTmp_chip		int;
declare     @param_temp_cursor_ingreso_pa				int;
declare     @param_temp_cursor_ingreso_fichas			int;
			
declare		@queryArchivo								varchar(3000);
declare		@queryCamposObligatorio						varchar(3000);
declare		@queryFormato								varchar(3000);
declare		@queryDataSistema							varchar(3000);
declare		@query_noencontrados 						varchar(3000) = '';
declare		@queryIngreso								varchar(3000);

DECLARE		@REG_CARGADOS								INT = 0;
DECLARE		@MSG_REG_CARGADOS							VARCHAR(2000) = ''
DECLARE		@NO_ENCONTRADOS								INT = 0;
DECLARE		@MSG_NO_ENCONTRADOS							VARCHAR(2000) = ''
DECLARE		@REG_ERRORES								INT = 0;
DECLARE		@MSG_REG_ERRORES							VARCHAR(200) = ''

DECLARE		@ftpHost									varchar(8000)
DECLARE		@ConectionStringBulk						varchar(8000)
DECLARE     @fisDirectorio								varchar(8000)

--para prueba, se borra esta información
truncate table pa_pagosChipTMP; 

set @param_fecha = replace(convert(varchar, getdate(), 103),'/',''); -- ddMMYYYY
set @param_nom_archivo = @nombre_archivo;

SET @ftpHost = (select top 1 ct_constantevalor from tg_constantes where ct_constante = 'ftpHost');
SET @ConectionStringBulk = (select top 1 ct_constantevalor from tg_constantes where ct_constante = 'ConectionStringBulk');
SET @fisDirectorio = 'd:\prod_app\cargas_ftp\juicios\dir\';
set @param_directorio_log = 'd:\prod_app\cargas_ftp\';


set @param_nom_archivo_log = @nombre_archivo_log; --'LogFtpIVCD_' + @param_fecha + '.txt';

Exec pa_clr_cargaMasiva_log_texto
@texto = '-----------Inicio-----------------------------------',
@LogDirectorio = @param_directorio_log,
@LogArchivo = @param_nom_archivo_log;

/*Realiza la recuperacion del archivo ftp y lo guarda en una tabla*/
Exec pa_clr_cargaMasiva_SFTP
@ftpHost				= @ftpHost,				--direccion ftp
@ConectionStringBulk	= @ConectionStringBulk, --string de coneccion para la carga de bulk
@fisDirectorio			= @fisDirectorio,		--directorio fisico de servidor para temporales
@ftpUser				= 'ftpbci',				--usuario ftp
@ftpPassword			= '3R45tfmG',			--contraseña tfp
@ftpArchivo				= @param_nom_archivo,	--archivo a recuperar del ftp
@fisArchivo				= 'cargamasivapagoschip.tmp',  --archivo temporal
@fisDelimitador			= '|',					--dilimitador de columnas del archivo rescatado
@nombreTabla			= 'pa_pagosChipTMP',  -- nombre de la tabla en cual se van a insertar los valores
@ColumnasIngreso		= 'CEDENTE,RUT_CLIENTE,DV_CLIENTE,OPERACION_ORIGINAL_ASICOM,OPERACION_VIGENTE_BCI,TIPO_PRODUCTO,FECHA_VENCIMIENTO,MONTO_PAGO,SALDO_MORA,INTERES_MORA,GASTO_COBRANZA,HONORARIO_JUDICIAL,COD_SUC_PAGO,COD_SUC_LMS,MARCA_CONDONACION,COND_CAPITAL,INTERES,COND_HONORARIOS,COND_GASTOS_COBRANZA,FECHA_PAGO,MOTIVO_PAGO,TIPO_CARGO,OFICINA,COD_CARGO,ESTADO_CANCELACION,VENCIMIENTO_PAGADO,CODIGO_DESTINO,DESCRIPCION_CODIGO_DESTINO,FECHA_PROCESO,FECHA_CONTABLE,MONTO_PAGO_ORIG,MONEDA_ORIG,VALOR_CONVERSION,SALDO_INSOLUTO,TIP_AC,CAMPO_LIBRE_1,CAMPO_LIBRE_2,CAMPO_LIBRE_3,CAMPO_LIBRE_4,CAMPO_LIBRE_5,CAMPO_LIBRE_6,CAMPO_LIBRE_7,CAMPO_LIBRE_8,CAMPO_LIBRE_9,CAMPO_LIBRE_10,CAMPO_LIBRE_11,CAMPO_LIBRE_12,CAMPO_LIBRE_13,CAMPO_LIBRE_14,CAMPO_LIBRE_15,CAMPO_LIBRE_16,CAMPO_LIBRE_17,CAMPO_LIBRE_18,CAMPO_LIBRE_19,CAMPO_LIBRE_20,CAMPO_LIBRE_21,CAMPO_LIBRE_22,CAMPO_LIBRE_23,CAMPO_LIBRE_24', -- nombre de columnas y posicion en cual se obtendran los valores del archivo, para luego ser copiado hacia la tabla. (separados por ",")
@columnaIdentidad		= 'id_temp',			-- columna de identidad de la tabla
@columnaFechaIngreso	= 'fechaingreso',		-- columna que tendra la fecha de carga (recomendable datetime)
@columnaArchivo			= 'archivo',			-- Columna en cual se guardara el nombre del archivo cargado (recomendable varchar(100))
@ColumnaNumeroLinea		= 'logLinea',			-- columna en cual se guardara la linea de los registros obtenidos (recomendable int)
@ColumnaLogError		= 'logErrorArchivo',	-- columna en cual se guardara el error encontrado. largo de campos, no castiable.
@FormatoFecha			= 'dd/MM/yyyy',			-- 'dd/MM/yyyy' 'yyyy/MM/dd' formato de fecha en cual se recuperarn los valores del archivo
@tieneCabezera			= 'true',				-- si obtiene el valor de la primera linea
@tieneLog				= 'true',
@LogDirectorio			= @param_directorio_log,
@LogArchivo				= @param_nom_archivo_log,
@ftpProtocoloSFTP		= 'false',
@ftpTLS					= 'false',
@ftpSSL					= 'false',
@Codificacion			= 'ANSI'

/*
es_valido tiene 3 estados para el procedimiento
null = sin verificar o estado inicial
0 = no va a ser cargado, tiene un error
1 = esta listo para ser cargado
*/

Declare @archivo_vacio as int
select @archivo_vacio = isnull(count(1),0) from pa_pagosChipTMP

IF @archivo_vacio = 0 BEGIN

	------------------------------------------------------------------------------------------------------------------------------------------------------
	Exec pa_clr_cargaMasiva_log_texto
	@texto = '-----------Errores del archivo------------------------------',
	@LogDirectorio = @param_directorio_log,
	@LogArchivo = @param_nom_archivo_log;

	Exec pa_clr_cargaMasiva_log_texto
	@texto = 'Archivo informado se encuentra vacío o no se encuentra bien informado.',
	@LogDirectorio = @param_directorio_log,
	@LogArchivo = @param_nom_archivo_log

	GOTO FIN;
END

/*******************************************************************************************************************************************************/
/*log con errores en las filas del archivo*/
/*actualizacion para saber si la fila tiene error de archivo de clr*/
print 'FORMATO'
update pa_pagosChipTMP set es_archivo = 0 where isnull(logErrorArchivo,'') <> '';
update pa_pagosChipTMP set logErrorCamposObligatorios = '',logErrorFormatos = '',logErrorDataSistema = '';

UPDATE pa_pagosChipTMP SET es_valido=0, es_archivo = 0, logErrorArchivo = ' - Formato incorrecto.'
where ltrim(rtrim(lower(logErrorArchivo))) like '%formato incorrecto%';

Exec pa_clr_cargaMasiva_log_texto
@texto = '-----------Errores del archivo------------------------------',
@LogDirectorio = @param_directorio_log,
@LogArchivo = @param_nom_archivo_log;

set @queryArchivo = 'select ''Linea'',logLinea,logErrorArchivo from pa_pagosChipTMP where es_archivo = 0';

Exec pa_clr_cargaMasiva_log_query @query = @queryArchivo,@LogDirectorio = @param_directorio_log,@LogArchivo = @param_nom_archivo_log;

/*******************************************************************************************************************************************************/
/*proceso que valida obligatoriedad y formato de los campos*/
print 'pa_carga_masiva_pagos_chip_campos_obligatorios'
exec pa_carga_masiva_pagos_chip_campos_obligatorios
@nombre_archivo	= @param_nom_archivo,
@nombre_archivo_log = @param_nom_archivo_log;

/*******************************************************************************************************************************************************/
/*proceso que valida los registros a cargar validos directamente al a base de datos*/
print 'pa_carga_masiva_pagos_chip_validaciones'
exec pa_carga_masiva_pagos_chip_validaciones
@nombre_archivo	= @param_nom_archivo,
@nombre_archivo_log = @param_nom_archivo_log,
@param_directorio_log = @param_directorio_log;

/*******************************************************************************************************************************************************/
print 'archivo'
DECLARE @ID_ARCHIVOPAGO AS INT;

INSERT INTO PA_ARCHIVOSPAGOS(AP_ARCHIVOPAGO,AP_USUARIO,AP_FECHA,AP_IP,ID_ARCHIVOPAGOTIPO)
VALUES(@param_nom_archivo,'SISTEMA',GETDATE(),'',1);

SELECT TOP 1 @ID_ARCHIVOPAGO = ISNULL(ID_ARCHIVOPAGO,0) FROM PA_ARCHIVOSPAGOS 
WHERE AP_ARCHIVOPAGO = @param_nom_archivo
AND CAST(AP_FECHA AS DATE) = CAST(GETDATE() AS DATE)
ORDER BY ID_ARCHIVOPAGO DESC;

UPDATE pa_pagosChipTMP SET id_archivopago = @ID_ARCHIVOPAGO;
/*******************************************************************************************************************************************************/
/*SE REUTILIZA PROCESO DE ELIMINACION DE TABLA PAGOS TEMPORAL VB2003*/
print 'pa_pa_pagosTmp_chip_truncate'
exec pa_pa_pagosTmp_chip_truncate;

/********************************************************************************************************************************************************/
/*Cursor para completar datos*/
print 'cursor'
DECLARE Cur_ingreso_pa CURSOR FOR
select id_temp 
from pa_pagosChipTMP;

OPEN Cur_ingreso_pa
FETCH Cur_ingreso_pa INTO @param_temp_cursor_ingreso_pa;

WHILE (@@FETCH_STATUS = 0 )
BEGIN
	
	update pa_pagosChipTMP set es_valido = 1 where id_temp = @param_temp_cursor_ingreso_pa and es_valido is null;
	
	exec pa_carga_masiva_pagos_chip_procesado
	@param_id_temp	=	@param_temp_cursor_ingreso_pa,
	@param_dir_log	= @param_directorio_log,
	@param_nom_arc_log	= @param_nom_archivo_log;
	--paso al siguiente registro
	
FETCH Cur_ingreso_pa INTO @param_temp_cursor_ingreso_pa
END

-- Cierra el cursor
CLOSE Cur_ingreso_pa
DEALLOCATE Cur_ingreso_pa

/*******************************************************************************************************************************************************/
/*REUTILIZAR PROCESOS MANUALES PARA QUE SE PROCESEN.*/
print 'pa_pa_pagosTmp_chip_actualizar_carga_masiva'
exec pa_pa_pagosTmp_chip_actualizar_carga_masiva

update pa_pagosChipTMP set id_persona = data.id_persona, id_operacion = data.id_operacion
from(
	 select id_temp,pa_pagosTmp_chip.id_operacion, pa_pagosTmp_chip.id_persona
	 from pa_pagoschiptmp
	 inner join pa_pagosTmp_chip on pa_pagosTmp_chip.id_pago = pa_pagoschiptmp.id_pago
) data
where pa_pagosChipTMP.id_temp = data.id_temp

--exec pa_pa_pagosTmp_chip_evalua
declare @errores					as int = 0;
declare @errores_pa_pagosTmp_chip		as int = 0;
declare @errores_pa_pagosChipTMP	as int = 0;

select @errores_pa_pagosTmp_chip = count(1) from pa_pagosTmp_chip where ISNULL(pa_rechazo,'') <> '';
select @errores_pa_pagosChipTMP = count(1) from pa_pagosChipTMP where es_valido = 0; -- nueva tabla -- archivo nuevo

SET @errores = @errores_pa_pagosTmp_chip + @errores_pa_pagosChipTMP

--si existe un solo error, rechaza todo el archivo LOGICA SE COPIA DEL PROCESO MANUAL
--print '@errores'
--IF @errores = 0 BEGIN

	print 'NO HAY ERRORES SE EJECUTA pa_pa_pagos_actualizar'
	-- CLONAR ESTOS 2 SPS
	exec pa_pa_pagos_actualizar_carga_masiva_chip
	exec pa_pa_pagosTmp_chip_actualizar_saldos_op 

-- 19/08/2019 CMC esto se omite ya que anteriormente si un registro estaba malo, se rechazaba la carga completa, ahora solo se rechaza la linea mala
--END
--ELSE BEGIN

	print 'HAY ERRORES SE RESCATAN LOS ERRORES'
	--pasar los errores a mi tabla temporal.
	update pa_pagosChipTMP set logErrorDataSistema = data.pa_rechazo + ' ' + logErrorDataSistema, es_datasistema = 0,es_VALIDO = 0
	from (

		select id_pago, pa_rechazo,pa_fecha,cod_operacion-- case pa_rechazo when '' then 'Pago no cargado' else pa_rechazo end pa_rechazo,
		from pa_pagosTmp_chip 
		where isnull(pa_rechazo,'') <> ''

	) data
	where pa_pagosChipTMP.operacion_original_asicom_casteado = data.cod_operacion and cast(pa_fecha as date) = cast(fecha_pago as date)

--END

print 'detalle'
------------------------------------------------------------------------------------------------------------------------------------------------------
Exec pa_clr_cargaMasiva_log_texto
@texto = '-----------Detalle por registros del archivo-----------------',
@LogDirectorio = @param_directorio_log,
@LogArchivo = @param_nom_archivo_log

Exec pa_clr_cargaMasiva_log_texto
@texto = 'Registros Cargados',
@LogDirectorio = @param_directorio_log,
@LogArchivo = @param_nom_archivo_log


set @queryIngreso = 'select distinct ''Linea'',logLinea,'+
' ''Pago ingresado correctamente'', '+
' ''Cedente:'',cedente+'','', '+
' ''Rut Cliente:'', rut_cliente_unido+'','', '+
' ''Cod. Operación:'', operacion_original_asicom_casteado+'','', '+
' ''Cod. Operación BCI:'', OPERACION_VIGENTE_BCI+'','', '+
' ''Tipo Producto:'', TIPO_PRODUCTO+'','', '+
' ''Rut cliente:'', rut_cliente_unido+'','', '+
' ''Monto Pago:'', MONTO_PAGO+'','', '+
' ''Fecha Pago:'', FECHA_PAGO+'','', '+
' ''Tipo Pago:'', tip_ac+'','', '+
' ''Motivo Pago:'', MOTIVO_PAGO+'','', '+
' ''Fecha Vencimiento:'', FECHA_VENCIMIENTO+'','', '+
' ''Numero Vencimiento Pagado:'', VENCIMIENTO_PAGADO '+
'from pa_pagosTmp_chip' + 
' inner join pa_pagosChipTMP on pa_pagosChipTMP.operacion_original_asicom_casteado = pa_pagosTmp_chip.cod_operacion ' +
' and pa_pagosChipTMP.id_archivopago = '+ cast(@ID_ARCHIVOPAGO as varchar) +' and es_valido = 1'

Exec pa_clr_cargaMasiva_log_query @query = @queryIngreso,@LogDirectorio = @param_directorio_log,@LogArchivo = @param_nom_archivo_log;
print @queryIngreso

------------------------------------------------------------------------------------------------------------------------------------------------------
print 'Registros no encontrados'
Exec pa_clr_cargaMasiva_log_texto
@texto = 'Registros no encontrados',
@LogDirectorio = @param_directorio_log,
@LogArchivo = @param_nom_archivo_log

set @query_noencontrados = 'select  distinct ''Linea'',logLinea,'+
' ''Pago no encontrado'', '+
' ''Cedente:'',cedente+'','', '+
' ''Rut Cliente:'', rut_cliente_unido+'','', '+
' ''Cod. Operación:'', operacion_original_asicom_casteado+'','', '+
' ''Cod. Operación BCI:'', OPERACION_VIGENTE_BCI+'','', '+
' ''Tipo Producto:'', TIPO_PRODUCTO+'','', '+
' ''Rut cliente:'', rut_cliente_unido+'','', '+
' ''Monto Pago:'', MONTO_PAGO+'','', '+
' ''Fecha Pago:'', FECHA_PAGO+'','', '+
' ''Tipo Pago:'', tip_ac+'','', '+
' ''Motivo Pago:'', MOTIVO_PAGO+'','', '+
' ''Fecha Vencimiento:'', FECHA_VENCIMIENTO+'','', '+
' ''Numero Vencimiento Pagado:'', VENCIMIENTO_PAGADO '+
'from pa_pagos_noencontrados ' + 
' inner join pa_pagosChipTMP on pa_pagosChipTMP.operacion_original_asicom_casteado = pa_pagos_noencontrados.cod_operacion ' +
' and pa_pagos_noencontrados.id_archivopago = '+ cast(@ID_ARCHIVOPAGO as varchar) +' and es_valido = 1'

Exec pa_clr_cargaMasiva_log_query @query = @query_noencontrados,@LogDirectorio = @param_directorio_log,@LogArchivo = @param_nom_archivo_log;
print @query_noencontrados

------------------------------------------------------------------------------------------------------------------------------------------------------
print 'Registros con errores'
Exec pa_clr_cargaMasiva_log_texto
@texto = 'Registros con errores',
@LogDirectorio = @param_directorio_log,
@LogArchivo = @param_nom_archivo_log

set @queryDataSistema = 'select ''Linea'',logLinea,	'+
' logErrorCamposObligatorios + '' '' + logErrorFormatos + '' '' + logErrorDataSistema, '+
' ''Cedente:'',cedente+'','', '+
' ''Rut cliente:'', rut_cliente_unido+'','', '+
' ''Cod. Operación:'', operacion_original_asicom_casteado+'','', '+
' ''Cod. Operación BCI:'', OPERACION_VIGENTE_BCI+'','', '+
' ''Tipo Producto:'', TIPO_PRODUCTO+'','', '+
' ''Rut cliente:'', rut_cliente_unido+'','', '+
' ''Monto Pago:'', MONTO_PAGO+'','', '+
' ''Fecha Pago:'', FECHA_PAGO+'','', '+
' ''Tipo Pago:'', tip_ac+'','', '+
' ''Motivo Pago:'', MOTIVO_PAGO+'','', '+
' ''Tipo Cargo:'', TIPO_CARGO+'','', '+
' ''Oficina:'', OFICINA+'','', '+
' ''Cod. Cargo: '', COD_CARGO+'','', '+
' ''Estado Cancelación: '', ESTADO_CANCELACION+'','', '+
' ''Vcto. Pagado:'', VENCIMIENTO_PAGADO+'','', '+
' ''Cod. Destino:'', CODIGO_DESTINO+'','', '+
' ''Descripción Cod. Destino:'', DESCRIPCION_CODIGO_DESTINO+'','', '+
' ''Fecha Proceso:'', FECHA_PROCESO+'','', '+
' ''Fecha Contable:'', FECHA_CONTABLE+'','', '+
' ''Fecha Vencimiento:'', FECHA_VENCIMIENTO+'','', '+
' ''Numero Vencimiento Pagado:'', VENCIMIENTO_PAGADO+'','', '+
' ''Monto Pago Orig.:'', MONTO_PAGO_ORIG+'','', '+
' ''Moneda Orig.:'', MONEDA_ORIG+'','', '+
' ''Valor Conversión:'', VALOR_CONVERSION+'','', '+
' ''Saldo Insoluto:'', SALDO_INSOLUTO+'','', '+
' ''Saldo Mora:'', SALDO_MORA+'','', '+
' ''Interes Mora:'', INTERES_MORA+'','', '+
' ''Gasto Cobranza:'', GASTO_COBRANZA+'','', '+
' ''Honorario Judicial:'', HONORARIO_JUDICIAL+'','', '+
' ''Cod. Sucursal Pago:'', COD_SUC_PAGO+'','', '+
' ''Marca Condonación:'', MARCA_CONDONACION+'','', '+
' ''Condonación Capital:'', COND_CAPITAL+'','', '+
' ''Interes:'', INTERES+'','', '+
' ''Cond. Honorarios:'', COND_HONORARIOS+'','', '+
' ''Cond. Gasto Cobranza:'', COND_GASTOS_COBRANZA +'','''+
'from pa_pagosChipTMP where es_valido = 0 ';

Exec pa_clr_cargaMasiva_log_query @query = @queryDataSistema,@LogDirectorio = @param_directorio_log,@LogArchivo = @param_nom_archivo_log;
print @queryDataSistema


------------------------------------------------------------------------------------------------------------------------------------------------------
/*CANTIDAD DE REGISTROS*/
print 'Resumen en cantidades'
Exec pa_clr_cargaMasiva_log_texto
@texto = '-----------Resumen en cantidades--------------------------------------',
@LogDirectorio = @param_directorio_log,
@LogArchivo = @param_nom_archivo_log

select  @REG_CARGADOS = count(data.loglinea)
from (
	select distinct 'Linea' linea,logLinea,' cod. Operación: ' msg1, cod_operacion,  ' Rut cliente: ' msg2, pa_rutcli, ' Pago ingresado correctamente' pago from pa_pagosTmp_chip
					  
	inner join pa_pagosChipTMP on pa_pagosChipTMP.operacion_original_asicom_casteado = pa_pagosTmp_chip.cod_operacion 
	and pa_pagosChipTMP.id_archivopago = @ID_ARCHIVOPAGO and es_valido = 1
) as data

set @MSG_REG_CARGADOS = 'Registros Cargados ('+ cast(@REG_CARGADOS as varchar(10)) +')';

Exec pa_clr_cargaMasiva_log_texto
@texto = @MSG_REG_CARGADOS,
@LogDirectorio = @param_directorio_log,
@LogArchivo = @param_nom_archivo_log

print 'Registros no encontrados'
SELECT @NO_ENCONTRADOS = count(distinct id_operacionnoencontrada) FROM pa_pagos_noencontrados  
inner join pa_pagosChipTMP on pa_pagosChipTMP.operacion_original_asicom_casteado = pa_pagos_noencontrados.cod_operacion 
and pa_pagos_noencontrados.id_archivopago =@ID_ARCHIVOPAGO and es_valido = 1

set @MSG_NO_ENCONTRADOS = 'Registros no encontrados ('+ cast(@NO_ENCONTRADOS as varchar(10)) +')'

Exec pa_clr_cargaMasiva_log_texto
@texto = @MSG_NO_ENCONTRADOS,
@LogDirectorio = @param_directorio_log,
@LogArchivo = @param_nom_archivo_log

print 'Registros con errores'
SELECT @REG_ERRORES = COUNT(*) FROM pa_pagosChipTMP  WHERE  es_valido = 0;
set @MSG_REG_ERRORES = 'Registros con errores ('+ cast(@REG_ERRORES as varchar(10)) +')'

Exec pa_clr_cargaMasiva_log_texto
@texto = @MSG_REG_ERRORES,
@LogDirectorio = @param_directorio_log,
@LogArchivo = @param_nom_archivo_log

	
--print '@TIENE_VALIDOS'
--/*EROCO SI EL ARCHIVO NO TIENE NINGUN ARCHIVO VALIDO DEBO ELIMINAR EL ARCHIVO LOS PAGOS NO ENCONTRADOS Y EL pa_pagosTmp_chip*/
--DECLARE @TIENE_VALIDOS as int;
--SELECT @TIENE_VALIDOS = COUNT(1) FROM pa_pagosChipTMP where es_valido = 1

--if @TIENE_VALIDOS = 0 begin

--	-- OJO QUE ESTO TB HAY QUE VER SI SE BORRA O NO
--	print 'borrar no encontrados y pagos erroneamente ingresados ya que el archivo completo esta invalido'
--	--borrar no encontrados
--	delete pa_pagos_noencontrados where id_archivopago = @ID_ARCHIVOPAGO
--	--borrar pa_pagosTmp_chip erroneos
--	delete pa_pagosTmp_chip where id_archivopago = @ID_ARCHIVOPAGO
--	--borrar historico
--	delete pa_pagoshistoricos where id_archivopago = @ID_ARCHIVOPAGO
--	--borrar archivo
--	delete PA_ARCHIVOSPAGOS where id_archivopago = @ID_ARCHIVOPAGO

--end

--print 'fin :'
FIN:
------------------------------------------------------------------------------------------------------------------------------------------------------
Exec pa_clr_cargaMasiva_log_texto
@texto = '-----------Fin--------------------------------------',
@LogDirectorio = @param_directorio_log,
@LogArchivo = @param_nom_archivo_log


