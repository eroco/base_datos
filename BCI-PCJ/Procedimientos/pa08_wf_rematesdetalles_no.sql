IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa08_wf_rematesdetalles_no]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa08_wf_rematesdetalles_no]
GO

CREATE PROCEDURE [dbo].[pa08_wf_rematesdetalles_no]
@Pagina			int = 1,
@RegistrosPorPaginas			int = 15,
@ColumnaOrden			varchar(50) = 'id_rematedetalle', 
@OrdenColumna			int = 0, 
@id_rematedetalle			int	 = 0,
@rd_rematedetalle			varchar(500)	 = '',
@id_remate			int	 = 0,
@re_remate			varchar(500)	 = '',
@rd_fechaetapa			varchar(20)	 = null,
@rd_fechaetapa_hasta			varchar(20)	 = null,
@id_personacierre			int	 = 0,
@pe_nombrecompleto			varchar(500)	 = '',
@id_personagenerador			int	 = 0,
@pe_nombrecompleto1			varchar(50)	 = '',
@id_responsable			int	 = 0,
@pe_nombrecompleto2			varchar(50)	 = '',
@us_consuser			varchar(20)	 = '',
@rd_fechaetapaingreso			varchar(20)	 = null,
@rd_fechaetapaingreso_hasta			varchar(20)	 = null,
@rd_fechafinetapa			varchar(20)	 = null,
@rd_fechafinetapa_hasta			varchar(20)	 = null,
@rd_comentario			varchar(8000)	 = '',
@rd_fechafinetapareal			varchar(20)	 = null,
@rd_fechafinetapareal_hasta			varchar(20)	 = null,
@id_remateetapa			int	 = 0,
@re_remateetapa			varchar(200)	 = '',
@re_diasplazo			int	 = 0,
@rd_esetapafinal			int	 = 2,
@id_remateresultado			int	 = 0,
@re_remateresultado			varchar(50)	 = null,
@id_remateopcion			int	 = 0,
@rd_remateopcion			varchar(50)	 = '',
@archivos					int = 0,
@dias_reales				int = 0,
@eficiencia					int = 0,
@re_urlanb					varchar(1000) = '',
@rr_remateresultado			varchar(500) = '',
@id_cliente					int = 0

/*
Creador por         : NTBK-EROCO
Fecha Creación      : 16/08/2019 9:37:58
Procedimiento       : dbo.pa08_wf_rematesdetalles_no
Modificado por      : 
Fecha Modificación  : 
llamada de test     : 
                      dbo.pa08_wf_rematesdetalles_no @Pagina, @RegistrosPorPaginas, @criterio, @ColumnaOrden, @OrdenColumna@id_rematedetalle,@rd_rematedetalle,@id_remate,@re_remate,@rd_fechaetapa,@id_persona,@us_consuser,@rd_fechaetapaingreso,@rd_fechafinetapa,@rd_comentario,@rd_fechafinetapareal,@id_remateetapa,@re_remateetapa,@re_diasplazo,@rd_esetapafinal,@id_remateresultado,@id_remateopcion

                      dbo.pa08_wf_rematesdetalles_no 1, 15, 1, '', 00, '', 0, '', null, 0, '', null, null, '', null, 0, '', 0, 0, 0, 0


Resultado           : Procedimiento de tipo nominas, devuelve selecciones ordenadas de acuerdo al criterio
*/

as


-----------------------------------------------------------------------------
--                       Declaración de variables                            
-----------------------------------------------------------------------------
Declare @Maximo              int
Declare @Minimo              int
Declare @paginas             int
Declare @totregs             int 
Declare @ordenamiento		varchar(4000)
declare @ordenascdesc		varchar(4)

Select @Maximo = (@Pagina * @RegistrosPorPaginas)
Select @Minimo = @Maximo - (@RegistrosPorPaginas - 1)
set nocount on

set rowcount @Maximo
-----------------------------------------------------------------------------
--                       Asignación  de variables                            
-----------------------------------------------------------------------------
declare @sqlstr as varchar(8000)
declare @sqlfrom as varchar(8000)
declare @strfiltro as varchar(8000)

declare @str_dias as varchar(2000)

set @str_dias = '
(
case dbo.wf_rematesdetalles.rd_fechafinetapareal when null then
	dbo.wf_rematesetapas.re_diasplazo - datediff(day,dbo.wf_rematesdetalles.rd_fechaetapa,getdate())
else
	dbo.wf_rematesetapas.re_diasplazo - datediff(day,dbo.wf_rematesdetalles.rd_fechafinetapareal,getdate())
end
)
'

declare @str_ultimo_avance as varchar(2000)
set @str_ultimo_avance = '
case ( 
		select max(rd.id_rematedetalle) 
		from  [wf_rematesdetalles] rd 
		where rd.id_remate = wf_remates.id_remate 
		and rd_fechafinetapa is null
	 ) 
when wf_rematesdetalles.id_rematedetalle then 1 
else 0 end
'
declare @str_dias_reales as varchar(2000)
set @str_dias_reales = '
	isnull(datediff(day,dbo.wf_rematesdetalles.rd_fechaetapa,getdate()),0)
'

declare @str_eficiencia as varchar(2000)
set @str_eficiencia = '
	(
		dbo.wf_rematesetapas.re_diasplazo - datediff(day,dbo.wf_rematesdetalles.rd_fechaetapa,getdate())
	)
'

declare @str_id_cliente as varchar(2000)
set @str_id_cliente = '
	(
		select top 1 ju_juicios.id_cliente
		from wf_remates
		inner join ju_juicios on ju_juicios.id_juicio = wf_remates.id_juicio
		where wf_remates.id_remate = wf_rematesdetalles.id_remate
	)
'

declare @str_archivos as varchar(2000)
set @str_archivos = '
	(
		select count(1)
		from bi_bitacoras
		inner join tg_documentos on tg_documentos.id_origen = bi_bitacoras.id_bitacora
		where tg_documentos.id_rematedetalle = wf_rematesdetalles.id_rematedetalle
	)
'


declare @tabletemp TABLE (variable varchar(2000), prefijo varchar(2000))
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.id_rematedetalle','id_rematedetalle')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.rd_rematedetalle','rd_rematedetalle')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.id_remate','id_remate')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_remates.re_remate','re_remate')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.rd_fechaetapa','rd_fechaetapa')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.id_personacierre','id_personacierre')
insert into @tabletemp (prefijo,variable) values ('dbo.tg_personas.pe_nombrecompleto','pe_nombrecompleto')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.id_personagenerador','id_personagenerador')
insert into @tabletemp (prefijo,variable) values ('tg_personas_1.pe_nombrecompleto','pe_nombrecompleto1')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.id_responsable','id_responsable')
insert into @tabletemp (prefijo,variable) values ('tg_personas_2.pe_nombrecompleto','pe_nombrecompleto2')
insert into @tabletemp (prefijo,variable) values ('dbo.ca_usuarios.us_consuser','us_consuser')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.rd_fechaetapaingreso','rd_fechaetapaingreso')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.rd_fechafinetapa','rd_fechafinetapa')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.rd_comentario','rd_comentario')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.rd_fechafinetapareal','rd_fechafinetapareal')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.id_remateetapa','id_remateetapa')
insert into @tabletemp (prefijo,variable) values ('ltrim(rtrim(dbo.wf_rematesetapas.re_remateetapa))','re_remateetapa')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesetapas.re_diasplazo','re_diasplazo')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.rd_esetapafinal','rd_esetapafinal')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.id_remateresultado','id_remateresultado')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesdetalles.id_remateopcion','id_remateopcion')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesopciones.rd_remateopcion','rd_remateopcion')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesresultados.rr_remateresultado','rr_remateresultado')

insert into @tabletemp (prefijo,variable) values (''+@str_dias+'','dias')
insert into @tabletemp (prefijo,variable) values (''+@str_dias_reales+'','dias_reales')
insert into @tabletemp (prefijo,variable) values (''+@str_eficiencia+'','eficiencia')
insert into @tabletemp (prefijo,variable) values (''+@str_archivos+'','archivos')


select @ordenascdesc = 'ASC'
if @OrdenColumna = 1 select @ordenascdesc = 'DESC'
select @ordenamiento = ' Row_Number() Over (Order by '+ prefijo + ' ' + @ordenascdesc +' ) as Norden '  
from @tabletemp where variable = @ColumnaOrden 

Select @sqlstr = ' Select top ' + cast(@Maximo as varchar) + ' 
dbo.wf_rematesdetalles.id_rematedetalle, 
dbo.wf_rematesdetalles.rd_rematedetalle, 
dbo.wf_rematesdetalles.id_remate, 
dbo.wf_remates.re_remate, 
dbo.wf_rematesdetalles.rd_fechaetapa, 
dbo.wf_rematesdetalles.id_personacierre, 
dbo.tg_personas.pe_nombrecompleto pe_nombrecompleto1, 
dbo.wf_rematesdetalles.id_personagenerador, 
tg_personas_1.pe_nombrecompleto pe_nombrecompleto2, 
dbo.wf_rematesdetalles.id_responsable, 
dbo.ca_usuarios.us_consuser,
tg_personas_2.pe_nombrecompleto,
dbo.wf_rematesdetalles.rd_fechaetapaingreso, 
dbo.wf_rematesdetalles.rd_fechafinetapa, 
dbo.wf_rematesdetalles.rd_comentario, 
dbo.wf_rematesdetalles.rd_fechafinetapareal, 
dbo.wf_rematesdetalles.id_remateetapa, 
dbo.wf_rematesetapas.re_remateetapa, 
dbo.wf_rematesetapas.re_diasplazo, 
dbo.wf_rematesdetalles.rd_esetapafinal, 
dbo.wf_rematesdetalles.id_remateresultado, 
dbo.wf_rematesresultados.rr_remateresultado, 
dbo.wf_rematesdetalles.id_remateopcion,
dbo.wf_rematesopciones.rd_remateopcion,
dbo.wf_rematesetapas.re_urlanb,
'+ @str_archivos +' as archivos,
'+ @str_dias_reales +' as dias_reales,
'+ @str_eficiencia +' as eficiencia,
'+ @str_dias +' as dias,
'+ @str_ultimo_avance +' ultimo_avance,
'+ @str_id_cliente +' id_cliente
, ' + @ordenamiento

select @sqlfrom = 
'
From [dbo].[wf_rematesdetalles]
Left Outer Join [dbo].[wf_remates] On [dbo].[wf_rematesdetalles].[id_remate] = [dbo].[wf_remates].[id_remate]
Left Outer Join [dbo].[wf_rematesetapas] On [dbo].[wf_rematesetapas].[id_remateetapa] = [dbo].[wf_rematesdetalles].[id_remateetapa]
Left Outer Join [dbo].[wf_rematesopciones] On [dbo].[wf_rematesopciones].[id_remateopcion] = [dbo].[wf_rematesdetalles].[id_remateopcion]
Left Outer Join [dbo].[tg_personas] On [dbo].[wf_rematesdetalles].[id_personacierre] = [dbo].[tg_personas].[id_persona]
Left Outer Join [dbo].[tg_personas] as [tg_personas_1] On [dbo].[wf_rematesdetalles].[id_personagenerador] = [tg_personas_1].[id_persona]
Left Outer Join [dbo].[tg_personas] as [tg_personas_2] On [dbo].[wf_rematesdetalles].[id_responsable] = [tg_personas_2].[id_persona]
Left Outer Join [dbo].[ca_usuarios] On [dbo].[ca_usuarios].[id_persona] = [dbo].[wf_rematesdetalles].[id_responsable]
Left Outer Join [dbo].[wf_rematesresultados] On [dbo].[wf_rematesresultados].[id_remateresultado] = [dbo].[wf_rematesdetalles].[id_remateresultado]
'


select @strfiltro=''
select @strfiltro=' WHERE 1=1 '
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.id_rematedetalle','int', @id_rematedetalle,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.rd_rematedetalle','str', @rd_rematedetalle,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.id_remate','int', @id_remate,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.re_remate','str', @re_remate,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.rd_fechaetapa','date', @rd_fechaetapa,@rd_fechaetapa_hasta)
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.id_personacierre','int', @id_personacierre,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_personas.pe_nombrecompleto','str', @pe_nombrecompleto,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.id_personagenerador','int', @id_personagenerador,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'tg_personas_1.pe_nombrecompleto','str', @pe_nombrecompleto1,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.id_responsable','int', @id_responsable,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'tg_personas_2.pe_nombrecompleto','str', @pe_nombrecompleto2,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.ca_usuarios.us_consuser','str', @us_consuser,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.rd_fechaetapaingreso','date', @rd_fechaetapaingreso,@rd_fechaetapaingreso_hasta)
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.rd_fechafinetapa','date', @rd_fechafinetapa,@rd_fechafinetapa_hasta)
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.rd_comentario','str', @rd_comentario,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.rd_fechafinetapareal','date', @rd_fechafinetapareal,@rd_fechafinetapareal_hasta)
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.id_remateetapa','int', @id_remateetapa,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesetapas.re_remateetapa','str', @re_remateetapa,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.rd_esetapafinal','bit', @rd_esetapafinal,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.id_remateresultado','int', @id_remateresultado,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesdetalles.id_remateopcion','int', @id_remateopcion,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesopciones.rd_remateopcion','str', @rd_remateopcion,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesresultados.rr_remateresultado','str', @rr_remateresultado,'')
 
if not @archivos  = ''
begin
	if @archivos  = -1 select @archivos  = 0
	select @strfiltro = @strfiltro + ' and 0 = ' + cast(@archivos as varchar(20))  
end

if not @re_diasplazo  = ''
begin
	if @re_diasplazo  = -1 select @re_diasplazo  = 0
	select @strfiltro = @strfiltro + ' and dbo.wf_rematesetapas.re_diasplazo = ' + cast(@re_diasplazo as varchar(20))  
end

if not @dias_reales  = ''
begin
	if @dias_reales  = -1 select @dias_reales  = 0
	select @strfiltro = @strfiltro + ' and '+ @str_dias_reales +' = ' + cast(@dias_reales as varchar(20))  
end

if not @eficiencia  = ''
begin
	if @eficiencia  = -1 select @eficiencia  = 0
	select @strfiltro = @strfiltro + ' and '+ @str_eficiencia +' = ' + cast(@eficiencia as varchar(20))  
end

select @sqlfrom = @sqlfrom + isnull(@strfiltro,'') 
-----------------------------------------------------------------------------
-- Devuelve la cantidad de registros totales
-----------------------------------------------------------------------------
declare @strtot as varchar(8000)
select @strtot = dbo.fn_ut_sqldinamico_totalregistros(@sqlfrom,@RegistrosPorPaginas)
print @strtot
exec(@strtot)

--print @strtot
select @sqlstr = 'Select 
id_rematedetalle, 
rd_rematedetalle, 
id_remate, 
re_remate, 
rd_fechaetapa, 
id_personacierre, 
pe_nombrecompleto, 
id_personagenerador, 
pe_nombrecompleto1, 
id_responsable, 
pe_nombrecompleto2, 
us_consuser, 
rd_fechaetapaingreso, 
rd_fechafinetapa, 
rd_comentario, 
rd_fechafinetapareal, 
id_remateetapa, 
re_remateetapa, 
re_diasplazo, 
rd_esetapafinal, 
id_remateresultado, 
rr_remateresultado,
id_remateopcion, 
rd_remateopcion, 
archivos,
dias_reales,
eficiencia,
ultimo_avance,
re_urlanb,
id_cliente,
 Norden 
From ( ' 
+ @sqlstr + isnull(@sqlfrom,'') + ' ) As  Paginacion 
Where 
Norden >= ' + cast(@minimo as varchar(20)) + ' and Norden <= ' + cast(@maximo as varchar(20))

print '-------------------------------------------------------------'
print @sqlstr

exec(@sqlstr)
set rowcount 0
