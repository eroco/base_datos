IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa08_op_operacionestipos_in]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa08_op_operacionestipos_in]
GO

CREATE PROCEDURE [dbo].[pa08_op_operacionestipos_in]
@op_operaciontipo as varchar(30) = '',
@op_esaunvencimiento as bit = 0,
@op_flgEsCHIP as bit = 0,
@op_codigonova as int = null,
@op_flgEsCONSUMO as bit = 0,
@op_esggee as bit = 0,
@op_escae as bit = 0,
@ot_plazosasignacionSpeed as int = null
/*
Creador por : DS-GFONTECILLA
Fecha Creación: 22-03-2013 12:27:28
Procedimiento:pa08_op_operacionestipos_in

Modificado por: EROCO
Fecha Modificación : 05-09-2019 proyecto speed marcas

Modificado por:
Fecha Modificación:

llamada de test     :  
begin tran
    pa08_op_operacionestipos_in '',0,0,0,0
rollback
anulartest : ROLLBACK(TRAN)
Resultado: Procedimiento de inserción de un nuevo registro en la tabla op_operacionestipos
*/

as

set nocount on
INSERT INTO dbo.[op_operacionestipos]
            (
            [op_operaciontipo],
            [op_esaunvencimiento],
            [op_flgEsCHIP],
            [op_codigonova],
            [op_flgEsCONSUMO],
			op_esggee ,
			op_escae,
			ot_plazosasignacionSpeed
            )
            Values
            (
            ltrim(rtrim(@op_operaciontipo)),
            @op_esaunvencimiento,
            @op_flgEsCHIP,
            @op_codigonova,
            @op_flgEsCONSUMO,
			@op_esggee ,
			@op_escae,
			@ot_plazosasignacionSpeed
            )

    RETURN SCOPE_IDENTITY()
