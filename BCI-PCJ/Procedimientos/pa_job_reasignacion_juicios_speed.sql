IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa_job_reasignacion_juicios_speed]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa_job_reasignacion_juicios_speed]
GO

CREATE procedure [dbo].[pa_job_reasignacion_juicios_speed]
as
Begin tran

-- VALIDAR QUE EL USUARIO HAYA INGRESADO CON ANTERIORIDAD LA GESTION MANDAMIENTO PARA GENERAR BITACORA, SI NO ESTA LA GESTION NO ENVIAR� BITACORA.
DECLARE @id_gestion_mandamiento			as int
declare @id_abogadospeed int
declare @ab_abogado varchar(100)

SELECT @id_gestion_mandamiento = DBO.fn_ut_parametro_constante('ID_GESTION_MANDAMIENTO')
set @id_abogadospeed = DBO.fn_ut_parametro_constante('ID_ABOGADOCARGO_SPEED')
set @ab_abogado = DBO.fn_ut_parametro_constante('NOMBRE_ABOGADOCARGO_SPEED')


DECLARE @TEMPORAL TABLE(
	ID int identity(1,1),
	idjuicio int,
	jurol varchar(200),
	clrut  varchar(200),
	clcliente  varchar(2000),
	fechaplazo smalldatetime,
	idabogadojuicio int,
	abogadojuicio varchar(200),
	idabogadojuicio_speed int,
	abogadojuicio_speed varchar(200)
)


--TODOS LOS JUICIOS CON MANDAMIENTIO
--TODOS LOS JUICIOS CON EL ABOGADO PREDETERMINADO
--TODOS LOS JUICIOS CON DEUDORES NO INHIBIDOS
INSERT INTO @TEMPORAL(IDJUICIO,jurol,clrut,clcliente,fechaplazo,idabogadojuicio,abogadojuicio,idabogadojuicio_speed,abogadojuicio_speed)
select DISTINCT ju_juiciosgestiones.ID_JUICIO,ju_rol,cl_rut,cl_cliente,ju_fechaareasignarspeed,id_abogado,ab_abogado,id_abogadospeed,ab_abogadospeed
from ju_juiciosgestiones
inner join ju_juicios on ju_juicios.id_juicio = ju_juiciosgestiones.id_juicio
inner join ju_juiciosestados on ju_juiciosestados.id_juicioestado = ju_juicios.id_juicioestado
where ju_juiciosgestiones.id_Gestion = @id_gestion_mandamiento  --si el juicio tiene la gestion mandamiento lo trae
AND [dbo].[fn_retorna_si_es_juicio_speed](ju_juiciosgestiones.ID_JUICIO) != 0 --si es juicio speed lo trae
and ju_juicios.id_abogado = @id_abogadospeed --abogado predeterminado si lo es trae el juicio
and ju_fechaareasignarspeed <= getdate() --si se cumplio la fecha a reasignar considerara el juicio
--and isnull(dbo.fn_co_estainhibidorut(cl_rut),'') = '' --deudor no puede estar inhibido
and isnull(je_flgessuspension,0) = 0 --solo juicios que no esten suspendido
and ju_fechafin is null
order by ju_juiciosgestiones.id_juicio
--if @@error <> 0 goto ON_ERROR

--select * from @temporal

declare @corr				int
declare @corrMAX			int
declare @NroInicial			int 
declare @NroFinal			int

declare @id_juicio						as int
DECLARE @id_AbogadoNuevo				AS INT
DECLARE @ab_AbogadoNuevo				AS VARCHAR(200) = ''
DECLARE @ab_AbogadoAnterior				AS VARCHAR(200) = ''
DECLARE @usuario						as varchar(20) = 'SISTEMA'
DECLARE @cuerpo_bitacora				AS VARCHAR(1000) = ''
DECLARE @FECHAHOY						AS SMALLDATETIME = NULL
set @FECHAHOY = CAST(GETDATE() AS SMALLDATETIME)

select  @NroFinal = max(id),
		@NroInicial = min(id) 
from @TEMPORAL 

set     @corr  = @NroInicial

WHILE ( @corr <= (@NroFinal) )
begin

	select	@id_juicio = idjuicio,
			@id_AbogadoNuevo = idabogadojuicio_speed,
			@ab_AbogadoNuevo = abogadojuicio_speed,
			@ab_AbogadoAnterior = abogadojuicio
	from @temporal
	where id = @corr

	if isnull(@id_AbogadoNuevo,0) != 0 begin

		/* CMC 01/08/2018 EN LA OPCION DELEGA PODER SOLO SE INFORMA LA REASIGNACION YA QUE SE REASIGNO EN LA OPCION 1*/
		select @ab_AbogadoAnterior = ab_abogado, @ab_AbogadoNuevo = ab_abogadoSpeed 
		from ju_juicios where id_juicio = @id_juicio
				
		insert into ju_reasignaciones(id_juicio,id_abogadooriginal,id_abogadonuevo,re_fecha,re_consuser)
		select
		id_juicio,
		id_abogado,
		id_abogadoSpeed,
		getdate(),
		@usuario
		from ju_juicios where id_juicio = @id_juicio
		if @@error <> 0 goto ON_ERROR

		/*27-08-2018 EROCO SPEED A SOLICITUD DE ESTEBAN LA ASIGNACION DEL ABOGADO SE REALIZARA EN ESTE PASO.*/
		UPDATE JU_JUICIOS SET 
		id_abogado = id_abogadoSpeed, 
		ab_abogado = ab_abogadoSpeed,
		ju_fechareasignacionspeed = getdate()
		WHERE ID_JUICIO = @id_juicio
		if @@error <> 0 goto ON_ERROR 


		UPDATE op_operaciones SET 
		id_abogado = ju_juicios.id_abogado,
		ab_abogado = ju_juicios.ab_abogado
		from op_operaciones o 
		inner join op_operacionesjuicios opj on o.id_operacion = opj.id_operacion
		inner join ju_juicios on ju_juicios.id_juicio = opj.id_juicio
		WHERE opj.id_juicio = @id_juicio
		if @@error <> 0 goto ON_ERROR

		UPDATE op_operacionesJuicios 
		SET id_persona = ju_juicios.id_abogado, 
			ab_abogado = ju_juicios.ab_abogado
		from op_operaciones o 
		inner join op_operacionesJuicios oj on o.id_operacion = oj.id_operacion 
		inner join ju_juicios on ju_juicios.id_juicio = oj.id_juicio
		WHERE  ju_juicios.id_juicio = @id_juicio
		if @@error <> 0 goto ON_ERROR

		/*27-08-2018 EROCO SPEED SE GENERA ASIGNACION DE CONTROLADOR*/
		select @id_AbogadoNuevo = id_Abogado from ju_juicios where id_juicio = @id_juicio
		EXEC cambio_asignacion @ID = @id_juicio, @proceso = 3, @usuario = @usuario, @sp = 'pa_speed_gestion',@v_id_abogado = @id_AbogadoNuevo
				
		--27-08-2018 
		--debo validar que el abogado nuevo ya sea participante???
		--a�adir como participante del juicio al nuevo abogado
		--y quitar a hernan como abogado. se le pregunto a hernan.
		--debo a�adir al staff del abogado nuevo?

		--SE ELIMINAN LOS ANTIGUOS PARTICIPANTES.
		DELETE [ju_participantesjuicios] WHERE ID_JUICIO = @id_juicio
		if @@error <> 0 goto ON_ERROR

		--SE A�ADE COMO NUEVO PARTICIPANTE AL ABOGADO OBTENIDO DE LA ASIGNACION
		INSERT INTO [ju_participantesjuicios]
			([id_juicio]
			,[id_persona]
			,[id_participantetipo]
			,[jp_gestiona]
			,[id_acceso]
			,[jp_fechavencimiento]
			,[id_abogadopersona])
		SELECT 
			@id_juicio
			,tg_abogadosPersonas.id_persona as id_persona
			,id_participantetipo as id_participantetipo
			,case id_participantetipo when 1 then 1 else 0 end as jp_gestiona
			,1 as id_acceso
			,null as jp_fechavencimiento
			,null id_abogadopersona
		FROM  tg_abogadosPersonas
		inner join ca_usuarios on ca_usuarios.id_persona = tg_abogadosPersonas.id_persona
		where tg_abogadosPersonas.id_abogado = (
			select tg_abogados.id_abogado from ju_juicios
			RIGHT OUTER JOIN tg_abogados ON tg_abogados.id_persona = ju_juicios.id_abogado
			where 1=1 AND JU_JUICIOS.ID_JUICIO = @id_juicio
		)
		and ca_usuarios.us_esvigente = 1
		if @@error <> 0 goto ON_ERROR 

		/*SE GENERA BITACORA*/
		SET @cuerpo_bitacora = 'Se informa del cambio de Abogado a Cargo '+ @ab_AbogadoAnterior + ' Nuevo Abogado seg�n asignaci�n Autom�tica ' + @ab_AbogadoNuevo + ' en este juicio'
		EXEC [pa_bi_bitacoras_automaticas_x_juicios_participantes_in]
		@bi_bitacora		= @cuerpo_bitacora,
		@bi_fecha			= @FECHAHOY,
		@id_persona			= 9, --(por defecto SISTEMA)
		@bi_esautomatico	= 1, --(por defecto)
		@bi_msg_user		= @usuario, --Usuario conectado al sistema
		@bi_msg_fecha		= @FECHAHOY,
		@id_origen			= @id_juicio,
		@bi_usuario			= @usuario, --Usuario conectado al sistema
		@bi_confidencial	= 0,
		@id_bitacorapadre	= null,
		@id_bitacoratipo	= 25
		if @@error <> 0 goto ON_ERROR 


		/*Generar correo*/
		--CORREO AL NUEVO ABOGADO
		DECLARE @CORREO AS VARCHAR(8000) = ''
		DECLARE @MSJ AS VARCHAR(8000) = ''
		DECLARE @ju_rol AS VARCHAR(100) = ''
		DECLARE @tr_tribunal AS VARCHAR(100) = ''
		DECLARE @cl_rut AS VARCHAR(100) = ''
		DECLARE @cl_deudor AS VARCHAR(100) = ''

		SELECT @CORREO = PU_EMAIL FROM TG_PERSONASUBICACIONES WHERE ID_PERSONA = @id_AbogadoNuevo AND PU_ESDENOTIFICACION = 1

		IF @CORREO != '' BEGIN

			select	@ju_rol = ltrim(rtrim(ju_rol)),
					@tr_tribunal = tr_tribunal,
					@cl_rut = cl_rut,
					@cl_deudor = cl_cliente
			from ju_juicios
			inner join ju_tribunales on ju_tribunales.id_tribunal = ju_juicios.id_tribunal
			where id_juicio = @id_juicio

			SET @MSJ += 'Estimado <br><br>'
			SET @MSJ += 'Se ha generado la siguiente asignaci�n del juicio Speed : <br><br>'
			SET @MSJ += '<b>Juicio :</b> ' + @ju_rol + '<br>'
			SET @MSJ += '<b>Tribunal :</b> ' + @tr_tribunal + '<br>'
			SET @MSJ += '<b>Deudor :</b> ' + @cl_deudor + '<br>'
			SET @MSJ += '<b>Rut :</b> ' + @cl_rut + '<br><br>'
			SET @MSJ += 'Saludos'

			Exec pa_clr_enviar_correo
			@config_Smtp = 'mail.plataformagroup.cl',
			@config_port = '25',
			@config_user = 'plataformajudicialbci@plataformagroup.cl',
			@config_pass = 'pcjbci$0270',
			@config_ssl = '0',
			@param_To = @CORREO,
			@param_Cc = 'eroco@plataformagroup.cl,ygonzalez@plataformagroup.cl,cmunoz@plataformagroup.cl',--'servicioalcliente@plataformagroup.cl',
			@param_From = 'plataformajudicialbci@plataformagroup.cl',
			@param_Subject = 'Proceso Speed Cambio Abogado',
			@param_body = @MSJ,
			@param_files = ''
			
		END

		/*---------------------MANEJO DE SALTOS DEL WHILE------------------*/
		if @corr = @NroFinal break

		/* LE ASIGNAMOS A LA VARIABLE @CORR EL SIGUIENTE VALOR VALIDO DEL JUICIO DEL CLIENTE */
		SELECT distinct top 1 @corr = id 
		from @TEMPORAL 
		where 1=1
		AND isnull(id,0) > @corr
		ORDER BY id	
	end --FIN isnull(@id_AbogadoNuevo,0) != 0
end


-- control de errores
if @@trancount > 0
commit tran
return 0

ON_ERROR:
If @@TRANCOUNT > 0
ROLLBACK TRAN

	/*INSERT INTO lq_log_quiebras(id_tabla,id_proceso,fecha_log,descripcion_log) 
	VALUES (@ID_PERSONA,7,getdate(), 'Se Produjo un error en la transacci�n y se abort�')
	if @@error <> 0 goto ON_ERROR */

select -1, 'Se Produjo un error en la transacci�n y se abort�'
return -1

Error_Params:

	/*INSERT INTO lq_log_quiebras(id_tabla,id_proceso,fecha_log,descripcion_log) 
	VALUES (@ID_PERSONA,7,getdate(), 'Problemas con los par�metros ')
	if @@error <> 0 goto ON_ERROR */

select -2, 'Problemas con los par�metros '
return -2
