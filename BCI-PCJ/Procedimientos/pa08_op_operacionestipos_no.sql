IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa08_op_operacionestipos_no]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa08_op_operacionestipos_no]
GO

CREATE PROCEDURE [dbo].[pa08_op_operacionestipos_no]
@Pagina			int = 1,
@RegistrosPorPaginas			int = 15,
@ColumnaOrden			varchar(50) = 'id_operaciontipo', 
@OrdenColumna			int = 0, 
@id_operaciontipo			int	 = 0,
@op_operaciontipo			varchar(30)	 = '',
@op_esaunvencimiento			int	 = 2,
@op_flgEsCHIP			int	 = 2,
@op_codigonova			int	 = 0,
@op_flgEsCONSUMO			int	 = 2,
@op_esggee			int	 = 2,
@op_escae			int	 = 2,
@ot_plazosasignacionSpeed			int	 = 0

/*
Creador por         : Emmanuel
Fecha Creación      : 23-09-2015 19:29:35
Procedimiento       : dbo.pa08_op_operacionestipos_no
Modificado por      : 
Fecha Modificación  : 
llamada de test     : 
                      dbo.pa08_op_operacionestipos_no @Pagina, @RegistrosPorPaginas, @criterio, @ColumnaOrden, @OrdenColumna@id_operaciontipo,@op_operaciontipo,@op_esaunvencimiento,@op_flgEsCHIP,@op_codigonova,@op_flgEsCONSUMO,@op_esggee

                      dbo.pa08_op_operacionestipos_no 1, 15, 1, '', 00, '', 0, 0, 0, 0, 0

					  [pa08_op_operacionestipos_no] @op_flgEsCONSUMO=1, @ot_plazosasignacionSpeed=2, @Pagina=1, @RegistrosPorPaginas=20, @ColumnaOrden='id_operaciontipo', @OrdenColumna=0
					  [pa08_op_operacionestipos_no] @op_flgEsCONSUMO=1, @op_escae=1, @ot_plazosasignacionSpeed=2, @Pagina=1, @RegistrosPorPaginas=20, @ColumnaOrden='id_operaciontipo', @OrdenColumna=0
					  [pa08_op_operacionestipos_no] @ot_plazosasignacionSpeed=2, @Pagina=1, @RegistrosPorPaginas=20, @ColumnaOrden='id_operaciontipo', @OrdenColumna=0
					  [pa08_op_operacionestipos_no] @op_escae=1, @Pagina=1, @RegistrosPorPaginas=20, @ColumnaOrden='id_operaciontipo', @OrdenColumna=0

					  [pa08_op_operacionestipos_no] @op_flgEsCONSUMO=0, @Pagina=1, @RegistrosPorPaginas=100, @ColumnaOrden='id_operaciontipo', @OrdenColumna=0
					  [pa08_op_operacionestipos_no] @op_flgEsCONSUMO=1, @Pagina=1, @RegistrosPorPaginas=100, @ColumnaOrden='id_operaciontipo', @OrdenColumna=0
					   [pa08_op_operacionestipos_no] @op_flgEsCONSUMO=2, @Pagina=1, @RegistrosPorPaginas=100, @ColumnaOrden='id_operaciontipo', @OrdenColumna=0

					   pa08_op_operacionestipos_no @Pagina=1, @RegistrosPorPaginas=20, @ColumnaOrden='id_operaciontipo', @OrdenColumna=0
Resultado           : Procedimiento de tipo nominas, devuelve selecciones ordenadas de acuerdo al criterio
*/

as


-----------------------------------------------------------------------------
--                       Declaración de variables                            
-----------------------------------------------------------------------------
Declare @Maximo              int
Declare @Minimo              int
Declare @paginas             int
Declare @totregs             int 
Declare @ordenamiento		varchar(4000)
declare @ordenascdesc		varchar(4)

Select @Maximo = (@Pagina * @RegistrosPorPaginas)
Select @Minimo = @Maximo - (@RegistrosPorPaginas - 1)
set nocount on

set rowcount @Maximo
-----------------------------------------------------------------------------
--                       Asignación  de variables                            
-----------------------------------------------------------------------------
declare @sqlstr as varchar(8000)
declare @sqlfrom as varchar(8000)
declare @strfiltro as varchar(8000)


declare @tabletemp TABLE (variable varchar(30), prefijo varchar(100))
insert into @tabletemp (prefijo,variable) values ('dbo.op_operacionestipos.id_operaciontipo','id_operaciontipo')
insert into @tabletemp (prefijo,variable) values ('dbo.op_operacionestipos.op_operaciontipo','op_operaciontipo')
insert into @tabletemp (prefijo,variable) values ('dbo.op_operacionestipos.op_esaunvencimiento','op_esaunvencimiento')
insert into @tabletemp (prefijo,variable) values ('dbo.op_operacionestipos.op_flgEsCHIP','op_flgEsCHIP')
insert into @tabletemp (prefijo,variable) values ('dbo.op_operacionestipos.op_codigonova','op_codigonova')
insert into @tabletemp (prefijo,variable) values ('dbo.op_operacionestipos.op_flgEsCONSUMO','op_flgEsCONSUMO')
insert into @tabletemp (prefijo,variable) values ('dbo.op_operacionestipos.op_esggee','op_esggee')
insert into @tabletemp (prefijo,variable) values ('dbo.op_operacionestipos.op_escae','op_escae')
insert into @tabletemp (prefijo,variable) values ('dbo.op_operacionestipos.ot_plazosasignacionSpeed','ot_plazosasignacionSpeed')

select @ordenascdesc = 'ASC'
if @OrdenColumna = 1 select @ordenascdesc = 'DESC'
select @ordenamiento = ' Row_Number() Over (Order by '+ prefijo + ' ' + @ordenascdesc +' ) as Norden '  
from @tabletemp where variable = @ColumnaOrden 

Select @sqlstr = ' Select top ' + cast(@Maximo as varchar) + ' 
dbo.op_operacionestipos.id_operaciontipo, 
dbo.op_operacionestipos.op_operaciontipo, 
dbo.op_operacionestipos.op_esaunvencimiento, 
dbo.op_operacionestipos.op_flgEsCHIP, 
dbo.op_operacionestipos.op_codigonova, 
dbo.op_operacionestipos.op_flgEsCONSUMO, 
dbo.op_operacionestipos.op_esggee,
dbo.op_operacionestipos.op_escae,
dbo.op_operacionestipos.ot_plazosasignacionSpeed
, ' + @ordenamiento

select @sqlfrom = 
'From 
    [dbo].[op_operacionestipos]
'

select @strfiltro=''
select @strfiltro='WHERE 1=1 '
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.op_operacionestipos.id_operaciontipo','int', @id_operaciontipo,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.op_operacionestipos.op_operaciontipo','str', @op_operaciontipo,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'isnull(dbo.op_operacionestipos.op_esaunvencimiento,0)','bit', @op_esaunvencimiento,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'isnull(dbo.op_operacionestipos.op_flgEsCHIP,0)','bit', @op_flgEsCHIP,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.op_operacionestipos.op_codigonova','int', @op_codigonova,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'isnull(dbo.op_operacionestipos.op_flgEsCONSUMO,0)','bit', @op_flgEsCONSUMO,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'isnull(dbo.op_operacionestipos.op_esggee,0)','bit', @op_esggee,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'isnull(dbo.op_operacionestipos.op_escae,0)','bit', @op_escae,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.op_operacionestipos.ot_plazosasignacionSpeed','int', @ot_plazosasignacionSpeed,'')

if not @ot_plazosasignacionSpeed  = ''
begin
        if @ot_plazosasignacionSpeed  = -1 select @ot_plazosasignacionSpeed  = 0
        select @strfiltro = @strfiltro + ' and dbo.op_operacionestipos.ot_plazosasignacionSpeed = ' + cast(@ot_plazosasignacionSpeed as varchar(20))  
end


select @sqlfrom = @sqlfrom + isnull(@strfiltro,'') 

-----------------------------------------------------------------------------
-- Devuelve la cantidad de registros totales
-----------------------------------------------------------------------------
declare @strtot as varchar(8000)
select @strtot = dbo.fn_ut_sqldinamico_totalregistros(@sqlfrom,@RegistrosPorPaginas)
print @strtot
exec(@strtot)

--print @strtot
select @sqlstr = 'Select 
id_operaciontipo, 
op_operaciontipo, 
CAST(isnull(op_esaunvencimiento ,0) AS BIT) op_esaunvencimiento, 
CAST(isnull(op_flgEsCHIP ,0) AS BIT) op_flgEsCHIP, 
op_codigonova, 
CAST(isnull(op_flgEsCONSUMO,0) AS BIT) op_flgEsCONSUMO, 
CAST(isnull(op_esggee,0) AS BIT) op_esggee, 
CAST(isnull(op_escae,0) AS BIT) op_escae,
ot_plazosasignacionSpeed,
 Norden 
From ( ' 
+ @sqlstr + isnull(@sqlfrom,'') + ' ) As  Paginacion 
Where 
Norden >= ' + cast(@minimo as varchar(20)) + ' and Norden <= ' + cast(@maximo as varchar(20))

print @sqlstr
exec(@sqlstr)
set rowcount 0
