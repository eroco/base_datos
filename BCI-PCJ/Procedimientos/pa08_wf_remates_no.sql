IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa08_wf_remates_no]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa08_wf_remates_no]
GO

CREATE PROCEDURE [dbo].[pa08_wf_remates_no]
@Pagina			int = 1,
@RegistrosPorPaginas			int = 100,
@ColumnaOrden			varchar(50) = 'rp_remateprorroga', 
@OrdenColumna			int = 0, 
@id_remate			int	 = 0,
@re_remate			varchar(500)	 = '',
@id_juicio			int	 = 0,
@ju_rol			varchar(20)	 = '',
@id_tribunal			int	 = 0,
@tr_tribunal			varchar(50)	 = '',
@id_cliente			int	 = 0,
@pe_nombrecompleto			varchar(500)	 = '',
@pe_rut			varchar(10)	 = '',
@pe_rut_formateado			varchar(1010)	 = '',
@id_juiciotipo			int	 = 0,
@re_fechasolicitud			varchar(20)	 = null,
@re_fechasolicitud_hasta			varchar(20)	 = null,
@id_persona			int	 = 0,
@re_fechafin			varchar(20)	 = null,
@re_fechafin_hasta			varchar(20)	 = null,
@re_fechafinreal			varchar(20)	 = null,
@re_fechafinreal_hasta			varchar(20)	 = null,
@id_remateetapa			int	 = 0,
@re_remateetapa			varchar(200)	 = '',
@id_rematealarma			int	 = 0,
@ra_rematealarma			varchar(50)	 = '',
@id_rematepago			int	 = 0,
@rp_rematepago			varchar(50)	 = '',
@re_usuario			varchar(50)	 = '',
@re_ultimocomentario			varchar(200)	 = '',
@id_remateDetalle			int	 = 0,
@id_remateprorroga			int	 = 0,
@rp_remateprorroga			varchar(500)	 = '',
@rdp_sbif					int = 0,
@rdp_direcccion varchar(500)	 = '',
@rp_monto					money	 = 0,
@dias						int=0,
@id_controlador				int = 0,
@id_personaConectada		int = 0,
@id_propiedadcomite			int = 0,
@id_propiedadrj				int = 0,
@rdp_direccion				varchar(500) = ''
/*
Creador por         : NTBK-EROCO
Fecha Creación      : 14/08/2019 13:26:19
Procedimiento       : dbo.pa08_wf_remates_no
Modificado por      : 
Fecha Modificación  : 
llamada de test     : 
                      dbo.pa08_wf_remates_no @Pagina, @RegistrosPorPaginas, @criterio, @ColumnaOrden, @OrdenColumna@id_remate,@re_remate,@id_juicio,@ju_rol,@id_tribunal,@tr_tribunal,@id_cliente,@pe_nombrecompleto,@pe_rut,@pe_rut_formateado,@id_juiciotipo,@re_fechasolicitud,@id_persona,@re_fechafin,@re_fechafinreal,@id_remateetapa,@re_remateetapa,@id_rematealarma,@ra_rematealarma,@id_rematepago,@rp_rematepago,@re_usuario,@re_ultimocomentario,@id_remateDetalle,@id_remateprorroga,@rp_remateprorroga

                      dbo.pa08_wf_remates_no 1, 15, 1, '', 00, '', 0, '', 0, '', 0, '', '', '', 0, null, 0, null, null, 0, '', 0, '', 0, '', '', '', 0, 0, ''


Resultado           : Procedimiento de tipo nominas, devuelve selecciones ordenadas de acuerdo al criterio
*/

as


-----------------------------------------------------------------------------
--                       Declaración de variables                            
-----------------------------------------------------------------------------
Declare @Maximo              int
Declare @Minimo              int
Declare @paginas             int
Declare @totregs             int 
Declare @ordenamiento		varchar(4000)
declare @ordenascdesc		varchar(4)

Select @Maximo = (@Pagina * @RegistrosPorPaginas)
Select @Minimo = @Maximo - (@RegistrosPorPaginas - 1)
set nocount on

set rowcount @Maximo
-----------------------------------------------------------------------------
--                       Asignación  de variables                            
-----------------------------------------------------------------------------
declare @sqlstr as varchar(8000)
declare @sqlfrom as varchar(8000)
declare @strfiltro as varchar(8000)


declare @str_dias varchar(2000)
--set @str_dias = '
--(case wf_remates.re_fechafin when null then
--	dbo.wf_rematesetapas.re_diasplazo - datediff(day,dbo.wf_rematesdetalles.rd_fechaetapa,getdate())
--else
--	dbo.wf_rematesetapas.re_diasplazo - datediff(day,dbo.wf_remates.re_fechafin,getdate())
--end)
--'

set @str_dias = '
(case when wf_remates.re_fechafin is null then
	case when (dbo.wf_rematesetapas.re_diasplazo - datediff(day,dbo.wf_rematesdetalles.rd_fechaetapa,getdate())) <= 0 then 0 
	else (dbo.wf_rematesetapas.re_diasplazo - datediff(day,dbo.wf_rematesdetalles.rd_fechaetapa,getdate())) end
else
	case when (datediff(day,dbo.wf_remates.re_fechafin,getdate()))  <= 0 then 0 
	else datediff(day,dbo.wf_remates.re_fechafin,getdate()) end
end)
'
declare @str_rdp_sbif varchar(2000)
set @str_rdp_sbif = '
(
select top 1 rdp_sbif 
from wf_rematesdetallespropiedadesrj
inner join wf_rematesdetalles on wf_rematesdetalles.id_rematedetalle = wf_rematesdetallespropiedadesrj.id_rematedetalle
where wf_rematesdetalles.id_remate = wf_Remates.id_remate
order by id_rematesdetallespropiedades desc
)
'

declare @str_rp_remateprorroga varchar(2000)
set @str_rp_remateprorroga = '
isnull((SELECT top 1 case when len(rp_remateprorroga) > 0 then rp_remateprorroga else ''Sin Comentario'' end
FROM wf_rematesprorrogas RP WHERE RP.ID_REMATE = wf_remates.ID_REMATE order by id_remateprorroga desc),'''')
'


declare @str_rp_monto varchar(2000)
set @str_rp_monto = '
(SELECT ISNULL(SUM(RP_MONTO),0) FROM WF_REMATESPAGOS RP WHERE RP.ID_REMATE = wf_remates.ID_REMATE)
'
declare @str_id_propiedadrj as varchar(2000)
set @str_id_propiedadrj= '
(
select top 1 id_propiedadrj 
from wf_rematesdetallespropiedadesrj
inner join wf_rematesdetalles on wf_rematesdetalles.id_rematedetalle = wf_rematesdetallespropiedadesrj.id_rematedetalle
where wf_rematesdetalles.id_remate = wf_Remates.id_remate
order by id_rematesdetallespropiedades desc
)
'

declare @str_id_propiedadcomite as varchar(2000)
set @str_id_propiedadcomite = '
(
select top 1 id_propiedadcomite 
from wf_rematesdetallespropiedadesrj
inner join wf_rematesdetalles on wf_rematesdetalles.id_rematedetalle = wf_rematesdetallespropiedadesrj.id_rematedetalle
where wf_rematesdetalles.id_remate = wf_Remates.id_remate
order by id_rematesdetallespropiedades desc
)
'

declare @str_rdp_direccion as varchar(2000)
set @str_rdp_direccion = '
(
select top 1 rdp_direccion
from wf_rematesdetallespropiedadesrj
inner join wf_rematesdetalles on wf_rematesdetalles.id_rematedetalle = wf_rematesdetallespropiedadesrj.id_rematedetalle
where wf_rematesdetalles.id_remate = wf_Remates.id_remate
order by id_rematesdetallespropiedades desc
)
'

declare @tabletemp TABLE (variable varchar(2000), prefijo varchar(2000))
insert into @tabletemp (prefijo,variable) values ('dbo.wf_remates.id_remate','id_remate')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_remates.re_remate','re_remate')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_remates.id_juicio','id_juicio')
insert into @tabletemp (prefijo,variable) values ('dbo.ju_juicios.ju_rol','ju_rol')
insert into @tabletemp (prefijo,variable) values ('dbo.ju_juicios.id_tribunal','id_tribunal')
insert into @tabletemp (prefijo,variable) values ('dbo.ju_tribunales.tr_tribunal','tr_tribunal')
insert into @tabletemp (prefijo,variable) values ('dbo.ju_juicios.id_cliente','id_cliente')
insert into @tabletemp (prefijo,variable) values ('dbo.tg_personas.pe_nombrecompleto','pe_nombrecompleto')
--insert into @tabletemp (prefijo,variable) values ('dbo.tg_personas.pe_rut','pe_rut')
insert into @tabletemp (prefijo,variable) values ('cast(replace(dbo.tg_personas.pe_rut,''k'',0) as int)','pe_rut')
insert into @tabletemp (prefijo,variable) values ('dbo.tg_personas.pe_rut_formateado','pe_rut_formateado')
insert into @tabletemp (prefijo,variable) values ('dbo.ju_juicios.id_juiciotipo','id_juiciotipo')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_remates.re_fechasolicitud','re_fechasolicitud')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_remates.id_persona','id_persona')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_remates.re_fechafin','re_fechafin')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_remates.re_fechafinreal','re_fechafinreal')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_remates.id_remateetapa','id_remateetapa')
insert into @tabletemp (prefijo,variable) values ('ltrim(rtrim(dbo.wf_rematesetapas.re_remateetapa))','re_remateetapa')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_remates.id_rematealarma','id_rematealarma')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesalarmas.ra_rematealarma','ra_rematealarma')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematespagos.id_rematepago','id_rematepago')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematespagos.rp_rematepago','rp_rematepago')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_remates.re_usuario','re_usuario')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_remates.re_ultimocomentario','re_ultimocomentario')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_remates.id_remateDetalle','id_remateDetalle')
insert into @tabletemp (prefijo,variable) values ('dbo.wf_rematesprorrogas.id_remateprorroga','id_remateprorroga')
insert into @tabletemp (prefijo,variable) values (''+ @str_rp_remateprorroga +'','rp_remateprorroga')
insert into @tabletemp (prefijo,variable) values (''+ @str_dias +'','dias')
insert into @tabletemp (prefijo,variable) values (''+ @str_rdp_sbif +'','rdp_sbif')
insert into @tabletemp (prefijo,variable) values (''+ @str_rp_remateprorroga +'','rp_remateprorroga')
insert into @tabletemp (prefijo,variable) values (''+ @str_rp_monto +'','rp_monto')
insert into @tabletemp (prefijo,variable) values (''+ @str_rdp_direccion +'','rdp_direccion')

select @ordenascdesc = 'ASC'
if @OrdenColumna = 1 select @ordenascdesc = 'DESC'
select @ordenamiento = ' Row_Number() Over (Order by '+ prefijo + ' ' + @ordenascdesc +' ) as Norden '  
from @tabletemp where variable = @ColumnaOrden 

Select @sqlstr = ' Select top ' + cast(@Maximo as varchar) + ' 
dbo.wf_remates.id_remate, 
dbo.wf_remates.re_remate, 
dbo.wf_remates.id_juicio, 
dbo.ju_juicios.ju_rol, 
dbo.ju_juicios.id_tribunal, 
dbo.ju_tribunales.tr_tribunal, 
dbo.ju_juicios.id_cliente, 
dbo.tg_personas.pe_nombrecompleto, 
dbo.tg_personas.pe_rut, 
dbo.tg_personas.pe_rut_formateado, 
dbo.ju_juicios.id_juiciotipo, 
dbo.wf_remates.re_fechasolicitud, 
dbo.wf_remates.id_persona, 
dbo.wf_remates.re_fechafin, 
dbo.wf_remates.re_fechafinreal, 
dbo.wf_remates.id_remateetapa, 
dbo.wf_rematesetapas.re_remateetapa, 
dbo.wf_remates.id_rematealarma, 
dbo.wf_rematesalarmas.ra_rematealarma, 
dbo.wf_remates.re_usuario, 
dbo.wf_remates.re_ultimocomentario, 
dbo.wf_remates.id_remateDetalle, 
0 as id_remateprorroga, 
' + @str_rp_remateprorroga + ' rp_remateprorroga,
0 id_rematepago, 
'''' rp_rematepago, 
'+ @str_rp_monto +' rp_monto,
'+ @str_rdp_sbif+' as rdp_sbif,
'+ @str_id_propiedadcomite +' as id_propiedadcomite,
'+ @str_id_propiedadrj +' as id_propiedadrj,
'+ @str_rdp_direccion +' as rdp_direccion,
' + @str_dias +' dias
, ' + @ordenamiento

select @sqlfrom = 
'
From [dbo].[wf_remates]
Left Outer Join [dbo].[ju_juicios] On [dbo].[wf_remates].[id_juicio] = [dbo].[ju_juicios].[id_juicio]
Left Outer Join [dbo].[wf_rematesalarmas] On [dbo].[wf_remates].[id_rematealarma] = [dbo].[wf_rematesalarmas].[id_rematealarma]
Left Outer Join [dbo].[ju_tribunales] On [dbo].[ju_tribunales].[id_tribunal] = [dbo].[ju_juicios].[id_tribunal]
Left Outer Join [dbo].[tg_personas] On [dbo].[tg_personas].[id_persona] = [dbo].[ju_juicios].[id_cliente]
Left Outer Join [dbo].[wf_rematesetapas] On [dbo].[wf_rematesetapas].[id_remateetapa] = [dbo].[wf_remates].[id_remateetapa]
Left Outer Join [dbo].[wf_rematesdetalles] On [dbo].[wf_rematesdetalles].[id_remateDetalle] = [dbo].[wf_remates].[id_remateDetalle]
'

select @strfiltro=''
select @strfiltro='WHERE 1=1 '
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.id_remate','int', @id_remate,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.re_remate','str', @re_remate,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.id_juicio','int', @id_juicio,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.ju_juicios.ju_rol','str', @ju_rol,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.ju_juicios.id_tribunal','int', @id_tribunal,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.ju_tribunales.tr_tribunal','str', @tr_tribunal,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.ju_juicios.id_cliente','int', @id_cliente,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_personas.pe_nombrecompleto','str', @pe_nombrecompleto,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_personas.pe_rut_formateado','str', @pe_rut_formateado,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.ju_juicios.id_juiciotipo','int', @id_juiciotipo,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.re_fechasolicitud','date', @re_fechasolicitud,@re_fechasolicitud_hasta)
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.id_persona','int', @id_persona,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.re_fechafin','date', @re_fechafin,@re_fechafin_hasta)
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.re_fechafinreal','date', @re_fechafinreal,@re_fechafinreal_hasta)
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.id_remateetapa','int', @id_remateetapa,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesetapas.re_remateetapa','str', @re_remateetapa,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.id_rematealarma','int', @id_rematealarma,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematesalarmas.ra_rematealarma','str', @ra_rematealarma,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.id_rematepago','int', @id_rematepago,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_rematespagos.rp_rematepago','str', @rp_rematepago,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.re_usuario','str', @re_usuario,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.re_ultimocomentario','str', @re_ultimocomentario,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.id_remateDetalle','int', @id_remateDetalle,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.wf_remates.id_remateprorroga','int', @id_remateprorroga,'')

set @pe_rut = replace(@pe_rut,'.','')
set @pe_rut = replace(@pe_rut,'-','')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_personas.pe_rut','str', @pe_rut,'')

if not @dias  = ''
begin
        if @dias  = -1 set @dias  = 0
        select @strfiltro = @strfiltro + ' and '+ @str_dias +' = ' + cast(@dias as varchar(20))  
end

if not @rp_monto  = ''
begin
        if @rp_monto  = -1 set @rp_monto  = 0
        select @strfiltro = @strfiltro + ' and '+ @str_rp_monto +' = ' + cast(@rp_monto as varchar(20))  
end

if not @rdp_sbif  = ''
begin

        select @strfiltro = @strfiltro + ' and '+ @str_rdp_sbif +' like ''%' + cast(@rdp_sbif as varchar(20))   + '%'''
end

if not @rdp_direccion  = ''
begin

        select @strfiltro = @strfiltro + ' and '+ @str_rdp_direccion +' like ''%' + cast(@rdp_direccion as varchar(20))   + '%'''
end

if not @rp_remateprorroga  = ''
begin

        select @strfiltro = @strfiltro + ' and '+ @str_rp_remateprorroga +' like ''%' + cast(@rp_remateprorroga as varchar(20))   + '%'''
end


Declare @str_controlador as varchar(500) = ''
Declare @str_Staff as varchar(500) = ''
declare @str_Responsable as varchar(500) = ''
declare @str_abogado as varchar(500) = ''
--CONTROLADOR
IF @id_controlador != 0 
	SELECT @str_controlador += ' 
		select id_remate from wf_remates
		inner join ju_juicios on ju_juicios.id_juicio = wf_remates.id_juicio
		where 1=1
		and ju_juicios.id_controlador = ' + cast(@id_controlador as varchar) + '
	'

--STAFF
IF @id_personaConectada != 0 BEGIN
	if @str_controlador != '' SELECT @str_Staff += ' UNION '
	SELECT @str_Staff += ' 
		select wf_remates.id_remate from wf_remates
		inner join ju_juicios on ju_juicios.id_juicio = wf_remates.id_juicio
		where 1=1
		and ju_juicios.id_abogado in (
			SELECT tg_abogados.id_persona 
			FROM tg_abogadosPersonas
			INNER JOIN tg_abogados ON tg_abogadosPersonas.id_abogado = tg_abogados.id_abogado 
			and tg_abogadosPersonas.id_persona = '+ cast(@id_personaConectada as varchar) +' 
		)
	'
END

--RESPONSABLE
IF @id_personaConectada != 0 begin
	if @str_Staff != '' SELECT @str_Responsable += ' UNION '
	SELECT @str_Responsable += ' 
		select DISTINCT wf_rematesdetalles.id_remate from wf_rematesdetalles
		where 1=1
		and wf_rematesdetalles.id_responsable in (
			'+ cast(@id_personaConectada as varchar) +' 
		)
	'
end

--ABOGADO
IF @id_personaConectada != 0 begin
	if @str_Responsable != '' SELECT @str_abogado += ' UNION '
	SELECT @str_abogado += ' 
		select wf_remates.id_remate from wf_remates
		inner join ju_juicios on ju_juicios.id_juicio = wf_remates.id_juicio
		where 1=1
		and ju_juicios.id_abogado in (
			'+ cast(@id_personaConectada as varchar) +' 
		)
	'
end


if @str_controlador != '' or @str_Staff != '' or @str_Responsable != '' begin
	select @strfiltro += '
		and wf_remates.id_remate in (
			'+ @str_controlador +'
			'+ @str_Staff +'
			'+ @str_Responsable +'		
			'+ @str_abogado +'
		)
	'

end



select @sqlfrom = @sqlfrom + isnull(@strfiltro,'') 
-----------------------------------------------------------------------------
-- Devuelve la cantidad de registros totales
-----------------------------------------------------------------------------
declare @strtot as varchar(8000)
select @strtot = dbo.fn_ut_sqldinamico_totalregistros(@sqlfrom,@RegistrosPorPaginas)
print @strtot
exec(@strtot)

--print @strtot
select @sqlstr = 'Select 
id_remate, 
re_remate, 
id_juicio, 
ju_rol, 
id_tribunal, 
tr_tribunal, 
id_cliente, 
pe_nombrecompleto, 
pe_rut, 
pe_rut_formateado, 
id_juiciotipo, 
re_fechasolicitud, 
id_persona, 
re_fechafin, 
re_fechafinreal, 
id_remateetapa, 
re_remateetapa, 
id_rematealarma, 
ra_rematealarma, 
id_rematepago, 
rp_rematepago, 
re_usuario, 
re_ultimocomentario, 
id_remateDetalle, 
id_remateprorroga, 
rp_remateprorroga, 
rdp_sbif,
rdp_direccion,
rp_monto,
dias,
id_propiedadcomite,
id_propiedadrj,
 Norden 
From ( ' 
+ @sqlstr + isnull(@sqlfrom,'') + ' ) As  Paginacion 
Where 
Norden >= ' + cast(@minimo as varchar(20)) + ' and Norden <= ' + cast(@maximo as varchar(20))

print '------------------'
print @sqlstr
exec(@sqlstr)
set rowcount 0
