IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa_ju_reasignaciones_in]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa_ju_reasignaciones_in]
GO

CREATE PROCEDURE [dbo].[pa_ju_reasignaciones_in]
@re_reasignacion	as varchar(500) = '',
@id_juicio			as int = 0,
@id_abogadooriginal as int = 0,
@id_abogadonuevo	as int = 0,
@re_fecha			as smalldatetime = null,
@re_consuser		as varchar(20) = ''

/*
Creador por			:	ds-program5
Fecha Creación		:	22-11-2007 8:54:17
Procedimiento		:	pa_ju_reasignaciones_in
Modificado por		:	Claudio A. Muñoz Colleman
Fecha Modificación	:	09-02-2009
Obs. Modificación	:	agregue la insersion en la op_reasignaciones y actualize el abogado a las operaciones
llamada de test     :  
						pa_ju_reasignaciones_in 's',34092,107,34,'09-06-2009','cmunoz'
anulartest			:	ROLLBACK(TRAN)
Resultado			:	Procedimiento de inserción de un nuevo registro en la tabla ju_reasignaciones

select count(id_operacion), id_juicio from op_operacionesjuicios group by id_juicio
select * from tg_personas where pe_nombrecompleto like '%molinari%'

select * from ju_juicios where id_juicio = 34092
select * from op_operacionesjuicios where id_juicio = 34092
select * from op_operaciones where id_operacion in (86125,86126)

criterio para actualización de operaciones


	Juicio  --  Ojucios  -- operacion
1)	a			a			a
	b			b			b
2)	a			a			b
	c			c			b

*/

as

set nocount on
declare @id_reasignacion as int
declare @corr       int
declare @NroInicial int , @NroFinal   int
select  @NroFinal   = max(id_operacion), @NroInicial = min(id_operacion) from  op_operacionesjuicios where id_juicio = @id_juicio
set     @corr       = @NroInicial
---------------------------------------------------------------

BEGIN TRAN

	INSERT INTO ju_reasignaciones
		(
		re_reasignación,
		id_juicio,
		id_abogadooriginal,
		id_abogadonuevo,
		re_fecha,
		re_consuser
		)
	Values
		(
		@re_reasignacion,
		@id_juicio,
		@id_abogadooriginal,
		@id_abogadonuevo,
		@re_fecha,
		@re_consuser
		)

	-- actualizo las operaciones involucradas en el juicio
	UPDATE op_operacionesjuicios
	set 
		id_persona =  @id_abogadonuevo
	where id_juicio = @id_juicio

	-- inserto las operaciones reasignadas
	INSERT INTO [dbo].[op_reasignaciones]
		([id_operacion]
		,[id_abogadooriginal]
		,[id_abogadonuevo]
		,[re_fecha]
		,[re_consuser])
	SELECT
		id_operacion,
		@id_abogadooriginal,
		@id_abogadonuevo,
		@re_fecha,
		@re_consuser
	FROM  op_operacionesjuicios 
	WHERE id_juicio = @id_juicio

	--select @corr, @NroFinal
	WHILE ( @corr <= (@NroFinal) )
	BEGIN
		--select id_operacion from  op_operacionesjuicios where id_juicio = @id_juicio and id_operacion = @corr
		if  exists ( select id_operacion from  op_operacionesjuicios where id_juicio = @id_juicio and id_operacion = @corr)
			BEGIN 
				-- ahora chequeo si estas operaciones tienen el mismo abogado, que originalmente tenia el juicio, si es igual, 
				-- lo cambio, de otra manera lo dejo como estaba.
				--select 1 from op_operaciones where id_abogado = @id_abogadooriginal and id_operacion = @corr
				if exists(select 1 from op_operaciones where id_abogado = @id_abogadooriginal and id_operacion = @corr)
					UPDATE op_operaciones
					set 
						id_abogado =  @id_abogadonuevo
					where id_operacion = @corr
			END
	-- ELSE  break 
	---------------------------------------------------------------

		set @corr = @corr + 1
	---------------------------------------------------------------
	END

	/*EROCO 15-01-2017 CUANDO SE REASIGNA EL ABOGADO LOS CONTROLADORES TAMBIEN SE REASIGNAN*/
	EXEC cambio_asignacion @ID = @id_juicio, @proceso = 3, @usuario = @re_consuser, @sp = 'pa_ju_reasignaciones_in', @v_id_abogado = @id_abogadonuevo


	/*EROCO 06/09/2019*/
	--VERIFICAR SI EL JUICIO ES SPEED, SI LO ES DEBO GENERAR UN CORREO AL NUEVO ABOGADO
	DECLARE @es_juicioSpeed					AS INT
	SELECT @es_juicioSpeed = DBO.fn_retorna_si_es_juicio_speed(@id_juicio)

	IF @es_juicioSpeed > 0 BEGIN

		--CORREO AL NUEVO ABOGADO
		DECLARE @CORREO AS VARCHAR(8000) = ''
		DECLARE @MSJ AS VARCHAR(8000) = ''
		DECLARE @ju_rol AS VARCHAR(100) = ''
		DECLARE @tr_tribunal AS VARCHAR(100) = ''
		DECLARE @cl_rut AS VARCHAR(100) = ''
		DECLARE @cl_deudor AS VARCHAR(100) = ''

		SELECT @CORREO = PU_EMAIL FROM TG_PERSONASUBICACIONES WHERE ID_PERSONA = @id_abogadonuevo AND PU_ESDENOTIFICACION = 1

		IF @CORREO != '' BEGIN

			select	@ju_rol = ltrim(rtrim(ju_rol)),
					@tr_tribunal = tr_tribunal,
					@cl_rut = cl_rut,
					@cl_deudor = cl_cliente
			from ju_juicios
			inner join ju_tribunales on ju_tribunales.id_tribunal = ju_juicios.id_tribunal
			where id_juicio = @id_juicio

			SET @MSJ += 'Estimado <br><br>'
			SET @MSJ += 'Se ha generado la siguiente asignación del juicio Speed : <br><br>'
			SET @MSJ += '<b>Juicio :</b> ' + @ju_rol + '<br>'
			SET @MSJ += '<b>Tribunal :</b> ' + @tr_tribunal + '<br>'
			SET @MSJ += '<b>Deudor :</b> ' + @cl_deudor + '<br>'
			SET @MSJ += '<b>Rut :</b> ' + @cl_rut + '<br><br>'
			SET @MSJ += 'Saludos'

			Exec pa_clr_enviar_correo
			@config_Smtp = 'mail.plataformagroup.cl',
			@config_port = '25',
			@config_user = 'plataformajudicialbci@plataformagroup.cl',
			@config_pass = 'pcjbci$0270',
			@config_ssl = '0',
			@param_To = @CORREO,
			@param_Cc = 'eroco@plataformagroup.cl,ygonzalez@plataformagroup.cl,cmunoz@plataformagroup.cl',--'servicioalcliente@plataformagroup.cl',
			@param_From = 'plataformajudicialbci@plataformagroup.cl',
			@param_Subject = 'Speed Cambio Abogado, Reasignación Manual',
			@param_body = @MSJ,
			@param_files = ''
			
		END
	END
-- control de errores
if @@error <> 0 goto on_error 
select @id_reasignacion = ident_current('ju_reasignaciones')

if @@trancount > 0
commit tran
select @id_reasignacion id_reasignacion,''
return 0

ON_ERROR:
If @@TRANCOUNT > 0
ROLLBACK TRAN
select -1, 'Se Produjo un error en la transacción y se abortó'
return -1

Error_Params:
select -2, 'Problemas con los parámetros '
return -2





