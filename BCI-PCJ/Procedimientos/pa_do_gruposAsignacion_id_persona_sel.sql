IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa_do_gruposAsignacion_id_persona_sel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa_do_gruposAsignacion_id_persona_sel]
GO

CREATE PROCEDURE [dbo].[pa_do_gruposAsignacion_id_persona_sel]
@id_segmento as int = 0,
@id_comuna as int = 0,
@id_persona as int = 0,
@id_oficina as int = 0,
@id_operaciontipo as int = 0,
@dtd as money = 0,
@id_marca as int =0,
@ID_PAGARE AS INT = 0

/*
Creador por			: ds-program4
Fecha Creación		: 07/11/2008 9:52:16
Procedimiento		:pa_do_gruposAsignacion_sel

Modificado por		: EROCO
Fecha Modificación	: 30-07-2018 Se añade bifurcacion para cuando la ficha sea de tipo SPeed contenido en su operacion esta traera el grupo de asignacion que tiene grabada la ficha.

Modificado por		:EROCO
Fecha Modificación	: 07-09-2019 Si la ficha se le asigna la comuna santiago refundido la ficha SPEED se desmarca

Modificado por		:
Fecha Modificación	:

llamada de test     :  
pa_do_gruposAsignacion_sel @id_grupoasignacion,@ag_grupoasignacion,@id_segmento,@se_segmento,@ag_mesesperiodo,@ag_mesescosecha
[pa_do_gruposAsignacion_id_persona_sel] 27, 337, 138866, 155, 13,78000000,0
select * from do_gruposasignacion
select * from ub_comunas
pa_do_gruposAsignacion_sel @id_segmento = 30, @id_comuna = 35
select g.id_grupoasignacion						   
from do_gruposasignacion as g inner join
do_comunasgruposasignacion as c on  g.id_grupoasignacion = c.id_grupoasignacion
where g.id_segmento = 28 and 28 = 28 and c.id_comuna = 337 and 2000000 <= 75000000

select * from do_gruposasignacion where id_grupoasignacion in ( 24,235)
select * from do_gruposAsignacion
select * from tg_clientes where id_persona=115814
select * from or_subbancas
select * from do_asignaciones where id_grupoasignacion=251
select * from do_tiposgruposAsignacion
Resultado: Procedimiento de tipo nominas, devuelve selecciones ordenadas de acuerdo al criterio
*/

as  

declare @id_grupoasignacion as int
declare @id_subbanca as int
declare @grupos table (
    id_grupoasignacion  int
)

set nocount on

declare @par_DTD as money
declare @SegmentoPagareCCEE as int
set @par_DTD = dbo.fn_ut_parametro_constante('DTD_GRUPOSASIGNACION')
select @SegmentoPagareCCEE =  dbo.fn_ut_constantes('SegmentoPagareCCEE') 
select top 1 @id_subbanca = id_subbanca
from tg_clientes where id_persona = @id_persona


/*INICIO  EROCO SPEED 30-07-2018 SE AÑADE NUEVO IF. SI LA FICHA ES UNA FICHA SPEED MOSTRARA EL GRUPO ASIGNACION DE SPEED DE LA FICHA*/
IF @ID_PAGARE > 0 BEGIN


	DECLARE @CUENTA_OP_SPEED INT = 0
	select @CUENTA_OP_SPEED = dbo.fn_retorna_si_es_ficha_speed(@ID_PAGARE)
	
END


IF @CUENTA_OP_SPEED > 0 and @id_comuna<>338 begin

	select top 1 ID_GRUPOASIGNACION,ag_grupoasignacion, 6 id_tipogrupoasignacion from do_gruposAsignacion 
	where isnull(ag_flgesSpeed,0) = 1

	select 1 id_asignacion

END
/* FIN */

ELSE BEGIN
	if (@id_segmento <> @SegmentoPagareCCEE and @id_comuna<>338 )
	begin

		if exists (select 1
				   from do_gruposasignacion as g inner join
						do_oficinasGruposAsignacion as o on g.id_grupoasignacion = o.id_grupoasignacion
				   where o.id_oficina = @id_oficina and id_tipogrupoasignacion=1 and isnull(ag_estadodesactivado,0)=0 and isnull(ag_flgesSpeed,0) = 0)
		begin           
				   insert into @grupos
				   select g.id_grupoasignacion
				   from do_gruposasignacion as g inner join
						do_oficinasGruposAsignacion as o on g.id_grupoasignacion = o.id_grupoasignacion
				   where o.id_oficina = @id_oficina and id_tipogrupoasignacion=1 and isnull(ag_estadodesactivado,0)=0
		           and isnull(ag_flgesSpeed,0) = 0

				   select id_grupoasignacion,ag_grupoasignacion
				   from do_gruposasignacion 
				   where id_grupoasignacion in (select id_grupoasignacion from @grupos) and isnull(ag_flgesSpeed,0) = 0
				   print 'Oficina'
		end           
		else  
		begin 
			--************* Producto, Comunas   
			--***************** Marcas
			if exists (select 1
					   from do_gruposasignacion as g inner join
							do_marcasGruposAsignacion as o on g.id_grupoasignacion = o.id_grupoasignacion inner join
							do_comunasgruposasignacion as c on  g.id_grupoasignacion = c.id_grupoasignacion
					   where o.id_marca = @id_marca and c.id_comuna = @id_comuna and id_tipogrupoasignacion=7 and isnull(ag_estadodesactivado,0)=0 and isnull(ag_flgesSpeed,0) = 0)
			begin           
					   insert into @grupos
					   select g.id_grupoasignacion
					   from do_gruposasignacion as g inner join
							do_marcasGruposAsignacion as o on g.id_grupoasignacion = o.id_grupoasignacion inner join
							do_comunasgruposasignacion as c on  g.id_grupoasignacion = c.id_grupoasignacion
					   where o.id_marca = @id_marca and c.id_comuna = @id_comuna and id_tipogrupoasignacion=7 and isnull(ag_estadodesactivado,0)=0 and isnull(ag_flgesSpeed,0) = 0
			           
					   select id_grupoasignacion,ag_grupoasignacion
					   from do_gruposasignacion 
					   where id_grupoasignacion in (select id_grupoasignacion from @grupos) and isnull(ag_flgesSpeed,0) = 0
				   				   print 'Marca'
			end 	
			else
			begin      
				if exists (select 1
						   from do_gruposasignacion as g inner join
								do_operacionestiposGruposAsignacion as o on g.id_grupoasignacion = o.id_grupoasignacion inner join
								do_comunasgruposasignacion as c on  g.id_grupoasignacion = c.id_grupoasignacion
						   where o.id_operaciontipo = @id_operaciontipo and c.id_comuna = @id_comuna and id_tipogrupoasignacion=2 and isnull(ag_estadodesactivado,0)=0 and isnull(ag_flgesSpeed,0) = 0)
				begin
						   insert into @grupos
						   select g.id_grupoasignacion
						   from do_gruposasignacion as g inner join
								do_operacionestiposGruposAsignacion as o on g.id_grupoasignacion = o.id_grupoasignacion inner join
								do_comunasgruposasignacion as c on  g.id_grupoasignacion = c.id_grupoasignacion
						   where o.id_operaciontipo = @id_operaciontipo and c.id_comuna = @id_comuna and id_tipogrupoasignacion=2 and isnull(ag_estadodesactivado,0)=0 and isnull(ag_flgesSpeed,0) = 0
			           
						   select id_grupoasignacion,ag_grupoasignacion
						   from do_gruposasignacion 
						   where id_grupoasignacion in (select id_grupoasignacion from @grupos) and isnull(ag_flgesSpeed,0) = 0
				   print 'Producto'					   
						   
				end
				else
				begin --************* Subbanca, Comunas
					if exists (select 1
							   from do_gruposasignacion as g inner join
									do_subbancasGruposAsignacion as o on g.id_grupoasignacion = o.id_grupoasignacion inner join
									do_comunasgruposasignacion as c on  g.id_grupoasignacion = c.id_grupoasignacion 
							   where o.id_subbanca = @id_subbanca and c.id_comuna = @id_comuna and id_tipogrupoasignacion=3 and isnull(ag_estadodesactivado,0)=0 and isnull(ag_flgesSpeed,0) = 0) 
					begin
							   insert into @grupos
							   select g.id_grupoasignacion                   
							   from do_gruposasignacion as g inner join
									do_subbancasGruposAsignacion as o on g.id_grupoasignacion = o.id_grupoasignacion inner join
									do_comunasgruposasignacion as c on  g.id_grupoasignacion = c.id_grupoasignacion
							   where o.id_subbanca = @id_subbanca and c.id_comuna = @id_comuna and id_tipogrupoasignacion=3 and isnull(ag_estadodesactivado,0)=0 and isnull(ag_flgesSpeed,0) = 0
			       
							   select id_grupoasignacion,ag_grupoasignacion
							   from do_gruposasignacion 
							   where id_grupoasignacion in (select id_grupoasignacion from @grupos)
						   				   print 'Subbanca'
			        
					end
					else
					begin --************* DTD, Segmento, Comunas
						if exists (select 1
								   from do_gruposasignacion as g inner join
										do_comunasgruposasignacion as c on  g.id_grupoasignacion = c.id_grupoasignacion
								   where g.id_segmento = @id_segmento and c.id_comuna = @id_comuna and @dtd > @par_DTD and id_tipogrupoasignacion=4 and isnull(ag_estadodesactivado,0)=0)        
						begin
								   insert into @grupos
								   select g.id_grupoasignacion
								   from do_gruposasignacion as g inner join
										do_comunasgruposasignacion as c on  g.id_grupoasignacion = c.id_grupoasignacion
								   where g.id_segmento = @id_segmento and c.id_comuna = @id_comuna and @dtd > @par_DTD and id_tipogrupoasignacion=4 and isnull(ag_estadodesactivado,0)=0
				           
								   select id_grupoasignacion,ag_grupoasignacion
								   from do_gruposasignacion 
								   where id_grupoasignacion in (select id_grupoasignacion from @grupos) and isnull(ag_flgesSpeed,0) = 0
							   				   print 'DTD'
			                            
						end
						else
						begin--************* DTD, Segmento Chip, Comunas -- CNC
							if exists (select 1
									   from do_gruposasignacion as g inner join
											do_comunasgruposasignacion as c on  g.id_grupoasignacion = c.id_grupoasignacion
									   where g.id_segmento = @id_segmento and @id_segmento = 28 and c.id_comuna = @id_comuna and @dtd <= @par_DTD and id_tipogrupoasignacion=5 and isnull(ag_estadodesactivado,0)=0 and isnull(ag_flgesSpeed,0) = 0)       
							begin
									   insert into @grupos
									   select g.id_grupoasignacion						   
									   from do_gruposasignacion as g inner join
											do_comunasgruposasignacion as c on  g.id_grupoasignacion = c.id_grupoasignacion
									   where g.id_segmento = @id_segmento and @id_segmento = 28 and c.id_comuna = @id_comuna and @dtd <= @par_DTD and id_tipogrupoasignacion=5 and isnull(ag_estadodesactivado,0)=0 and isnull(ag_flgesSpeed,0) = 0
					           
									   select id_grupoasignacion,ag_grupoasignacion
									   from do_gruposasignacion 
									   where id_grupoasignacion in (select id_grupoasignacion from @grupos) and isnull(ag_flgesSpeed,0) = 0            
			    				   print 'CNC'
				           
							end   
							else
							begin--************* Generico
								if exists (select 1
										   from do_gruposasignacion as g left outer join
												do_comunasgruposasignacion as c on  g.id_grupoasignacion = c.id_grupoasignacion left outer join
												do_operacionestiposGruposAsignacion as o on g.id_grupoasignacion = o.id_grupoasignacion left outer join
												do_subbancasGruposAsignacion as s on g.id_grupoasignacion = s.id_grupoasignacion left outer join
												do_oficinasGruposAsignacion as ofi on g.id_grupoasignacion = ofi.id_grupoasignacion
										   where (g.id_segmento = @id_segmento or @id_segmento = 0) and 
												 (c.id_comuna = @id_comuna or @id_comuna = 0) and isnull(ag_estadodesactivado,0)=0 and id_tipogrupoasignacion=6 --and 
												 --(o.id_operaciontipo = @id_operaciontipo or @id_operaciontipo = 0) and 							         
												 --(ofi.id_oficina = @id_oficina or @id_oficina = 0) and 
												 --(@dtd > 75000000)
												and isnull(ag_flgesSpeed,0) = 0 
												)        
								begin
										   insert into @grupos
										   select g.id_grupoasignacion		
										   from do_gruposasignacion as g left outer join
												do_comunasgruposasignacion as c on  g.id_grupoasignacion = c.id_grupoasignacion left outer join
												do_operacionestiposGruposAsignacion as o on g.id_grupoasignacion = o.id_grupoasignacion left outer join
												do_subbancasGruposAsignacion as s on g.id_grupoasignacion = s.id_grupoasignacion left outer join
												do_oficinasGruposAsignacion as ofi on g.id_grupoasignacion = ofi.id_grupoasignacion
										   where (g.id_segmento = @id_segmento or @id_segmento = 0) and 
												 (c.id_comuna = @id_comuna or @id_comuna = 0)  and isnull(ag_estadodesactivado,0)=0 and id_tipogrupoasignacion=6 --and 
												 --(o.id_operaciontipo = @id_operaciontipo or @id_operaciontipo = 0) and 							         
												 --(ofi.id_oficina = @id_oficina or @id_oficina = 0) and 
												 --(@dtd > 75000000)
												and isnull(ag_flgesSpeed,0) = 0

										   select id_grupoasignacion,ag_grupoasignacion,id_tipogrupoasignacion
										   from do_gruposasignacion 
										   where id_grupoasignacion in (select id_grupoasignacion from @grupos) and isnull(ag_flgesSpeed,0) = 0       
									   				   print 'Generico'       
					    
					           
								end
								else
									select id_grupoasignacion,ag_grupoasignacion
									from do_gruposasignacion  
									where id_grupoasignacion = 0 and isnull(ag_flgesSpeed,0) = 0
							end 
						end
					end
				end	
			end
		end	
	end		
	else begin
		select id_grupoasignacion,ag_grupoasignacion
		from do_gruposasignacion 
		where id_grupoasignacion = 0
	end

	declare @id_asignacionestado_var as int
	declare @id_grupoAsignacion_var as int
	select @id_asignacionestado_var = dbo.fn_ut_constantes('ASIGNACIONESTADO_VISADO') 

	--SELECT  @id_grupoAsignacion_var = dbo.do_gruposAsignacion.id_grupoAsignacion
	--FROM dbo.do_comunasGruposAsignacion RIGHT OUTER JOIN
	--     dbo.do_gruposAsignacion ON dbo.do_comunasGruposAsignacion.id_grupoAsignacion = dbo.do_gruposAsignacion.id_grupoAsignacion
	--WHERE (dbo.do_gruposAsignacion.id_segmento = @id_segmento) AND (dbo.do_comunasGruposAsignacion.id_comuna = @id_comuna)

	--print cast(@id_grupoAsignacion_var as varchar(2))
	--print cast(@id_asignacionestado_var as varchar(2))
	--select * from do_asignaciones where id_grupoasignacion=69
	--select g.*,ga.ag_grupoasignacion from @grupos g inner join
	--do_gruposasignacion ga on g.id_grupoasignacion=ga.id_grupoasignacion
	SELECT     
		count(isnull(id_asignacion, 0)) id_asignacion
	FROM         
		dbo.do_asignaciones
	WHERE     
		(convert(varchar(10), getdate(), 103) >= convert(varchar(10), as_fechaini, 103)) AND 
		(convert(varchar(10), getdate(), 103) <= convert(varchar(10), as_fechafin, 103)) AND 
		(id_asignacionestado = @id_asignacionestado_var) AND 
		(id_grupoAsignacion in (select id_grupoasignacion from @grupos))
END




		         
-- control de errores
if @@error <> 0 goto on_error 
select 0,''
return 0

ON_ERROR:
select -1, 'Se Produjo un error en la transacción y se abortó'
return -1

Error_Params:
select -2, 'Problemas con los parámetros '
return -2











