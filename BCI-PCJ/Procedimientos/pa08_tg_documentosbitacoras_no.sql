

ALTER PROCEDURE [dbo].[pa08_tg_documentosbitacoras_no]
@Pagina			int = 1,
@RegistrosPorPaginas			int = 15,
@ColumnaOrden			varchar(50) = 'id_documento', 
@OrdenColumna			int = 0, 
@id_documento			int	 = 0,
@id_tabla			int	 = 0,
@ta_tabla			varchar(50)	 = '',
@do_documento			varchar(100)	 = '',
@id_documentotipo			int	 = 0,
@dt_documentotipo			varchar(50)	 = '',
@do_fecha			varchar(20)	 = null,
@do_fecha_hasta			varchar(20)	 = null,
@id_origen			int	 = 0,
@do_archname			varchar(100)	 = '',
@id_tipoarchivo			int	 = 0,
@ta_tipoarchivo			varchar(50)	 = '',
@do_texto			varchar(50)	 = '',
@do_esprivado			int	 = 2,
@id_bitacora			int = 0,
@bi_bitacora			varchar(4000) = '',
@bi_usuario				varchar(20) = '',
@id_persona				int = 0,
@es_cargamasiva			int = 0,
@pe_nombrecompleto		varchar(200) = '',
@pe_rut					varchar(200) = '',
@comentario				varchar(200) = '',
@informacion			varchar(8000) = '',
@id_rematedetalle		int = 0

/*
Creador por         : NTBK-EROCO
Fecha Creación      : 09-03-2017 12:33:42
Procedimiento       : dbo.pa08_tg_documentos_no
Modificado por      : 
Fecha Modificación  : 
llamada de test     : 
                      dbo.pa08_tg_documentos_no @Pagina, @RegistrosPorPaginas, @criterio, @ColumnaOrden, @OrdenColumna@id_documento,@id_tabla,@ta_tabla,@do_documento,@id_documentotipo,@dt_documentotipo,@do_fecha,@id_origen,@do_archname,@id_tipoarchivo,@ta_tipoarchivo,@do_texto,@do_esprivado

                      dbo.pa08_tg_documentos_no 1, 15, 1, '', 00, 0, '', '', 0, '', null, 0, '', 0, '', '', 0

					[pa08_tg_documentosbitacoras_no] @id_persona = 753414, @ColumnaOrden = 'id_bitacora',@OrdenColumna=1
					select * from tg_personas where pe_rut = '176630310'

					-- 5000
					[pa08_tg_documentosbitacoras_no] @id_persona = 285955, @ColumnaOrden = 'id_bitacora',@OrdenColumna=1
					select * from tg_personas where pe_rut = '966033908'

Resultado           : Procedimiento de tipo nominas, devuelve selecciones ordenadas de acuerdo al criterio
*/

as


-----------------------------------------------------------------------------
--                       Declaración de variables                            
-----------------------------------------------------------------------------
Declare @Maximo              int
Declare @Minimo              int
Declare @paginas             int
Declare @totregs             int 
Declare @ordenamiento		varchar(4000)
declare @ordenascdesc		varchar(4)

Select @Maximo = (@Pagina * @RegistrosPorPaginas)
Select @Minimo = @Maximo - (@RegistrosPorPaginas - 1)
set nocount on

set rowcount @Maximo
-----------------------------------------------------------------------------
--                       Asignación  de variables                            
-----------------------------------------------------------------------------
declare @sqlstr as varchar(8000)
declare @sqlfrom as varchar(8000)
declare @strfiltro as varchar(8000)


declare @tabletemp TABLE (variable varchar(30), prefijo varchar(100))
insert into @tabletemp (prefijo,variable) values ('dbo.tg_documentos.id_documento','id_documento')
--insert into @tabletemp (prefijo,variable) values ('dbo.tg_documentos.id_tabla','id_tabla')
--insert into @tabletemp (prefijo,variable) values ('dbo.tg_tablas.ta_tabla','ta_tabla')
insert into @tabletemp (prefijo,variable) values ('dbo.tg_documentos.do_documento','do_documento')
insert into @tabletemp (prefijo,variable) values ('dbo.tg_documentos.id_documentotipo','id_documentotipo')
insert into @tabletemp (prefijo,variable) values ('dbo.tg_documentosTipos.dt_documentotipo','dt_documentotipo')
insert into @tabletemp (prefijo,variable) values ('dbo.bi_bitacoras.bi_msg_fecha','do_fecha')
insert into @tabletemp (prefijo,variable) values ('dbo.tg_documentos.id_origen','id_origen')
insert into @tabletemp (prefijo,variable) values ('dbo.tg_documentos.do_archname','do_archname')
insert into @tabletemp (prefijo,variable) values ('dbo.tg_documentos.id_tipoarchivo','id_tipoarchivo')
insert into @tabletemp (prefijo,variable) values ('dbo.tg_archivostipos.ta_tipoarchivo','ta_tipoarchivo')
insert into @tabletemp (prefijo,variable) values ('dbo.tg_documentos.do_texto','do_texto')
insert into @tabletemp (prefijo,variable) values ('dbo.tg_documentos.do_esprivado','do_esprivado')

insert into @tabletemp (prefijo,variable) values ('dbo.bi_bitacoras.id_bitacora','id_bitacora')
insert into @tabletemp (prefijo,variable) values ('dbo.bi_bitacoras.bi_bitacora','bi_bitacora')
insert into @tabletemp (prefijo,variable) values ('dbo.bi_bitacoras.bi_usuario','bi_usuario')


select @ordenascdesc = 'ASC'
if @OrdenColumna = 1 select @ordenascdesc = 'DESC'
select @ordenamiento = ' Row_Number() Over (Order by '+ prefijo + ' ' + @ordenascdesc +' ) as Norden '  
from @tabletemp where variable = @ColumnaOrden 

Select @sqlstr = ' Select top ' + cast(@Maximo as varchar) + ' 
dbo.tg_documentos.id_documento, 
0 as id_tabla, 
'''' as ta_tabla, 
dbo.tg_documentos.do_documento, 
dbo.tg_documentos.id_documentotipo, 
dbo.tg_documentosTipos.dt_documentotipo, 


dbo.tg_documentos.do_fecha do_fecha2, 
dbo.bi_bitacoras.bi_msg_fecha do_fecha,
dbo.tg_documentos.id_origen, 
dbo.tg_documentos.do_archname, 
dbo.tg_documentos.id_tipoarchivo, 
dbo.tg_archivostipos.ta_tipoarchivo, 
dbo.tg_documentos.do_texto, 
dbo.tg_documentos.do_esprivado,
dbo.tg_documentos.es_cargamasiva,
dbo.tg_documentos.comentario,
dbo.tg_documentos.informacion,
dbo.bi_bitacoras.id_bitacora,
dbo.bi_bitacoras.bi_bitacora,
upper(dbo.bi_bitacoras.bi_usuario) bi_usuario,
dbo.bi_bitacoras.id_persona,
dbo.bi_bitacoras.bi_msg_fecha
, ' + @ordenamiento

/*
select @sqlfrom = 
'From [dbo].[tg_documentos]
Left Outer Join [dbo].[tg_tablas] On [dbo].[tg_documentos].[id_tabla] = [dbo].[tg_tablas].[id_tabla]
Left Outer Join [dbo].[tg_documentosTipos] On [dbo].[tg_documentos].[id_documentotipo] = [dbo].[tg_documentosTipos].[id_documentotipo]
Left Outer Join [dbo].[tg_archivostipos] On [dbo].[tg_documentos].[id_tipoarchivo] = [dbo].[tg_archivostipos].[id_tipoarchivo]
'
*/

select @sqlfrom = 
'
From [dbo].[bi_bitacoras]
LEFT OUTER JOIN [dbo].[tg_documentos] ON [dbo].[tg_documentos].[id_origen] = [dbo].[bi_bitacoras].[id_bitacora]

Left Outer Join [dbo].[tg_documentosTipos] On [dbo].[tg_documentos].[id_documentotipo] = [dbo].[tg_documentosTipos].[id_documentotipo]
Left Outer Join [dbo].[tg_archivostipos] On [dbo].[tg_documentos].[id_tipoarchivo] = [dbo].[tg_archivostipos].[id_tipoarchivo]
'
--Left Outer Join [dbo].[tg_tablas] On [dbo].[tg_documentos].[id_tabla] = [dbo].[tg_tablas].[id_tabla]

select @strfiltro=''
select @strfiltro=' WHERE 1=1 '
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_documentos.id_documento','int', @id_documento,'')
--select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_documentos.id_tabla','int', @id_tabla,'')
--select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_tablas.ta_tabla','str', @ta_tabla,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_documentos.do_documento','str', @do_documento,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_documentos.id_documentotipo','int', @id_documentotipo,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_documentosTipos.dt_documentotipo','str', @dt_documentotipo,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.bi_bitacoras.bi_msg_fecha','date', @do_fecha,@do_fecha_hasta)
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_documentos.id_origen','int', @id_origen,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_documentos.do_archname','str', @do_archname,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_documentos.id_tipoarchivo','int', @id_tipoarchivo,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_archivostipos.ta_tipoarchivo','str', @ta_tipoarchivo,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_documentos.do_texto','str', @do_texto,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.tg_documentos.do_esprivado','bit', @do_esprivado,'')

select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.bi_bitacoras.id_bitacora','int', @id_bitacora,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.bi_bitacoras.bi_bitacora','str', @bi_bitacora,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.bi_bitacoras.id_persona','int', @id_persona,'')
select @strfiltro = dbo.fn_ut_sqldinamico_filtros(@strfiltro,'dbo.bi_bitacoras.bi_usuario','str', @bi_usuario,'')

select @strfiltro = @strfiltro + '
and id_bitacoratipo = 63
'

if @id_rematedetalle = '' begin

	select @strfiltro = @strfiltro + ' and isnull(es_workflow,0) = 0 '

end
else begin

	select @strfiltro = @strfiltro + ' and id_rematedetalle = ' + cast(@id_rematedetalle as varchar)

end


select @strfiltro = @strfiltro + '
and id_bitacora not in (
	select distinct id_bitacora from tg_documentos
	inner join bi_bitacoras on bi_bitacoras.id_bitacora = tg_documentos.id_origen
	where 1=1 and es_cargamasiva = 1 and id_bitacoratipo = 63 and isnull(do_esvigente,0) = 0 ' 

if  isnull(@id_persona,0) <> 0 
	set @strfiltro = @strfiltro + ' and bi_bitacoras.id_persona = ' + cast(@id_persona as varchar(20)) + ') '
else
	set @strfiltro = @strfiltro + ' ) '

/*
EROCO IDEAL SERIA ESTE AND PARA QUE LA CONSULTA NO QUEDE TAN PESADA*//*
select @strfiltro = @strfiltro + '
	and (es_cargamasiva = 0 or (es_cargamasiva = 1 and do_esvigente = 1))
'
*/

select @sqlfrom = @sqlfrom + isnull(@strfiltro,'') 
-----------------------------------------------------------------------------
-- Devuelve la cantidad de registros totales
-----------------------------------------------------------------------------
declare @strtot as varchar(8000)
select @strtot = dbo.fn_ut_sqldinamico_totalregistros(@sqlfrom,@RegistrosPorPaginas)
print @strtot
exec(@strtot)

--print @strtot
select @sqlstr = 'Select 
id_documento, 
id_tabla, 
ta_tabla, 
do_documento, 
id_documentotipo, 
dt_documentotipo, 
do_fecha, 
id_origen, 
do_archname, 
id_tipoarchivo, 
ta_tipoarchivo, 
do_texto, 
do_esprivado, 
id_bitacora,
bi_bitacora,
bi_usuario,
id_persona,
bi_msg_fecha,
es_cargamasiva,
comentario,
informacion,
 Norden 
From ( ' 
+ @sqlstr + isnull(@sqlfrom,'') + ' ) As  Paginacion 
Where 
Norden >= ' + cast(@minimo as varchar(20)) + ' and Norden <= ' + cast(@maximo as varchar(20))
print '------------------------------------------------------------'
print @sqlstr
exec(@sqlstr)
set rowcount 0

GO


