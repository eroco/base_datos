/****** Object:  StoredProcedure [dbo].[pa_bi_bitacoras_automaticas_notifica_WORKFLOW]    Script Date: 23/08/2019 10:39:36 ******/
DROP PROCEDURE [dbo].[pa_bi_bitacoras_automaticas_notifica_WORKFLOW]
GO

/****** Object:  StoredProcedure [dbo].[pa_bi_bitacoras_automaticas_notifica_WORKFLOW]    Script Date: 23/08/2019 10:39:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[pa_bi_bitacoras_automaticas_notifica_WORKFLOW]
@bi_bitacora		as varchar(4000) = '',
@bi_fecha			as smalldatetime = null,
@id_persona			as int = 0,
@bi_esautomatico	as bit = 0,
@bi_msg_user		as varchar(40) = '',
@bi_msg_fecha		as smalldatetime = null,
@id_origen			as int = 0,
@bi_usuario			as varchar(20) = '',
@bi_confidencial	as bit = 0,
@id_bitacorapadre	as int = 0,
@id_bitacoratipo	as int = 0,
@id_remate			as int = 0

as

declare @id_bitacora as int

set nocount on


	INSERT INTO bi_bitacoras
		(
		bi_bitacora,
		bi_fecha,
		id_persona,
		bi_esautomatico,
		bi_msg_user,
		bi_msg_fecha,
		id_origen,
		bi_usuario,
		bi_confidencial,
		id_bitacorapadre,
		id_bitacoratipo
		)
	VALUES
		(
		@bi_bitacora,
		@bi_fecha,
		@id_persona,
		isnull(@bi_esautomatico,0),
		@bi_msg_user,
		@bi_msg_fecha,
		@id_origen,
		@bi_usuario,
		isnull(@bi_confidencial,0),
		@id_bitacorapadre,
		@id_bitacoratipo
		)

	-- control de errores

	select @id_bitacora = ident_current('bi_bitacoras')

	if isnull(@id_bitacora,0) <> 0 
		INSERT INTO [bi_notificados]
			(
			[id_bitacora],
			[no_usuario],
			[id_perfil]
			)
		SELECT     
			@id_bitacora, 
			ca_usuarios.us_consuser,
			0
		FROM ju_participantesjuicios (nolock) inner join ca_usuarios (nolock) on ju_participantesjuicios.id_persona = ca_usuarios.id_persona
		WHERE ju_participantesjuicios.id_juicio = @id_origen
			AND ca_usuarios.us_esvigente=1

INSERT INTO bi_bitacoras
		(
		bi_bitacora,
		bi_fecha,
		id_persona,
		bi_esautomatico,
		bi_msg_user,
		bi_msg_fecha,
		id_origen,
		bi_usuario,
		bi_confidencial,
		id_bitacorapadre,
		id_bitacoratipo
		)
	VALUES
		(
		@bi_bitacora,
		@bi_fecha,
		@id_persona,
		isnull(@bi_esautomatico,0),
		@bi_msg_user,
		@bi_msg_fecha,
		@id_remate,
		@bi_usuario,
		isnull(@bi_confidencial,0),
		@id_bitacorapadre,
		77
		)
GO


