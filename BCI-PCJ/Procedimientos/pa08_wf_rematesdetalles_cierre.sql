USE [PJBCI_INT]
GO
/****** Object:  StoredProcedure [dbo].[pa08_wf_rematesdetalles_cierre]    Script Date: 11/09/2019 12:57:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[pa08_wf_rematesdetalles_cierre]
@id_rematedetalle		as int = 0,
@rd_comentario			as varchar(8000) = '',
@rd_fechafinetapareal	as varchar(20) = '',
@id_remateresultado		as int = 0,
@rd_esetapafinal		as int = 0,
@id_remateetapa			as int = 0,
@id_remateopcion		as int = 0,
@id_personaGenerador	as int = 0,
@id_personaCierre		as int = 0,
@us_consuser			as varchar(500) = ''
/*
Creador por : NTBK-EROCO
Fecha Creación: 16/08/2019 12:41:15
Procedimiento:pa08_wf_rematesdetalles_in
Modificado por:
Fecha Modificación
*/

as

set nocount on

declare @id_abogado					as int
declare @id_controlador				as int
declare @es_rechazo					as int
declare @re_orden					as int
declare @id_remate					as int
declare @id_remateetapa_anterior	as int
declare @id_rematedetalle_creado	as int
declare @id_responsable				as int
declare @id_remateopcion_anterior	as int
declare @id_juicio					as int
declare @accion						as varchar(100)	= ''
declare @fechacumplimiento			AS smalldatetime
declare @plazo						AS int
declare @sin_abogado				as int
declare @sin_controlador			as int

set		@sin_abogado		= dbo.fn_ut_constantes('ID_PERSONA_SINABOGADO')
set		@sin_controlador	= dbo.fn_ut_constantes('ID_PERSONA_SININFORMACION')


select @id_abogado = id_abogado, @id_controlador = id_controlador from ju_juicios where id_juicio = @id_juicio

select @es_rechazo = isnull(rr_esrechazo,0) from wf_rematesresultados where id_remateresultado = @id_remateresultado

select  @re_orden = re_orden,
		@id_remate = wf_rematesdetalles.id_remate,
		@id_juicio = id_juicio,
		@id_responsable = case when wf_rematesetapas.id_responsable = @sin_abogado then @id_abogado else case when  wf_rematesetapas.id_responsable = @sin_controlador then @id_controlador else wf_rematesetapas.id_responsable end end --wf_rematesdetalles.id_responsable
from wf_rematesdetalles
inner join wf_rematesetapas on wf_rematesetapas.id_remateetapa = wf_rematesdetalles.id_remateetapa
inner join wf_remates on wf_remates.id_remate = wf_rematesdetalles.id_remate
where wf_rematesdetalles.id_rematedetalle = @id_rematedetalle



PRINT 'SI ES RECHAZO SE DEBE ABRIR ETAPA ANTERIOR'
IF @es_rechazo = 1 BEGIN

	PRINT 'RECHAZO ETAPA Y DEBO CREAR ETAPA ANTERIOR, PERO DEBO VERIFICAR SI EXISTE ETAPA ANTERIOR atravez del orden.'

	select top 1 @id_remateetapa_anterior = wf_rematesdetalles.id_remateetapa,
				 @id_remateopcion_anterior = wf_rematesdetalles.id_remateopcion,
				 @id_responsable = case when wf_rematesetapas.id_responsable = @sin_abogado then @id_abogado else case when  wf_rematesetapas.id_responsable = @sin_controlador then @id_controlador else wf_rematesetapas.id_responsable end end,
				 @plazo = re_diasplazo
	from wf_rematesdetalles
	inner join wf_rematesetapas on wf_rematesetapas.id_remateetapa = wf_rematesdetalles.id_remateetapa
	where re_orden < @re_orden and id_remate = @id_remate 
	order by re_orden desc, id_rematedetalle desc

	
	if isnull(@id_remateetapa_anterior,0) != 0	begin
		
		set @accion = 'Retroceso';

		PRINT 'cierro avance'
		update wf_rematesdetalles set rd_fechafinetapa = @rd_fechafinetapareal,
									  rd_fechafinetapareal = getdate(),
									  rd_comentario = @rd_comentario,
									  id_personacierre = @id_personaCierre,
									  id_remateresultado = @id_remateresultado
		where id_rematedetalle = @id_rematedetalle

		PRINT 'retrocedo con el avance'
		insert into wf_rematesdetalles(
			id_remateetapa,
			id_responsable,
			id_remate,
			rd_fechaetapaingreso,
			rd_fechaetapa,
			id_remateresultado,
			id_personagenerador,
			rd_comentario,
			id_remateopcion,
			rd_fechacumplimiento
		)
		values(
			@id_remateetapa_anterior,
			@id_responsable, --> aca cambiar responsable
			@id_remate,
			@rd_fechafinetapareal,
			getdate(),
			1, --PENDIENTE
			@id_personaGenerador,
			'PENDIENTE',
			@id_remateopcion_anterior,
			dateadd(d,@plazo, getdate())
		)

		select top 1 @id_rematedetalle_creado = id_rematedetalle 
		from wf_rematesdetalles 
		where id_remate = @id_remate order by id_rematedetalle desc;

		update wf_remates set	id_rematedetalle = @id_rematedetalle_creado,
								id_remateetapa = @id_remateetapa_anterior,
								re_ultimocomentario = substring(@rd_comentario,0,199)
		where id_remate = @id_remate;
	end
	else begin
		PRINT 'NO existe retroceso asi que cierro el workflow'

			set @accion = 'Fin';

			update wf_rematesdetalles set	rd_fechafinetapa = @rd_fechafinetapareal,
											rd_fechafinetapareal = GETDATE(),
											rd_comentario = substring(@rd_comentario,0,7999),
											id_personacierre = @id_personaCierre,
											id_remateresultado = @id_remateresultado
			where id_rematedetalle = @id_rematedetalle

			update wf_remates set	re_ultimocomentario = substring(@rd_comentario,0,199),
									re_fechafin = getdate()
			where id_remate = @id_remate;
	end
	
END
ELSE BEGIN

	PRINT 'NO ES RECHAZO ASI QUE VERIFICO SI ES ETAPA FINAL'
	IF @rd_esetapafinal = 1 BEGIN
		PRINT 'ES LA ETAPA FINAL ASI QUE DEBO CERRAR EL WORKFLOW ¿EL COMENTARIO DONDE LO GUARDO? el comentario quedaria en el avance que esta cerrando.'

		set @accion = 'Fin';	

		update wf_rematesdetalles set	rd_fechafinetapa = @rd_fechafinetapareal,
										rd_fechafinetapareal = GETDATE(),
										rd_comentario = substring(@rd_comentario,0,7999),
										id_personacierre = @id_personaCierre,
										id_remateresultado = @id_remateresultado
		where id_rematedetalle = @id_rematedetalle

		update wf_remates set	re_ultimocomentario = substring(@rd_comentario,0,199),
								re_fechafin = getdate()
		where id_remate = @id_remate;

	END
	ELSE BEGIN

		set @accion = 'Avance';

		PRINT 'ES UN AVANCE ETAPA ASI QUE DEBO CREAR UN AVANCE PARA EL WORKFLOW'

		PRINT 'cierro avance'
		update wf_rematesdetalles set	rd_fechafinetapa = @rd_fechafinetapareal,
										rd_fechafinetapareal = getdate(),
										rd_comentario = substring(@rd_comentario,0,7999),
										id_personacierre = @id_personaCierre,
										id_remateresultado = @id_remateresultado
		where id_rematedetalle = @id_rematedetalle

		select top 1	@id_responsable = case when wf_rematesetapas.id_responsable = @sin_abogado then @id_abogado else case when  wf_rematesetapas.id_responsable = @sin_controlador then @id_controlador else wf_rematesetapas.id_responsable end end, --wf_rematesetapas.id_responsable,
						@plazo = re_diasplazo	
		from wf_rematesetapas
		where  id_remateetapa = @id_remateetapa
		order by re_orden desc

		PRINT 'creo nuevo avance'
		insert into wf_rematesdetalles(
			id_remateetapa,
			id_responsable,
			id_remate,
			rd_fechaetapaingreso,
			rd_fechaetapa,
			id_remateresultado,
			id_personagenerador,
			rd_comentario,
			id_remateopcion,
			rd_fechacumplimiento
		)
		values(
			@id_remateetapa,
			@id_responsable, --> aca cambiar responsable
			@id_remate,
			@rd_fechafinetapareal,
			getdate(),
			1, --PENDIENTE
			@id_personaGenerador,
			'PENDIENTE',
			CASE @id_remateopcion WHEN 0 THEN NULL ELSE @id_remateopcion END,
			dateadd(d,@plazo, getdate())
		)

		select top 1 @id_rematedetalle_creado = id_rematedetalle 
		from wf_rematesdetalles 
		where id_remate = @id_remate order by id_rematedetalle desc;

		update wf_remates set	id_rematedetalle = @id_rematedetalle_creado,
								id_remateetapa = @id_remateetapa,
								re_ultimocomentario = substring(@rd_comentario,0,199)
		where id_remate = @id_remate;


	END
END


/*INSERTO BITACORA AL JUICIO Y AL REMATE PARA LOS COMENTARIOS*/
DECLARE @FECHAHOY									AS SMALLDATETIME
DECLARE @cuerpo_bitacora							AS VARCHAR(1000)	= ''
DECLARE @param_fecha								AS VARCHAR(100)		

set @param_fecha = (select replace(convert(varchar, getdate(), 103),'/','-')); --dd-mm-yyyy
set @FECHAHOY = CAST(GETDATE() AS SMALLDATETIME)

/*SE GENERA BITACORA AL JUICIO*/
SET @cuerpo_bitacora = 'WorkFlow ' + @accion + ' : ' + substring(@rd_comentario,0,7900)

--BITACORA AL JUICIO Y SUS PARTICIPANTES
EXEC [dbo].[pa_bi_bitacoras_automaticas_notifica_participante_juicio]
@bi_bitacora		= @cuerpo_bitacora,
@bi_fecha			= @FECHAHOY,
@id_persona			= @id_personaGenerador, --(por defecto SISTEMA)
@bi_esautomatico	= 1, --(por defecto)
@bi_msg_user		= @us_consuser, --Usuario conectado al sistema
@bi_msg_fecha		= @FECHAHOY,
@id_origen			= @id_juicio,
@bi_usuario			= @us_consuser, --Usuario conectado al sistema
@bi_confidencial	= 0,
@id_bitacorapadre	= null,
@id_bitacoratipo	= 25

--BITACORA NUEVA PARA LOS COMENTARIO DEL WORKFLOW
EXEC [dbo].[pa_bi_bitacoras_automaticas_in]
@bi_bitacora		= @cuerpo_bitacora,
@bi_fecha			= @FECHAHOY,
@id_persona			= @id_personaGenerador, --(por defecto SISTEMA)
@bi_esautomatico	= 1, --(por defecto)
@bi_msg_user		= @us_consuser, --Usuario conectado al sistema
@bi_msg_fecha		= @FECHAHOY,
@id_origen			= @id_remate,
@bi_usuario			= @us_consuser, --Usuario conectado al sistema
@bi_confidencial	= 0,
@id_bitacorapadre	= null,
@id_bitacoratipo	= 77

--CORREO AL NUEVO RESPONSABLE O AL ANTIGUO
DECLARE @CORREO AS VARCHAR(8000) = ''
DECLARE @MSJ AS VARCHAR(8000) = ''
DECLARE @ju_rol AS VARCHAR(100) = ''
DECLARE @tr_tribunal AS VARCHAR(100) = ''
DECLARE @cl_rut AS VARCHAR(100) = ''
DECLARE @cl_deudor AS VARCHAR(100) = ''


SELECT @CORREO = PU_EMAIL FROM TG_PERSONASUBICACIONES 
WHERE ID_PERSONA = @ID_RESPONSABLE 
AND PU_ESDENOTIFICACION = 1

IF @CORREO != '' BEGIN

	select	@ju_rol = ltrim(rtrim(ju_rol)),
			@tr_tribunal = tr_tribunal,
			@cl_rut = cl_rut,
			@cl_deudor = cl_cliente
	from ju_juicios
	inner join ju_tribunales on ju_tribunales.id_tribunal = ju_juicios.id_tribunal
	where id_juicio = @id_juicio

	SET @MSJ += 'Estimado <br><br>'
	SET @MSJ += 'Se ha generado la siguiente actuación en el Workflow de remate del juicio : <br><br>'
	SET @MSJ += '<b>Juicio :</b> ' + @ju_rol + '<br>'
	SET @MSJ += '<b>Tribunal :</b> ' + @tr_tribunal + '<br>'
	SET @MSJ += '<b>Deudor :</b> ' + @cl_deudor + '<br>'
	SET @MSJ += '<b>Rut :</b> ' + @cl_rut + '<br>'
	SET @MSJ += '<b>Actuación :</b> ' + substring(@rd_comentario,0,7700) + ' <br><br>'
	SET @MSJ += 'Saludos'

	Exec pa_clr_enviar_correo
	@config_Smtp = 'mail.plataformagroup.cl',
	@config_port = '25',
	@config_user = 'plataformajudicialbci@plataformagroup.cl',
	@config_pass = 'pcjbci$0270',
	@config_ssl = '0',
	@param_To = @CORREO,
	@param_Cc = 'eroco@plataformagroup.cl,ygonzalez@plataformagroup.cl,cmunoz@plataformagroup.cl',--'servicioalcliente@plataformagroup.cl',
	@param_From = 'plataformajudicialbci@plataformagroup.cl',
	@param_Subject = 'WorkFlow Actuación',
	@param_body = @MSJ,
	@param_files = ''
END

select 'retorno algo'