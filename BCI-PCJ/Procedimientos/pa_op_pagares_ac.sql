USE [PJBCI_INT]
GO
/****** Object:  StoredProcedure [dbo].[pa_op_pagares_ac]    Script Date: 07/09/2019 17:18:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[pa_op_pagares_ac]
@id_pagare as int = 0,
@id_persona as int = 0,
@id_abogado as int = 0,
@id_oficina as int = 0,
@id_pagaretipo as int = NULL,
@pa_codunico as varchar(20) = '',
@pa_codunicocontrato as varchar(20) = '',
@pa_capitalinsoluto as money = 0,
@pa_interesprimervcto as money = 0,
@pa_totalimpago as money = 0,
@pa_plazopactado as int = 0,
@pa_tasaspread as money = 0,
@pa_cuotascanceladas as int = 0,
@pa_cuotasimpagas as int = 0,
@pa_fecharecepcion as smalldatetime = null,
@pa_consuser as varchar(50) = 'CMUNOZ',
@pa_fechauser as smalldatetime = null,
@id_mandatotipo as int = 5,
@pa_montooriginalUF as money = 0,
@pa_montooriginal as money = 0,
@pa_fechacurse as smalldatetime = null,
@pa_fechaprotesto as smalldatetime = null,
@pa_fechaprimervctooriginal as smalldatetime = null,
@pa_fechaprimervctoimp as smalldatetime = null,
@pa_montointereses as decimal = 0,
@pa_fechamandato as smalldatetime = null,
@pa_docsPagare as bit = 0,
@pa_docsMandato as bit = 0,
@pa_docsCondicionesgenerales as bit = 0,
@pa_docsSolicitudCredito as bit = 0,
@pa_docsSolicitudSegCesantia as bit = 0,
@pa_docsCalPago as bit = 0,
@pa_docsArticulo as bit = 0,
@pa_CobExt as bit = 0,
@pa_pmds as bit = 0,
@pa_delegFac as bit = 0,
@pa_mandatoAbogado as bit = 0,
@pa_garantias as varchar(500) = '',
@pa_deudadirecta as money = 0,
@pa_estadoconvenio as varchar(50) = '',
@pa_flgtraspasojudicial as bit = 0,
@pa_estudioasignado as varchar(3) = '',
@pa_ciframillon as int = 0,
@pa_datodeuda as varchar(2) = '',
@pa_tarea as varchar(500) = '',
@pa_fch_fintarea as smalldatetime = null,
@id_moneda as int = 1,
@pa_docsOtros bit = null,
@id_segmento as int = 30,
@pa_docsComplemento as bit = null,
@pa_docsDecJurada as bit = null,
@pa_docsContrato as bit = null,
@pa_consuservisado as varchar(20) = 'jcoloma',
@pa_fechavisado as smalldatetime = null,
@id_comunajurisdiccion as int = 0,
@pa_flgasignacionautom as bit = null,
@pa_flgnoasignar as bit = null,
@id_grupoAsignacion as int = 0, 
@id_motivonoasignacion as int = 0,
@pa_comentario as varchar(500) = '',
@pa_fechaNoAsignacion as smalldatetime = null,
@id_estadoPagare as int = 0,
@pa_numeroDivImpagos as int = 0,
@pa_montoDivImpagos as money = 0,
@pa_chipObservacion	as varchar(500) = '',
@pa_chipHipotecario	as bit = null,
@pa_chipHipotecarioEndosable as bit = null,
@pa_chipHipotecarioUCG as bit = null,
@pa_chipDatosGarantia as bit = null,	
@id_tipoPagare as int = 0,
@id_controlador as int =0,
@fechacompromiso as smalldatetime = null,
@id_organizacion as int = 0
/*
Creador por			: DS-PROGRAM3
Fecha Creación		: 11-07-2008 12:01:39 PM
Procedimiento		:pa_op_pagares_ac
Modificado por		: Eroco
Fecha Modificación	: 15-01-2017 se añade nueva obtencion de controladores desde tabla de tg_asignacioncontroladores
Modificado por		: Eroco
Fecha Modificación	: 11-05-2018 se añade obtencion de perfiles del usuario receptor de la ficha para saber si la ficha es de uapc o no.

llamada de test     :  
begin tran

pa_op_pagares_ac @id_pagare=49582, @id_persona=333150, @id_abogado=325145, @id_oficina=131, @id_pagaretipo=1, @pa_codunico='', @pa_codunicocontrato='', 
@pa_capitalinsoluto=1475.0164, @pa_interesprimervcto=0, @pa_totalimpago=1475.0164, @pa_plazopactado=0, @pa_tasaspread=0, @pa_cuotascanceladas=0, @pa_cuotasimpagas=0, 
@pa_fecharecepcion=0, @pa_consuser='cmunoz', @pa_fechauser='06-01-2010', @id_mandatotipo=0, @pa_montooriginalUF=2151, @pa_montooriginal=2151, @pa_fechacurse='04-11-2005', 
@pa_fechaprotesto=0, @pa_fechaprimervctooriginal='10-01-2006', @pa_fechaprimervctoimp='10-01-2006', @pa_montointereses=0, @pa_fechamandato=0, @pa_docsPagare=True, 
@pa_docsMandato=False, @pa_docsCondicionesgenerales=False, @pa_docsSolicitudCredito=False, @pa_docsSolicitudSegCesantia=False, @pa_docsCalPago=False, 
@pa_docsArticulo=False, @pa_CobExt=False, @pa_pmds=False, @pa_delegFac=False, @pa_mandatoAbogado=False, @pa_garantias='Sin Garantía', @pa_deudadirecta=48869155, 
@pa_estadoconvenio='', @pa_flgtraspasojudicial=0, @pa_estudioasignado='', @pa_ciframillon=0, @pa_datodeuda='', @pa_tarea='', @pa_fch_fintarea=0, @id_moneda=2, 
@pa_docsOtros=False, @id_segmento=27, @pa_docsComplemento=False, @pa_docsDecJurada=False, @pa_docsContrato=False, @pa_consuservisado='cmunoz', @pa_fechavisado='06-01-2010', 
@pa_comentario='', @id_comunajurisdiccion=346, @pa_flgasignacionautom=0, @pa_flgnoasignar=False, @id_grupoAsignacion=0, @id_motivonoasignacion=0, @pa_fechaNoAsignacion=0, 
@id_estadoPagare=2, @pa_numeroDivImpagos=0, @pa_montoDivImpagos=0, @pa_chipObservacion='', @pa_chipHipotecario=True, @pa_chipHipotecarioEndosable=False, 
@pa_chipHipotecarioUCG=False, @pa_chipDatosGarantia=False, @id_tipoPagare=2

select top 200 * from op_pagares order by 1 desc
select * from tg_clientes where id_persona = 311817
select * from tg_personas where id_persona = 311817
update tg_clientes set id_clientetipo = 3 where id_persona = 311817
select * from op_reasignaciones where id_operacion in (select id_operacion from op_operaciones where op_codoperacion='888888888888')

rollback
anulartest : ROLLBACK(TRAN)
Resultado: Procedimiento de inserción de un nuevo registro en la tabla op_pagares
*/

as
if @id_mandatotipo = 0 set @id_mandatotipo = 5 -- sin mandato

declare @pa_consuservisado_ant	varchar(20)
declare @id_abogado_ant			int
declare @datosmail				varchar(2000)
declare @myheader				varchar(8000)
declare @myquery				varchar(8000)
set		@myquery				= ''
declare @pa_fecharecepcion_ant	smalldatetime
declare @pa_consuserasigna		varchar(30)
declare @pa_fechaasigna			smalldatetime
declare @pa_consuserentrega		varchar(30)
declare @pa_fechaentrega		smalldatetime
--declare @id_controlador int
--declare @co_controlador varchar(100)	

set nocount on
begin tran
	
	select	@pa_consuserasigna = pa_consuserasigna, 
			@pa_fechaasigna = pa_fechaasigna, 
			@pa_consuservisado_ant = pa_consuservisado, 
			@id_abogado_ant = id_abogado, 
			@pa_fecharecepcion_ant = pa_fecharecepcion 
	from op_pagares (nolock) 
	where id_pagare = @id_pagare

	if isnull(@id_abogado_ant,11) <> isnull(@id_abogado,11)
		begin
			set @pa_fechauser		= getdate()
			set @pa_consuserasigna	= @pa_consuser
			set	@pa_fechaasigna		= getdate()

			-- inserto las operaciones reasignadas
			INSERT INTO op_reasignaciones
						([id_operacion]
						,[id_abogadooriginal]
						,[id_abogadonuevo]
						,[re_fecha]
						,[re_consuser]
						,re_reasignacion)
			select 
				id_operacion,
				@id_abogado_ant,
				@id_abogado,
				getdate(),
				@pa_consuser,
				'Reasignación manual desde la Ficha de Asignación (Modificación Abogado)'
			from op_operacionespagares
			where id_pagare = @id_pagare
			
			/*correo bienes	*/
			exec pa_tg_cm_bienes_pagares_correo
			@param_id_pagare = @id_pagare,
			@param_fecharecepcion = @pa_fecharecepcion,
			@param_id_abogado = @id_abogado
			/*fin correo bienes*/

			/*EROCO RAC 15-01-2017 CUANDO SE REASIGNA EL ABOGADO LOS CONTROLADORES TAMBIEN SE REASIGNAN*/ 
			--EROCO 02-08-2018 se comenta ya que existe otro proceso que ya esta pegando el abogado
			--EXEC cambio_asignacion @ID = @id_pagare, @proceso = 1, @usuario = @pa_consuser, @sp = 'pa_op_pagares_ac',@v_id_abogado = @id_abogado
		end

	


	/*EROCO 15-01-2017 SE COMENTA YA QUE EL CONTROLADOR LO TOMARA DESDE UNA NUEVA TABLA Y CON OTROS PARAMETROS*/
	/*
	if isnull(@id_grupoasignacion,0)<>0
	begin
		select @id_controlador = id_controlador
		from do_gruposasignacion
		where id_grupoasignacion = @id_grupoasignacion
	end
	*/


	if isnull(@pa_fecharecepcion_ant,'') <> isnull(@pa_fecharecepcion,'')
		begin
			set @pa_consuserentrega	= @pa_consuser
			set	@pa_fechaentrega	= getdate()
		end
	--else
	--		@pa_fechauser = @pa_fechacreacion_ant

	--EROCO 06-02-2018 EL PROYECTO NO ESTA ENVIANDO EL CONTROLADOR LO CUAL ESTA QUEDANDO VACIO
	select @id_controlador = id_controlador from op_pagares where id_pagare = @id_pagare

	if @id_controlador = 0
	   set @id_controlador = null
	   
	Update op_pagares set
			pa_consuserasigna			= @pa_consuserasigna,
			pa_fechaasigna				= @pa_fechaasigna,	
			pa_consuserentrega			= @pa_consuserentrega,
			pa_fechaentrega				= @pa_fechaentrega,	
            id_persona					= @id_persona,
            id_abogado					= @id_abogado,
            id_oficina					= @id_oficina,
            id_pagaretipo				= @id_pagaretipo,
            pa_codunico					= @pa_codunico,
            pa_codunicocontrato			= @pa_codunicocontrato,
            pa_capitalinsoluto			= @pa_capitalinsoluto,
            pa_interesprimervcto		= @pa_interesprimervcto,
            pa_totalimpago				= @pa_totalimpago,
            pa_plazopactado				= @pa_plazopactado,
            pa_tasaspread				= @pa_tasaspread,
            pa_cuotascanceladas			= @pa_cuotascanceladas,
            pa_cuotasimpagas			= @pa_cuotasimpagas,
            pa_fecharecepcion			= @pa_fecharecepcion,
            pa_consuser					= @pa_consuser,
            pa_fechauser				= @pa_fechauser,
            id_mandatotipo				= @id_mandatotipo,
            pa_montooriginalUF			= @pa_montooriginalUF,
            pa_montooriginal			= @pa_montooriginal,
            pa_fechacurse				= @pa_fechacurse,
            pa_fechaprotesto			= @pa_fechaprotesto,
            pa_fechaprimervctooriginal	= @pa_fechaprimervctooriginal,
            pa_fechaprimervctoimp		= @pa_fechaprimervctoimp,
            pa_montointereses			= @pa_montointereses,
            pa_fechamandato				= @pa_fechamandato,
            pa_docsPagare				= @pa_docsPagare,
            pa_docsMandato				= @pa_docsMandato,
            pa_docsCondicionesgenerales = @pa_docsCondicionesgenerales,
            pa_docsSolicitudCredito		= @pa_docsSolicitudCredito,
            pa_docsSolicitudSegCesantia = @pa_docsSolicitudSegCesantia,
            pa_docsCalPago				= @pa_docsCalPago,
            pa_docsArticulo				= @pa_docsArticulo,
            pa_CobExt					= @pa_CobExt,
            pa_pmds						= @pa_pmds,
            pa_delegFac					= @pa_delegFac,
            pa_mandatoAbogado			= @pa_mandatoAbogado,
            pa_garantias				= @pa_garantias,
            pa_deudadirecta				= @pa_deudadirecta,
            pa_estadoconvenio			= @pa_estadoconvenio,
            pa_flgtraspasojudicial		= @pa_flgtraspasojudicial,
            pa_estudioasignado			= @pa_estudioasignado,
            pa_ciframillon				= @pa_ciframillon,
            pa_datodeuda				= @pa_datodeuda,
            pa_tarea					= @pa_tarea,
            pa_fch_fintarea				= @pa_fch_fintarea,
            id_moneda					= @id_moneda,
            pa_docsOtros				= @pa_docsOtros,
            id_segmento					= @id_segmento,
            pa_docsComplemento			= @pa_docsComplemento,
            pa_docsDecJurada			= @pa_docsDecJurada,
            pa_docsContrato				= @pa_docsContrato,
            pa_consuservisado			= @pa_consuservisado,
			--CARIAS 17/01/2011 : LA FECHA DE VISADO SE ELIMINA DE EL SISTEMA, DEBE SIEMPRE SER IGUAL A FECHA ASIGNACION
            --pa_fechavisado			= @pa_fechauser,
			pa_comentario				= @pa_comentario,
			id_comunajurisdiccion		= @id_comunajurisdiccion,
			pa_flgasignacionautom		= @pa_flgasignacionautom,
			pa_flgnoasignar				= @pa_flgnoasignar,
			id_grupoAsignacion			= @id_grupoAsignacion,
			id_motivonoasignacion		= @id_motivonoasignacion,
			pa_fechaNoAsignacion		= @pa_fechaNoAsignacion,
			id_estadoPagare				= @id_estadoPagare,
			pa_numeroDivImpagos			= @pa_numeroDivImpagos,
			pa_montoDivImpagos			= @pa_montoDivImpagos,
			pa_chipObservacion			= @pa_chipObservacion,
			pa_chipHipotecario			= @pa_chipHipotecario,
			pa_chipHipotecarioEndosable = @pa_chipHipotecarioEndosable,
			pa_chipHipotecarioUCG		= @pa_chipHipotecarioUCG,
			pa_chipDatosGarantia		= @pa_chipDatosGarantia,
			id_tipoPagare				= @id_tipoPagare,
			id_controlador				= @id_controlador,
			id_organizacion				=  @id_organizacion
	WHERE id_pagare = @id_pagare
	if @@error <> 0 goto ON_ERROR 

	-- actualiza tipo de cliente en funcion del segmento
	declare  @id_clientetipo int
	select  @id_clientetipo =	case isnull(@id_segmento,0) 
									when 0	then 3
									when 30 then 1
									when 31 then 1 
								else 2 end
	update tg_clientes set id_clientetipo = @id_clientetipo where id_persona = @id_persona --and tg_clientes.id_clientetipo = 3
	if @@error <> 0 goto ON_ERROR 

	--eroco 11-05-2018
	--actualiza ficha si tiene atribución de uapc
	if isnull(@pa_fecharecepcion_ant,'') = '' and isnull(@pa_fecharecepcion,'') <> '' begin
		
		declare @permiso_ACC_APROBAR_DEVOLVER_COBRANZA int

		set @permiso_ACC_APROBAR_DEVOLVER_COBRANZA = (
														SELECT count(1)
														FROM  ca_opciones 
														INNER JOIN ca_perfilesopciones ON ca_perfilesopciones.ID_OPCION = ca_opciones.ID_OPCION
														inner join ca_perfiles on ca_perfiles.id_perfil = ca_perfilesopciones.id_perfil
														inner join ca_usuariossistemas on ca_usuariossistemas.id_perfil = ca_perfiles.id_perfil
														inner join ca_usuarios on ca_usuarios.id_usuario = ca_usuariossistemas.id_usuario
														WHERE	ca_opciones.op_constante = 'ACC_APROBAR_DEVOLVER_COBRANZA' 
														and		lower(ltrim(rtrim(us_consuser))) = lower(ltrim(rtrim(@pa_consuser))) 
														and		pe_esvigente = 1
													 )

		
		if @permiso_ACC_APROBAR_DEVOLVER_COBRANZA > 0 begin

			update op_pagares set pa_esuapc = 1 where id_pagare = @id_pagare
			if @@error <> 0 goto ON_ERROR 

		end
		
	end


	--SI LA FICHA ES SPEED Y LA COMUNA DE JURISDICCION ES SANTIAGO REFUNDIDO. ESTA DESMARCARA LA FICHA
	DECLARE @CUENTA_OP_SPEED INT = 0
	select @CUENTA_OP_SPEED = dbo.fn_retorna_si_es_ficha_speed(@ID_PAGARE)

	IF @id_comunajurisdiccion = 338 AND @CUENTA_OP_SPEED > 0 BEGIN

		update op_operaciones set op_hito = replace(lower(op_hito),'speed',''),op_fechadesmarquespeed = getdate(), op_userdesmarquespeed = @pa_consuser
		where id_operacion in (
		
			select id_operacion from op_operacionespagares where id_pagare = @id_pagare
		)
	END

-- control de errores
if @@error <> 0 goto ON_ERROR 
if @@trancount > 0
COMMIT TRAN
select 0,''
return 0

ON_ERROR:
If @@TRANCOUNT > 0
ROLLBACK TRAN
select -1, 'Se Produjo un error en la transacción y se abortó'
return -1


