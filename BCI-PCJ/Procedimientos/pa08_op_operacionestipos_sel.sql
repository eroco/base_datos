IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pa08_op_operacionestipos_sel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pa08_op_operacionestipos_sel]
GO

CREATE PROCEDURE [dbo].[pa08_op_operacionestipos_sel]
@id_operaciontipo as int = 0
/*
Creador por : DS-GFONTECILLA
Fecha Creación: 22-03-2013 12:27:27
Procedimiento:pa08_op_operacionestipos_sel

Modificado por: EROCO
Fecha Modificación : 05-09-2019 proyecto speed marcas

Modificado por:
Fecha Modificación :

llamada de test     :  
    pa08_op_operacionestipos_sel     pa08_op_operacionestipos_sel Resultado: Procedimiento de tipo nominas, devuelve selecciones ordenadas de acuerdo al criterio
*/

as

-----------------------------------------------------------------------------
-- Declaracion de variables para construccion del select
-----------------------------------------------------------------------------
DECLARE @l_count int 
    -- Get the rowcount first and make sure 
    -- only one row is returned
    SELECT @l_count = count(*) 
    FROM [dbo].[op_operacionestipos]
    WHERE id_operaciontipo = @id_operaciontipo

    IF @l_count = 0
        RAISERROR ('The record no longer exists.', 16, 1)
    IF @l_count > 1
        RAISERROR ('duplicate object instances.', 16, 1)

    -- Get the row from the query.  Checksum value will be
    -- returned along the row data to support concurrency.
Select 
     [dbo].[op_operacionestipos].[id_operaciontipo],
     [dbo].[op_operacionestipos].[op_operaciontipo],
     cast(isnull([dbo].[op_operacionestipos].[op_esaunvencimiento],0) as bit) [op_esaunvencimiento],
     cast(isnull([dbo].[op_operacionestipos].[op_flgEsCHIP],0) as bit) [op_flgEsCHIP],
     [dbo].[op_operacionestipos].[op_codigonova],
     cast(isnull([dbo].[op_operacionestipos].[op_flgEsCONSUMO],0) as bit) [op_flgEsCONSUMO],
	 cast(isnull([dbo].[op_operacionestipos].[op_esggee],0) as bit) [op_esggee],
	 cast(isnull([dbo].[op_operacionestipos].[op_escae],0) as bit) [op_escae], 
	 [dbo].[op_operacionestipos].[ot_plazosasignacionSpeed],
	CAST
	(
		BINARY_CHECKSUM
		(
			[dbo].[op_operacionestipos].[id_operaciontipo],
			[dbo].[op_operacionestipos].[op_operaciontipo],
			[dbo].[op_operacionestipos].[op_esaunvencimiento],
			[dbo].[op_operacionestipos].[op_flgEsCHIP],
			[dbo].[op_operacionestipos].[op_codigonova],
			[dbo].[op_operacionestipos].[op_flgEsCONSUMO],
			[dbo].[op_operacionestipos].[op_esggee],
			[dbo].[op_operacionestipos].[op_escae],
			[dbo].[op_operacionestipos].[ot_plazosasignacionSpeed]
		) AS nvarchar(4000)
	) AS BinaryChecksum 
From  
[dbo].[op_operacionestipos]
    WHERE dbo.op_operacionestipos.id_operaciontipo = @id_operaciontipo
