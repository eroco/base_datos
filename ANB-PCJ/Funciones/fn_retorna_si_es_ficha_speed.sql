
ALTER FUNCTION [dbo].[fn_retorna_si_es_ficha_speed](
    @id_pagare			as int
)
returns INT

/*
@Author : Emanuel Roco
@Fecha : 30/07/2018

@Author : Emanuel Roco
@Fecha Modificación : 22/03/2019
ahora los Speed se identifican por los primeros 5 caracteres.
*/

AS 
BEGIN
	DECLARE @CUENTA_OP_SPEED INT

	SELECT @CUENTA_OP_SPEED = COUNT(*) FROM OP_OPERACIONES
	INNER JOIN OP_OPERACIONESPAGARES ON OP_OPERACIONESPAGARES.ID_OPERACION = OP_OPERACIONES.ID_OPERACION
	WHERE ID_PAGARE = @id_pagare 
	AND [dbo].[fn_retorna_si_es_operacion_speed](op_operaciones.id_operacion) = 1

	RETURN @CUENTA_OP_SPEED

END



