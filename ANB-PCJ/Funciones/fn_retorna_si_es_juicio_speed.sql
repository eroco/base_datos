
ALTER FUNCTION [dbo].[fn_retorna_si_es_juicio_speed](
    @id_juicio			as int
)
returns INT

/*
@Author : Emanuel Roco
@Fecha : 30/07/2018
*/

AS 
BEGIN
	DECLARE @CUENTA_OP_SPEED INT

	SELECT @CUENTA_OP_SPEED = COUNT(*) FROM ju_juicios
	INNER JOIN op_operacionesjuicios ON op_operacionesjuicios.id_juicio = ju_juicios.id_juicio
	INNER JOIN op_operaciones on op_operaciones.id_operacion = op_operacionesjuicios.id_operacion
	WHERE ju_juicios.id_juicio = @id_juicio 
	AND [dbo].[fn_retorna_si_es_operacion_speed](op_operaciones.id_operacion) = 1

	RETURN @CUENTA_OP_SPEED

END



