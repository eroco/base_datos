/****** Object:  StoredProcedure [dbo].[pa_ufs_ac_tabla]    Script Date: 28-12-2018 15:42:51 ******/
DROP PROCEDURE [dbo].[pa_ufs_ac_tabla]
GO

/****** Object:  StoredProcedure [dbo].[pa_ufs_ac_tabla]    Script Date: 28-12-2018 15:42:51 ******/
CREATE    procedure [dbo].[pa_ufs_ac_tabla]
    @fecha     smalldatetime,
    @valor     decimal(18,2) = 0,
    @id_moneda int = 0
    /*
    Creado por      :  Claudio A. Mu�oz Colleman
    Fecha Creaci�n  :  17-10-2004
    Procedimiento   :  pa_ufs_ac_tabla
    Pagina ASP asoc.:  .asp
    llamada de test :  pa_ufs_ac_tabla 1,'01-10-2004', 17224.15
                       pa_ufs_ac_tabla '10/11/2004',17500.69,1
                       
    anular  test    :  ROLLBACK TRAN
    comentario      :  ingresa valores
    */ 
as

set nocount on

    update pr_tablaufs 
    set 
        uf_valor = @valor
    where uf_fecha = @fecha
--        and id_moneda = @id_moneda
    if @@error <> 0 goto ON_ERROR

    select uf_valor 
    from pr_tablaufs
    where uf_fecha = @fecha
        and id_moneda = @id_moneda

IF @@TRANCOUNT > 0
    COMMIT TRAN
return 0

ON_ERROR:
IF @@TRANCOUNT > 0
    ROLLBACK TRAN
Print 'Se Produjo un error en la transacci�n y se abort�'
return -1

Error_Params:
Print 'Debe usarse :dbo.pa_ufs_ac_tabla
         @id_moneda int = 0'
return -2







GO


