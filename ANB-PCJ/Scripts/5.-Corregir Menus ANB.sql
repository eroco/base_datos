select * from op_menu order by 1 desc

/*Dar a desarrollo los permisos*/
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 85) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,85) --ACC_Modificar_Parametros
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 338) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,338) --ACC_Modificar_Parametros
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 507) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,507) --ACC_Consultar_Abogados
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 508) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,508) --ACC_Modificar_Abogados
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 509) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,509) --ACC_Eliminar_Abogados
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 510) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,510) --ACC_Ingresar_Abogados
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 511) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,511) --ACC_Consultar_Categorias
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 512) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,512) --ACC_Modificar_Categorias
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 513) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,513) --ACC_Eliminar_Categorias
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 514) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,514) --ACC_Ingresar_Categorias
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 515) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,515) --ACC_Consultar_Sub_Categorias
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 516) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,516) --ACC_Modificar_Sub_Categorias
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 517) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,517) --ACC_Eliminar_Sub_Categorias
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 518) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,518) --ACC_Ingresar_Sub_Categorias
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 519) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,519) --ACC_Consultar_Centro_Costos
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 520) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,520) --ACC_Modificar_Centro_Costos
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 521) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,521) --ACC_Eliminar_Centro_Costos
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 522) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,522) --ACC_Ingresar_Centro_Costos
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 571) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,571) --ACC_Consultar_Funcionarios
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 572) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,572) --ACC_Modificar_Funcionarios
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 573) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,573) --ACC_Eliminar_Funcionarios
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 574) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,574) --ACC_Ingresar_Funcionarios
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 575) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,575) --ACC_Consultar_Tipo_Imagenes
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 576) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,576) --ACC_Modificar_Tipo_Imagenes
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 577) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,577) --ACC_Eliminar_Tipo_Imagenes
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 578) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,578) --ACC_Ingresar_Tipo_Imagenes
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 587) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,587) --ACC_Consultar_Ocupaciones
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 588) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,588) --ACC_Modificar_Ocupaciones
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 589) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,589) --ACC_Eliminar_Ocupaciones
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 590) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,590) --ACC_Ingresar_Ocupaciones
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 591) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,591) --ACC_Consultar_Origenes
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 592) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,592) --ACC_Modificar_Origenes
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 593) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,593) --ACC_Eliminar_Origenes
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 594) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,594) --ACC_Ingresar_Origenes
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 595) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,595) --ACC_Consultar_Origenes_Propiedad
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 596) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,596) --ACC_Modificar_Origenes_Propiedad
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 597) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,597) --ACC_Eliminar_Origenes_Propiedad
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 598) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,598) --ACC_Ingresar_Origenes_Propiedad
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 611) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,611) --ACC_Consultar_Sucursales
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 612) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,612) --ACC_Modificar_Sucursales
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 613) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,613) --ACC_Eliminar_Sucursales
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 614) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,614) --ACC_Ingresar_Sucursales
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 615) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,615) --ACC_Consultar_Tabla_Ufs
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 616) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,616) --ACC_Modificar_Tabla_Ufs
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 617) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,617) --ACC_Eliminar_Tabla_Ufs
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 618) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,618) --ACC_Ingresar_Tabla_Ufs
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 691) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,691) --ACC_Consultar_Tipo_Gastos
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 692) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,692) --ACC_Modificar_Tipo_Gastos
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 693) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,693) --ACC_Eliminar_Tipo_Gastos
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 694) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,694) --ACC_Ingresar_Tipo_Gastos
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 950) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,950) --ACC_Proveedores_Nomina
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 951) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,951) --ACC_Proveedores_Ingresar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 952) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,952) --ACC_Proveedores_Modificar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 953) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,953) --ACC_Proveedores_Eliminar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1046) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1046) --ACC_Corredores_Consultar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1047) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1047) --ACC_Corredores_Modificar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1048) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1048) --ACC_Corredores_Eliminar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1049) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1049) --ACC_Corredores_Ingresar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1050) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1050) --ACC_Parametros_Consultar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1051) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1051) --ACC_Parametros_Eiminar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1052) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1052) --ACC_Parametros_Ingresar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1053) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1053) --ACC_Segmentaciones_Consultar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1054) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1054) --ACC_Segmentaciones_Modificar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1055) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1055) --ACC_Segmentaciones_Eliminar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1056) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1056) --ACC_Segmentaciones_Ingresar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1057) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1057) --ACC_Bancas_Consultar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1058) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1058) --ACC_Bancas_Modificar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1059) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1059) --ACC_Bancas_Eliminar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1060) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1060) --ACC_Bancas_Ingresar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1061) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1061) --ACC_Resultado_Remate_Consultar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1062) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1062) --ACC_Resultado_Remate_Modificar
if not exists(select * from op_opciones_tusuarios where id_tipousuario = 1 AND id_opcion = 1064) insert into op_opciones_tusuarios (id_tipousuario,id_opcion) values (1,1064) --ACC_Resultado_Remate_Ingresar

/*Mover Nominas a un menu general*/
/*Mover Nominas a un menu general*/
if not exists(select * from [op_menu] where men_nombre = 'Mantenedores Básicos')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'Mantenedores Básicos','',
		2,85,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'Mantenedores Básicos'
)


/*Modificando el menu padre creado*/
update op_menu set id_menupadre = (select top 1 id_menu from [op_menu] where men_nombre = 'Mantenedores Básicos') where men_url like '%/ANB_Base/MantenedoreBasicos/%'

/*Completar url para acceder Nominas*/
--select replace(men_url,'/ANB_Base/MantenedoreBasicos','(base)MantenedoresBasicos'),* from op_menu where men_url like '%/ANB_Base/MantenedoreBasicos/%'
update op_menu set men_url = replace(men_url,'/ANB_Base/MantenedoreBasicos','(base)MantenedoresBasicos') where  men_url like '%/ANB_Base/MantenedoreBasicos/%'








