if exists (select * from dbo.sysobjects where id = object_id('[dbo].[FK_pr_proveedor_op_consignatario]') and OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE pr_proveedores DROP CONSTRAINT FK_pr_proveedor_op_consignatario
ALTER TABLE pr_proveedores ADD CONSTRAINT FK_pr_proveedor_op_consignatario FOREIGN KEY (id_consignatario) REFERENCES [dbo].[op_consignatarios](id_consignatario);

if exists (select * from dbo.sysobjects where id = object_id('[dbo].[FK_pr_origenespropiedad_op_consignatario]') and OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE pr_origenespropiedad DROP CONSTRAINT FK_pr_origenespropiedad_op_consignatario
ALTER TABLE pr_origenespropiedad ADD CONSTRAINT FK_pr_origenespropiedad_op_consignatario FOREIGN KEY (id_consignatario) REFERENCES [dbo].[op_consignatarios](id_consignatario);

if exists (select * from dbo.sysobjects where id = object_id('[dbo].[FK_pr_corredores_pr_corredores]') and OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE pr_corredores DROP CONSTRAINT FK_pr_corredores_pr_corredores
ALTER TABLE pr_corredores ADD CONSTRAINT FK_pr_corredores_pr_corredores FOREIGN KEY (id_corredor_padre) REFERENCES pr_corredores(id_corredor);

update pr_corredores set id_ejecutivo = null where id_ejecutivo = 0
if exists (select * from dbo.sysobjects where id = object_id('[dbo].[FK_pr_corredores_pr_ejecutivos]') and OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE pr_corredores DROP CONSTRAINT FK_pr_corredores_pr_ejecutivos
ALTER TABLE pr_corredores ADD CONSTRAINT FK_pr_corredores_pr_ejecutivos FOREIGN KEY (id_ejecutivo) REFERENCES pr_ejecutivos(id_ejecutivo);

if exists (select * from dbo.sysobjects where id = object_id('[dbo].[FK_pr_abogados_pr_abogados]') and OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE pr_abogados DROP CONSTRAINT FK_pr_abogados_pr_abogados
ALTER TABLE pr_abogados ADD CONSTRAINT FK_pr_abogados_pr_abogados FOREIGN KEY (id_abogado_control) REFERENCES pr_abogados(id_abogado);

if exists (select * from dbo.sysobjects where id = object_id('[dbo].[FK_ta_pc_parametros_op_consignatarios]') and OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE ta_pc_parametros DROP CONSTRAINT FK_ta_pc_parametros_op_consignatarios
ALTER TABLE ta_pc_parametros ADD CONSTRAINT FK_ta_pc_parametros_op_consignatarios FOREIGN KEY (id_consignatario) REFERENCES op_consignatarios(id_consignatario);

if exists (select * from dbo.sysobjects where id = object_id('[dbo].[FK_pr_tablaufs_pr_monedas]') and OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE pr_tablaufs DROP CONSTRAINT FK_pr_tablaufs_pr_monedas
ALTER TABLE pr_tablaufs ADD CONSTRAINT FK_pr_tablaufs_pr_monedas FOREIGN KEY (id_moneda) REFERENCES pr_monedas(id_moneda);

if exists (select * from dbo.sysobjects where id = object_id('[dbo].[FK_pr_subcategoria_ta_pp_tipocategorias]') and OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE pr_subcategoria DROP CONSTRAINT FK_pr_subcategoria_ta_pp_tipocategorias
ALTER TABLE pr_subcategoria ADD CONSTRAINT FK_pr_subcategoria_ta_pp_tipocategorias FOREIGN KEY (id_tipocategoria) REFERENCES ta_pp_tipocategorias(id_tipocategoria);


if exists (select * from dbo.sysobjects where id = object_id('[dbo].[FK_pr_propiedades_pr_origenespropiedad]') and OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE pr_propiedades DROP CONSTRAINT FK_pr_propiedades_pr_origenespropiedad
if exists (select * from dbo.sysobjects where id = object_id('[dbo].[PK_pr_origenescartera]'))
ALTER TABLE dbo.pr_origenespropiedad drop constraint PK_pr_origenescartera

ALTER TABLE pr_origenespropiedad ADD CONSTRAINT PK_pr_origenescartera  PRIMARY KEY(id_origenpropiedad)
ALTER TABLE pr_propiedades ADD CONSTRAINT FK_pr_propiedades_pr_origenespropiedad FOREIGN KEY (id_origenpropiedad) REFERENCES pr_origenespropiedad(id_origenpropiedad);





