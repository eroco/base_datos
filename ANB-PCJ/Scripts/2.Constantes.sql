/*TIPO GASTOS*/
--691	Consultar Tipo Gastos	ACC_Consultar_Tipo_Gastos	59
--692	Modificar Tipo Gastos	ACC_Modificar_Tipo_Gastos	59
--693	Eliminar Tipo Gastos	ACC_Eliminar_Tipo_Gastos	59
--694	Ingresar Tipo Gastos	ACC_Ingresar_Tipo_Gastos	59

/*TIPO PROVEEDORES*/
--224	Mantenedor Proveedores	ACC_Mantenedor_Proveedores	24
--950	Proveedores Nomina	ACC_Proveedores_Nomina	24
--951	Proveedores Ingresar	ACC_Proveedores_Ingresar	24
--952	Proveedores Modificar	ACC_Proveedores_Modificar	24
--953	Proveedores Eliminar	ACC_Proveedores_Eliminar	24

/*CENTRO COSTO*/
--173	Mantenedor Centro Costos	ACC_Mantenedor_Centro_Costos	24
--519	Consultar Centro Costos	ACC_Consultar_Centro_Costos	54
--520	Modificar Centro Costos	ACC_Modificar_Centro_Costos	54
--521	Eliminar Centro Costos	ACC_Eliminar_Centro_Costos	54
--522	Ingresar Centro Costos	ACC_Ingresar_Centro_Costos	54

/*SUCURSALES*/
--174	Mantenedor Sucursales	ACC_Mantenedor_Sucursales	24
--611	Consultar Sucursales	ACC_Consultar_Sucursales	54
--612	Modificar Sucursales	ACC_Modificar_Sucursales	54
--613	Eliminar Sucursales	ACC_Eliminar_Sucursales	54
--614	Ingresar Sucursales	ACC_Ingresar_Sucursales	54

/*Origenes y origenes propiedad*/
--591	Consultar Origenes	ACC_Consultar_Origenes	54
--592	Modificar Origenes	ACC_Modificar_Origenes	54
--593	Eliminar Origenes	ACC_Eliminar_Origenes	54
--594	Ingresar Origenes	ACC_Ingresar_Origenes	54
--595	Consultar Origenes Propiedad	ACC_Consultar_Origenes_Propiedad	54
--596	Modificar Origenes Propiedad	ACC_Modificar_Origenes_Propiedad	54
--597	Eliminar Origenes Propiedad	ACC_Eliminar_Origenes_Propiedad	54
--598	Ingresar Origenes Propiedad	ACC_Ingresar_Origenes_Propiedad	54

/*Corredores*/
--no existen

if not exists(select * from op_opciones where op_constante = 'ACC_Corredores_Consultar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1046,'Consultar Corredores',54,'ACC_Corredores_Consultar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Corredores_Modificar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1047,'Modificar Corredores',54,'ACC_Corredores_Modificar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Corredores_Eliminar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1048,'Eliminar Corredores',54,'ACC_Corredores_Eliminar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Corredores_Ingresar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1049,'Ingresar Corredores',54,'ACC_Corredores_Ingresar',0);


/*abogados*/
--178	Mantenedor Abogados	ACC_Mantenedor_Abogados	24
--507	Consultar Abogados	ACC_Consultar_Abogados	54
--508	Modificar Abogados	ACC_Modificar_Abogados	54
--509	Eliminar  Abogados	ACC_Eliminar_Abogados	54
--510	Ingresar Abogados	ACC_Ingresar_Abogados	54

/*Parametros*/
--338	Modificar Parametros	ACC_Modificar_Parametros	24
if not exists(select * from op_opciones where op_constante = 'ACC_Parametros_Consultar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1050,'Consultar Parametros',54,'ACC_Parametros_Consultar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Parametros_Eiminar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1051,'Eliminar Parametros',54,'ACC_Parametros_Eiminar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Parametros_Ingresar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1052,'Ingresar Parametros',54,'ACC_Parametros_Ingresar',0);

/*Ufs*/
--615	Consultar Tabla Ufs	ACC_Consultar_Tabla_Ufs	54
--616	Modificar Tabla Ufs	ACC_Modificar_Tabla_Ufs	54
--617	Eliminar Tabla Ufs	ACC_Eliminar_Tabla_Ufs	54
--618	Ingresar Tabla Ufs	ACC_Ingresar_Tabla_Ufs	54

/*Tipos de Im�genes*/
--575	Consultar Tipo Imagenes	ACC_Consultar_Tipo_Imagenes	54
--576	Modificar Tipo Imagenes	ACC_Modificar_Tipo_Imagenes	54
--577	Eliminar Tipo Imagenes	ACC_Eliminar_Tipo_Imagenes	54
--578	Ingresar Tipo Imagenes	ACC_Ingresar_Tipo_Imagenes	54

/*Funcionarios*/
--571	Consultar Funcionarios	ACC_Consultar_Funcionarios	54
--572	Modificar Funcionarios	ACC_Modificar_Funcionarios	54
--573	Eliminar Funcionarios	ACC_Eliminar_Funcionarios	54
--574	Ingresar Funcionarios	ACC_Ingresar_Funcionarios	54

/*Segmentaciones*/
--no existen
if not exists(select * from op_opciones where op_constante = 'ACC_Segmentaciones_Consultar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1053,'Consultar Segmentaciones',54,'ACC_Segmentaciones_Consultar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Segmentaciones_Modificar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1054,'Modificar Segmentaciones',54,'ACC_Segmentaciones_Modificar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Segmentaciones_Eliminar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1055,'Eliminar Segmentaciones',54,'ACC_Segmentaciones_Eliminar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Segmentaciones_Ingresar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1056,'Ingresar Segmentaciones',54,'ACC_Segmentaciones_Ingresar',0);

/*Bancas*/
--no existen

if not exists(select * from op_opciones where op_constante = 'ACC_Bancas_Consultar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1057,'Consultar Bancas',54,'ACC_Bancas_Consultar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Bancas_Modificar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1058,'Modificar Bancas',54,'ACC_Bancas_Modificar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Bancas_Eliminar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1059,'Eliminar Bancas',54,'ACC_Bancas_Eliminar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Bancas_Ingresar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1060,'Ingresar Bancas',54,'ACC_Bancas_Ingresar',0);

/*Ocupaciones*/
--587	Consultar Ocupaciones	ACC_Consultar_Ocupaciones	54
--588	Modificar Ocupaciones	ACC_Modificar_Ocupaciones	54
--589	Eliminar Ocupaciones	ACC_Eliminar_Ocupaciones	54
--590	Ingresar Ocupaciones	ACC_Ingresar_Ocupaciones	54

/*Subcategorias*/
/*Categorias*/
--511	Consultar Categorias	ACC_Consultar_Categorias	54
--512	Modificar Categorias	ACC_Modificar_Categorias	54
--513	Eliminar Categorias	ACC_Eliminar_Categorias	54
--514	Ingresar Categorias	ACC_Ingresar_Categorias	54
--515	Consultar Sub Categorias	ACC_Consultar_Sub_Categorias	54
--516	Modificar Sub Categorias	ACC_Modificar_Sub_Categorias	54
--517	Eliminar Sub Categorias	ACC_Eliminar_Sub_Categorias	54
--518	Ingresar Sub Categorias	ACC_Ingresar_Sub_Categorias	54

/*Resultado Remates*/
--no existen
if not exists(select * from op_opciones where op_constante = 'ACC_Resultado_Remate_Consultar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1061,'Consultar Resultado Remates',54,'ACC_Resultado_Remate_Consultar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Resultado_Remate_Modificar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1062,'Modificar Resultado Remates',54,'ACC_Resultado_Remate_Modificar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Resultado_Remate_Eliminar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1063,'Eliminar Resultado Remates',54,'ACC_Resultado_Remate_Eliminar',0);

if not exists(select * from op_opciones where op_constante = 'ACC_Resultado_Remate_Ingresar')
	insert into op_opciones(id_opcion,op_opcion,id_proceso,op_constante,op_interna)
	values(1064,'Ingresar Resultado Remates',54,'ACC_Resultado_Remate_Ingresar',0);
