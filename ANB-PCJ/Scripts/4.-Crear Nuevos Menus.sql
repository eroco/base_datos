--select * from [op_menu] order by 1 desc

if not exists(select * from [op_menu] where men_nombre = 'N�mina Tipos de Gastos')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Tipos de Gastos','/ANB_Base/MantenedoreBasicos/wf_pr_tipogastos_no.aspx',
		2,691,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Tipos de Gastos'
)

if not exists(select * from [op_menu] where men_nombre = 'N�mina Proveedores')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Proveedores','/ANB_Base/MantenedoreBasicos/wf_pr_proveedores_no.aspx',
		2,950,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Proveedores'
)


if not exists(select * from [op_menu] where men_nombre = 'N�mina Centros de Costos')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Centros de Costos','/ANB_Base/MantenedoreBasicos/wf_pr_centrocostos_no.aspx',
		2,519,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Centros de Costos'
)

if not exists(select * from [op_menu] where men_nombre = 'N�mina Sucursales')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Sucursales','/ANB_Base/MantenedoreBasicos/wf_pr_sucursales_no.aspx',
		2,611,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Sucursales'
)

if not exists(select * from [op_menu] where men_nombre = 'N�mina Or�genes de Propiedad')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Or�genes de Propiedad','/ANB_Base/MantenedoreBasicos/wf_pr_origenespropiedad_no.aspx',
		2,595,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Or�genes de Propiedad'
)

if not exists(select * from [op_menu] where men_nombre = 'N�mina Or�genes')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Or�genes','/ANB_Base/MantenedoreBasicos/wf_pr_origenes_no.aspx',
		2,591,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Or�genes'
)

if not exists(select * from [op_menu] where men_nombre = 'N�mina Corredores')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Corredores','/ANB_Base/MantenedoreBasicos/wf_pr_corredores_no.aspx',
		2,1046,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Corredores'
)

if not exists(select * from [op_menu] where men_nombre = 'N�mina Abogados')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Abogados','/ANB_Base/MantenedoreBasicos/wf_pr_abogados_no.aspx',
		2,507,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Abogados'
)

if not exists(select * from [op_menu] where men_nombre = 'N�mina Par�metros')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Par�metros','/ANB_Base/MantenedoreBasicos/wf_ta_pc_parametros_no.aspx',
		2,1050,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Par�metros'
)

if not exists(select * from [op_menu] where men_nombre = 'N�mina Ufs')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Ufs','/ANB_Base/MantenedoreBasicos/wf_pr_tablaufs_no.aspx',
		2,615,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Ufs'
)

if not exists(select * from [op_menu] where men_nombre = 'N�mina Tipos de Im�genes')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Tipos de Im�genes','/ANB_Base/MantenedoreBasicos/wf_pr_tipoimagenes_no.aspx',
		2,575,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Tipos de Im�genes'
)

if not exists(select * from [op_menu] where men_nombre = 'N�mina Funcionarios')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Funcionarios','/ANB_Base/MantenedoreBasicos/wf_pr_funcionarios_no.aspx',
		2,571,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Funcionarios'
)

if not exists(select * from [op_menu] where men_nombre = 'N�mina Segmentaciones')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Segmentaciones','/ANB_Base/MantenedoreBasicos/wf_ta_rj_segmentacion_no.aspx',
		2,1053,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Segmentaciones'
)

if not exists(select * from [op_menu] where men_nombre = 'N�mina Bancas')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Bancas','/ANB_Base/MantenedoreBasicos/wf_ta_rj_bancas_no.aspx',
		2,1057,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Bancas'
)

if not exists(select * from [op_menu] where men_nombre = 'N�mina Ocupaciones')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Ocupaciones','/ANB_Base/MantenedoreBasicos/wf_pr_ocupaciones_no.aspx',
		2,587,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Ocupaciones'
)




if not exists(select * from [op_menu] where men_nombre = 'N�mina Subcategor�as')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Subcategor�as','/ANB_Base/MantenedoreBasicos/wf_pr_subcategoria_no.aspx',
		2,515,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Subcategor�as'
)




if not exists(select * from [op_menu] where men_nombre = 'N�mina Categor�as')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Categor�as','/ANB_Base/MantenedoreBasicos/wf_pr_categorias_no.aspx',
		2,511,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Categor�as'
)


if not exists(select * from [op_menu] where men_nombre = 'N�mina Resultado Remates')
Insert into op_menu(id_menupadre,men_nombre,men_url,
id_tipoventana,id_opcion,men_opciones,
men_ancho,men_alto,men_orden,
men_activo,men_nombreventana)
values(
		2,'N�mina Resultado Remates','/ANB_Base/MantenedoreBasicos/wf_ta_rj_remates_resultado_no.aspx',
		2,1061,'scrollbars=yes,width=800,height=540, resizable=yes, menubar=no, statusbar=no',
		800,560,200,
		1,'N�mina Resultado Remates'
)






